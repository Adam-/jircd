/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.test.IRCClient;
import net.rizon.plexus.test.PlexusRunner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class WhoisTest
{
	@Rule
	public PlexusRunner runner = new PlexusRunner();

	@Test
	public void testPlexus()
	{
		Plexus plexus = runner.getPlexus();

		IRCClient client = new IRCClient(plexus);
		client.connect("127.0.0.1", 6667);

		client.introduce("test", "test", "test");
		client.when("PING", m -> { client.pong(m.getParams()[0]); });
		client.expectNumeric(Numeric.RPL_WELCOME);

		client.whois("test");
		client.expectNumeric(Numeric.RPL_WHOISSERVER, (message) -> {
			Assert.assertEquals("[irc.rizon.net] 312 test test irc.rizon.net :Rizon IRC Network - Client Server", message.toString());
		});
	}
}
