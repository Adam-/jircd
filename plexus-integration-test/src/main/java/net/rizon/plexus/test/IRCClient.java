/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.test;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.function.Consumer;
import java.util.function.Predicate;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.io.IRCMessage;
import net.rizon.plexus.test.io.IRCClientInitializer;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IRCClient
{
	private static final Logger logger = LoggerFactory.getLogger(IRCClient.class);

	private static final int EXPECT_TIMEOUT = 15000;

	private final Plexus plexus;
	private Channel channel;
	private final Deque<IRCMessage> readQueue = new ArrayDeque<>();

	public IRCClient(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public void connect(String host, int port)
	{
		Bootstrap b = new Bootstrap();
		b.group(plexus.getWorkerGroup())
			.channel(NioSocketChannel.class)
			.option(ChannelOption.TCP_NODELAY, true)
			.handler(new IRCClientInitializer(this));

		ChannelFuture future = b.connect("127.0.0.1", 6667);
		channel = future.channel();
		future.awaitUninterruptibly();

		logger.info("Connected to {}:{}", host, port);
	}

	private void write(IRCMessage message)
	{
		channel.writeAndFlush(message);
	}

	private void write(String command, String... args)
	{
		IRCMessage ircmessage = new IRCMessage();
		ircmessage.setCommand(command);
		ircmessage.setParams(args);

		write(ircmessage);
	}

	public void onRead(IRCMessage message)
	{
		synchronized (readQueue)
		{
			readQueue.add(message);
			readQueue.notify();
		}
	}

	public void when(Predicate<IRCMessage> matcher, Consumer<IRCMessage> handler)
	{
		for (;;)
		{
			synchronized (readQueue)
			{
				while (!readQueue.isEmpty())
				{
					IRCMessage m = readQueue.pop();

					if (matcher.test(m))
					{
						logger.debug("Found match: {}", m);

						handler.accept(m);
						return;
					}
				}

				try
				{
					readQueue.wait(EXPECT_TIMEOUT);

					if (readQueue.isEmpty())
					{
						Assert.fail("when/expect timeout");
					}
				}
				catch (InterruptedException ex)
				{
					logger.warn(null, ex);
				}
			}
		}
	}

	public void when(String command, Consumer<IRCMessage> handler)
	{
		when(m -> m.getCommand().equals(command), handler);
	}

	public void expect(Predicate<IRCMessage> matcher)
	{
		when(matcher, (message) -> {});
	}

	public void expectNumeric(Numeric numeric)
	{
		expectNumeric(numeric, (message) -> {});
	}

	public void expectNumeric(Numeric numeric, Consumer<IRCMessage> handler)
	{
		when(m -> m.getCommand().equals(String.format("%03d", numeric.getNumeric())), handler);
	}

	public void introduce(String nick, String user, String realname)
	{
		write("USER", user, ".", ".", realname);
		write("NICK", nick);
	}

	public void pong(String target)
	{
		write("PONG", target);
	}

	public void whois(String target)
	{
		write("WHOIS", target);
	}
}
