# jircd

jircd is an IRCD built on [copycat](http://atomix.io/copycat/), which is an implementation of the [Raft Consensus Algorithm](https://raft.github.io/).
This allows for a network of servers to be distributed and fault tolerant, without the spanningtree type links of traditional IRCDs.

For a server to perform an operation that will change the state of the network, it must submit an operation to the cluster, and wait for the command to
be replicated or applied to its state machine. The operations submitted to the cluster are serialized to JSON when communicated between servers, which
allows for great flexability and easy extensibility compared to traditional RFC1459 framing.

## To run

```
mvn package
./run.sh
```
