/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.util.function.BiConsumer;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.command.CommandManager;
import net.rizon.plexus.config.Config;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.modes.channelmodes.InviteOnly;
import net.rizon.plexus.replication.ReplicationClient;
import net.rizon.plexus.replication.commands.KnockChannel;
import net.rizon.plexus.users.LocalUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class KnockTest
{
	@Mock
	private CommandManager manager;
	
	@Mock
	private Plexus plexus;
	
	@Mock
	private IRC irc;
	
	@Mock
	private LocalUser user;
	
	@Mock
	private Channel channel;
	
	@Mock
	private Config config;
	
	@Mock
	private net.rizon.plexus.config.Channel configChannel;
	
	@Mock
	private ReplicationClient client;
	
	@Mock
	private ModeManager modeManager;
	
	@Mock
	private InviteOnly inviteOnly;
	
	@Before
	public void before()
	{
		when(manager.getPlexus()).thenReturn(plexus);
		
		when(plexus.getIrc()).thenReturn(irc);
		when(plexus.getConf()).thenReturn(config);
		when(plexus.getClient()).thenReturn(client);
		when(plexus.getModeManager()).thenReturn(modeManager);
		
		when(channel.getName()).thenReturn("#a");
		
		when(config.getChannel()).thenReturn(configChannel);
		
		when(irc.findChannel("#a")).thenReturn(channel);
		
		when(modeManager.getInviteOnly()).thenReturn(inviteOnly);
		
		when(channel.hasMode(modeManager.getInviteOnly())).thenReturn(true);
	}

	@Test
	public void testExecute()
	{
		Knock knock = new Knock();
		knock.setPlexus(plexus);
		knock.setManager(manager);
		knock.setChannel("#a");
		
		knock.execute(user);
		
		ArgumentCaptor<KnockChannel> knockCaptor = ArgumentCaptor.forClass(KnockChannel.class);
		
		verify(client).submit(any(Client.class), knockCaptor.capture(), any(BiConsumer.class));
		
		KnockChannel kc = knockCaptor.getValue();
		Assert.assertEquals("#a", kc.getChannel());
	}
	
}
