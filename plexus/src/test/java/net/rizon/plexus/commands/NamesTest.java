/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.util.Arrays;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.Server;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.numerics.RplNamesReply;
import net.rizon.plexus.users.Capability;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NamesTest
{
	@Mock
	private Plexus plexus;

	@Mock
	private IRC irc;

	@Mock
	private LocalUser user;
	
	@Mock
	private Client client;

	@Mock
	private Channel channel;
	
	@Mock
	private Server me;
	
	@Mock
	private ModeManager modeManager;

	@Before
	public void before()
	{
		when(plexus.getIrc()).thenReturn(irc);
		when(plexus.getMe()).thenReturn(me);
		when(plexus.getModeManager()).thenReturn(modeManager);
		
		when(me.getName()).thenReturn("irc.rizon.net");

		when(user.getClient()).thenReturn(client);
		when(user.getPlexus()).thenReturn(plexus);
		when(user.getName()).thenReturn("Adam");

		when(irc.findChannel("#a")).thenReturn(channel);
		
		when(channel.getName()).thenReturn("#a");
	}

	@Test
	public void testExecute()
	{
		User userOnChannel = mock(User.class);
		
		when(userOnChannel.getName()).thenReturn("Adam2");
		
		when(channel.getUsers()).thenReturn(Arrays.asList(new Membership(userOnChannel, channel, null)));
		
		Names names = new Names();
		names.setPlexus(plexus);
		names.setChannel("#a");
		names.execute(user);

		ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
		verify(user).writeMessage(messageCaptor.capture());
		
		RplNamesReply reply = (RplNamesReply) messageCaptor.getValue();

		Assert.assertEquals('=', reply.getCharacter());
		Assert.assertEquals("#a", reply.getChannel());
		Assert.assertEquals("Adam2", reply.getNames());
	}
	
	@Test
	public void testUhnames()
	{
		User userOnChannel = mock(User.class);
		
		when(userOnChannel.getName()).thenReturn("Adam2");
		when(userOnChannel.getUsername()).thenReturn("user");
		when(userOnChannel.getVhost()).thenReturn("host");
		
		when(channel.getUsers()).thenReturn(Arrays.asList(new Membership(userOnChannel, channel, null)));
		
		when(client.hasCap(Capability.USERHOST_IN_NAMES)).thenReturn(true);
		
		Names names = new Names();
		names.setPlexus(plexus);
		names.setChannel("#a");
		names.execute(user);

		ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);
		verify(user).writeMessage(messageCaptor.capture());

		RplNamesReply reply = (RplNamesReply) messageCaptor.getValue();

		Assert.assertEquals('=', reply.getCharacter());
		Assert.assertEquals("#a", reply.getChannel());
		Assert.assertEquals("Adam2!user@host", reply.getNames());
	}
}
