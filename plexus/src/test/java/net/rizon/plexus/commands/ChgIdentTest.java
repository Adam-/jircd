/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.replication.PlexusCommand;
import net.rizon.plexus.replication.ReplicationClient;
import net.rizon.plexus.replication.commands.ChangeIdent;
import net.rizon.plexus.users.LocalUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ChgIdentTest
{
	@Mock
	private LocalUser user;
	
	@Mock
	private Plexus plexus;
	
	@Mock
	private IRC irc;
	
	@Mock
	private ReplicationClient client;

	@Before
	public void setUp()
	{
		when(user.isNetAdmin()).thenReturn(true);
		
		when(plexus.getIrc()).thenReturn(irc);
		when(plexus.getClient()).thenReturn(client);
		
		when(irc.findUser("Adam")).thenReturn(user);
	}

	@Test
	public void testExecute()
	{
		ChgIdent chgIdent = new ChgIdent();
		chgIdent.setPlexus(plexus);
		chgIdent.setTarget("Adam");
		chgIdent.setUsername("adam");
		
		chgIdent.execute(user);
		
		ArgumentCaptor<PlexusCommand> commandCaptor = ArgumentCaptor.forClass(PlexusCommand.class);
		verify(client).submit(Matchers.isNull(Client.class), commandCaptor.capture());
		
		ChangeIdent command = (ChangeIdent) commandCaptor.getValue();
		Assert.assertEquals("adam", command.getIdent());
	}

}
