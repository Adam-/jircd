/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Client;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.messages.CapMessage;
import net.rizon.plexus.users.Capability;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CapTest
{
	@Mock
	private Plexus plexus;

	@Mock
	private Client client;

	@Before
	public void before()
	{
		when(client.getMessageName()).thenReturn("*");
	}

	@Test
	public void testCapLs()
	{
		Cap cap = new Cap();
		cap.setPlexus(plexus);
		cap.setSubcommand("LS");

		cap.executeUnregistered(client);

		verify(client).writeMessage(new CapMessage(null, "*", "LS", "multi-prefix userhost-in-names"));
	}

	@Test
	public void testCapReq()
	{
		Cap capReq = new Cap();
		capReq.setPlexus(plexus);
		capReq.setSubcommand("REQ");
		capReq.setArgs("multi-prefix userhost-in-names");

		capReq.executeUnregistered(client);

		verify(client).writeMessage(new CapMessage(null, "*", "ACK", "multi-prefix userhost-in-names"));
		verify(client).addCap(Capability.MULTI_PREFIX);
		verify(client).addCap(Capability.USERHOST_IN_NAMES);
	}

	@Test
	public void testCapReqDel()
	{
		Cap capReq = new Cap();
		capReq.setPlexus(plexus);
		capReq.setSubcommand("REQ");
		capReq.setArgs("multi-prefix userhost-in-names");

		capReq.executeUnregistered(client);

		verify(client).writeMessage(new CapMessage(null, "*", "ACK", "multi-prefix userhost-in-names"));
		verify(client).addCap(Capability.MULTI_PREFIX);
		verify(client).addCap(Capability.USERHOST_IN_NAMES);

		Cap capReqDel = new Cap();
		capReqDel.setPlexus(plexus);
		capReqDel.setSubcommand("REQ");
		capReqDel.setArgs("-multi-prefix -userhost-in-names");

		capReqDel.executeUnregistered(client);

		verify(client).writeMessage(new CapMessage(null, "*", "ACK", "-multi-prefix -userhost-in-names"));
		verify(client).delCap(Capability.MULTI_PREFIX);
		verify(client).delCap(Capability.USERHOST_IN_NAMES);
	}

	@Test
	public void testCapReqInvalid()
	{
		Cap cap = new Cap();
		cap.setPlexus(plexus);
		cap.setSubcommand("REQ");
		cap.setArgs("not-a-real-cap userhost-in-names");

		cap.executeUnregistered(client);

		verify(client).writeMessage(new CapMessage(null, "*", "NAK", "not-a-real-cap userhost-in-names"));
		verify(client, never()).addCap(Matchers.any(Capability.class));
	}

}
