/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.plexus.commands;

import java.time.Instant;
import java.util.Collections;
import java.util.function.BiConsumer;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.config.Auth;
import net.rizon.plexus.config.Config;
import net.rizon.plexus.config.General;
import net.rizon.plexus.replication.ReplicationClient;
import net.rizon.plexus.replication.commands.NickChange;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.xline.XLineManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NickTest
{
	@Mock
	private LocalUser user;

	@Mock
	private Client client;

	@Mock
	private Auth auth;

	@Mock
	private Plexus plexus;

	@Mock
	private XLineManager xlineManager;

	@Mock
	private IRC irc;

	@Mock
	private Config config;

	@Mock
	private General general;
	
	@Mock
	private ReplicationClient replicationClient;

	@Before
	public void before()
	{
		when(user.getClient()).thenReturn(client);
		when(user.getPlexus()).thenReturn(plexus);

		when(client.getIline()).thenReturn(auth);

		when(auth.getFlags()).thenReturn(Collections.EMPTY_LIST);

		when(plexus.getXlineManager()).thenReturn(xlineManager);
		when(plexus.getIrc()).thenReturn(irc);
		when(plexus.getConf()).thenReturn(config);
		when(plexus.getClient()).thenReturn(replicationClient);

		when(config.getGeneral()).thenReturn(general);
	}

	@Test
	public void testExecute()
	{
		when(user.getTs()).thenReturn(42L);
		
		Nick nick = new Nick();
		nick.setPlexus(plexus);
		nick.setNick("Adam");

		nick.execute(user);

		ArgumentCaptor<NickChange> nickCaptor = ArgumentCaptor.forClass(NickChange.class);

		verify(replicationClient).submit(any(Client.class), nickCaptor.capture(), any(BiConsumer.class));

		NickChange nc = nickCaptor.getValue();
		Assert.assertEquals(Instant.ofEpochSecond(42L), nc.getSourceTs());
	}

}
