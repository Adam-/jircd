/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.serializer;

import com.google.gson.Gson;
import java.util.UUID;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.commands.IntroduceServer;
import net.rizon.plexus.replication.snapshot.Snapshot;
import net.rizon.plexus.replication.snapshot.UserSnapshot;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GsonFactoryTest
{
	private static final Logger logger = LoggerFactory.getLogger(GsonFactoryTest.class);
	
	@Test
	public void testCommandSerialization()
	{
		Gson gson = GsonFactory.create();
		
		PlexusCopycatCommand pcc = new PlexusCopycatCommand();
		
		IntroduceServer is = new IntroduceServer();
		is.setUuid(UUID.randomUUID());
		is.setName("test");
		is.setDescription("test test");
		
		pcc.setCommand(is);
		
		String json = gson.toJson(pcc);
		logger.info("to json: {}", json);
		
		PlexusCopycatCommand out = gson.fromJson(json, PlexusCopycatCommand.class);
		logger.info("from json: {}", out);
		
		Assert.assertTrue(out.getCommand() instanceof IntroduceServer);
	}
	
	@Test
	public void testSnapshotSerialization()
	{
		Gson gson = GsonFactory.create();
		
		UserSnapshot us = new UserSnapshot();
		us.setNickname("test");
		
		Snapshot snapshot = new Snapshot();
		snapshot.getUsers().add(us);
		
		String json = gson.toJson(snapshot);
		
		Snapshot snapshot2 = gson.fromJson(json, Snapshot.class);
		UserSnapshot us2 = snapshot2.getUsers().get(0);
		Assert.assertEquals("test", us2.getNickname());
	}
	
}
