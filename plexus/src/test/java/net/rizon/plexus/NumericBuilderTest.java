/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import java.util.ArrayList;
import java.util.List;
import net.rizon.plexus.numerics.RplNamesReply;
import net.rizon.plexus.users.LocalUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NumericBuilderTest
{
	private static final int CLONES = 1000;
	private static final int TIMES = 20;

	@Mock
	private LocalUser user;

	@Mock
	private Plexus plexus;

	@Mock
	private Server server;

	@Before
	public void before()
	{
		when(user.getName()).thenReturn("Adam");
		when(user.getPlexus()).thenReturn(plexus);

		when(server.getName()).thenReturn("irc.rizon.net");

		when(plexus.getMe()).thenReturn(server);
	}

	@Test
	public void testNumericBuilder()
	{
		NumericBuilder builder = new NumericBuilder(Numeric.RPL_NAMEREPLY, user, '*', "#anope");
		
		for (int i = 0; i < CLONES; ++i)
		{
			builder.add("clone" + i);
		}

		builder.send();

		ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);

		verify(user, times(TIMES)).writeMessage(messageCaptor.capture());

		List<String[]> allValues = new ArrayList<>();

		for (Message numeric : messageCaptor.getAllValues())
		{
			Assert.assertTrue(numeric instanceof RplNamesReply);

			RplNamesReply reply = (RplNamesReply) numeric;

			Assert.assertEquals('*', reply.getCharacter());
			Assert.assertEquals("#anope", reply.getChannel());

			allValues.add(reply.getNames().split(" "));
		}

		int namePos = 0;

		Assert.assertEquals(TIMES, allValues.size());

		for (String[] array : allValues)
		{
			for (String name : array)
			{
				Assert.assertEquals("clone" + namePos++, name);
			}
		}

		Assert.assertEquals(CLONES, namePos);
	}
}
