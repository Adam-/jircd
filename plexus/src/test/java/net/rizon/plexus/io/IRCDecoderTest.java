/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.io;

import io.netty.channel.ChannelHandlerContext;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class IRCDecoderTest
{
	@Mock
	private ChannelHandlerContext ctx;

	@Mock
	private List<Object> list;

	@Test
	public void testDecode() throws Exception
	{
		IRCDecoder decoder = new IRCDecoder();
		decoder.decode(ctx, ":Adam PRIVMSG #anope :hi there", list);

		verify(list).add(new IRCMessage("Adam", "PRIVMSG", "#anope", "hi there"));
	}

	@Test
	public void testDecodeNoCommand() throws Exception
	{
		IRCDecoder decoder = new IRCDecoder();
		decoder.decode(ctx, ":Adam", list);

		verify(list, never()).add(Matchers.any(IRCMessage.class));
	}

	@Test
	public void testNoParameters() throws Exception
	{
		IRCDecoder decoder = new IRCDecoder();
		decoder.decode(ctx, ":Adam LUSERS", list);

		verify(list).add(new IRCMessage("Adam", "LUSERS"));
	}

	@Test
	public void test16Parameters() throws Exception
	{
		IRCDecoder decoder = new IRCDecoder();
		decoder.decode(ctx, "COMMAND 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16", list);

		verify(list).add(new IRCMessage(null, "COMMAND", "1", "2", "3",
			"4", "5", "6", "7", "8", "9", "10", "11", "12", "13",
			"14", "15"));
	}

	@Test
	public void testSpaces() throws Exception
	{
		IRCDecoder decoder = new IRCDecoder();
		decoder.decode(ctx, ":Adam      PRIVMSG         #anope      :hi there", list);

		verify(list).add(new IRCMessage("Adam", "PRIVMSG", "#anope", "hi there"));
	}

}
