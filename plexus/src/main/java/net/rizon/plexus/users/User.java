/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.users;

import io.netty.channel.ChannelFuture;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.rizon.plexus.ClientManager;
import net.rizon.plexus.Entity;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Message;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.Server;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Invite;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.messages.MessageMessage;
import net.rizon.plexus.messages.QuitMessage;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.commands.KillUser;
import net.rizon.plexus.replication.commands.MessageUser;
import net.rizon.plexus.replication.commands.QuitUser;

public class User implements Entity
{
	private final Plexus plexus;

	private String name;
	private String username;
	private String ip;
	private String cip;
	private String host;
	private String chost;
	private String vhost;
	private String gecos;
	private String away;
	private String account;
	private String certfp;
	private String authflags;
	private UUID id;
	private Server server;
	private String modes = "";
	private long signon;
	private long ts;

	private final Map<Channel, Membership> channels = new HashMap<>();

	private final List<Invite> invites = new ArrayList<>();

	/**
	 * accepts this user has or accepts this user is on
	 */
	private final List<Accept> accepts = new ArrayList<>();

	public User(Plexus plexus)
	{
		this.plexus = plexus;
	}

	protected void destroy()
	{
		// Remove all accepts on other users to this user
		clearAccepts();

		// Remove invites to this user, and null source of invites from this user
		clearInvites();

		for (Channel c : channels.keySet())
		{
			c.removeUser(this);
		}
		channels.clear();

		if (this.hasMode(plexus.getModeManager().getInvisible()))
		{
			plexus.getStats().decrementInvisible();
		}
	}

	@Override
	public String getSource()
	{
		return name + "!" + username + "@" + vhost;
	}

	public String getMaskRealhost()
	{
		return name + "!" + username + "@" + host;
	}

	@Override
	public void exit(String reason)
	{
		QuitUser quitUser = new QuitUser();
		quitUser.setUser(this.id);
		quitUser.setReason(reason);

		getPlexus().getClient().submit(quitUser);
	}

	public boolean isOper()
	{
		ModeManager modeManager = getPlexus().getModeManager();
		return hasMode(modeManager.getOper());
	}

	public boolean isAdmin()
	{
		ModeManager modeManager = getPlexus().getModeManager();
		return hasMode(modeManager.getAdmin());
	}

	public boolean isNetAdmin()
	{
		return isAdmin();//XXX hasMode(UserMode.netadmin);
	}

	public boolean isServices()
	{
		return false;
	}

	public boolean isLocal()
	{
		return false;
	}

	public void quit(String reason)
	{
		plexus.getWhowas().add(this);

		// Inform local clients that the user goes away
		this.sendCommon(this, new QuitMessage(this, reason));
		this.destroy();
	}

	public void kill(Entity source, String reason)
	{
		KillUser ku = new KillUser();
		ku.setSource(source.getId());
		ku.setUser(id);
		ku.setReason(reason);

		getPlexus().getClient().submit(ku);
	}

	public void changeNick(String newnick, long ts)
	{
		if (!IRC.equalsIgnoreCase(this.name, newnick))
		{
			/* clear this user from the accept list of other users */
			clearAcceptsFromOthers();
		}

		this.name = newnick;
		this.ts = ts;
	}

	public void sendCommon(User except, Message message)
	{
		long serial = plexus.getClientManager().getNextSerial();

		for (Channel c : channels.keySet())
		{
			for (LocalUser u : c.getLocalUsers())
			{
				if (u == except || u.getClient().getSerial() == serial)
				{
					continue;
				}

				u.writeMessage(message);
				u.getClient().setSerial(serial);
			}
		}
	}

	public ChannelFuture writeNotice(String message, Object... args)
	{
		MessageUser mu = new MessageUser();
		mu.setSource(getPlexus().getMe().getId());
		mu.setCommand(MessageMessage.Command.NOTICE);
		mu.setTarget(this.getId());
		mu.setMessage(String.format(message, args));

		getPlexus().getClient().submit(mu);

		return null;
	}

	@Override
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public String getCip()
	{
		return cip;
	}

	public void setCip(String cip)
	{
		this.cip = cip;
	}

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public String getChost()
	{
		return chost;
	}

	public void setChost(String chost)
	{
		this.chost = chost;
	}

	public String getVhost()
	{
		return vhost;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}

	public String getGecos()
	{
		return gecos;
	}

	public void setGecos(String gecos)
	{
		this.gecos = gecos;
	}

	public String getAway()
	{
		return away;
	}

	public void setAway(String away)
	{
		this.away = away;
	}

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public String getCertfp()
	{
		return certfp;
	}

	public void setCertfp(String certfp)
	{
		this.certfp = certfp;
	}

	public String getAuthflags()
	{
		return authflags;
	}

	public void setAuthflags(String authflags)
	{
		this.authflags = authflags;
	}

	@Override
	public UUID getId()
	{
		return id;
	}

	public void setId(UUID id)
	{
		this.id = id;
	}

	public Server getServer()
	{
		return server;
	}

	public void setServer(Server server)
	{
		this.server = server;
	}

	public String getModes()
	{
		return modes;
	}

	public void setModes(String modes)
	{
		// Don't store the +
		this.modes = modes.replace("+", "");
	}

	public long getSignon()
	{
		return signon;
	}

	public void setSignon(long signon)
	{
		this.signon = signon;
	}

	public long getTs()
	{
		return ts;
	}

	public void setTs(long ts)
	{
		this.ts = ts;
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	public Accept findAcceptFrom(User from)
	{
		for (Accept a : accepts)
		{
			if (a.getUser() == this && a.getFrom() == from)
			{
				return a;
			}
		}
		return null;
	}

	public void addAccept(Accept accept)
	{
		if (accepts.contains(accept))
		{
			return;
		}

		accepts.add(accept);
	}

	public void removeAccept(Accept accept)
	{
		accepts.remove(accept);
	}

	public List<Accept> getAccepts()
	{
		return accepts;
	}

	public void clearAcceptsFromOthers()
	{
		for (Accept accept : new ArrayList<>(accepts))
		{
			if (accept.getUser() == this)
			{
				continue;
			}

			assert accept.getFrom() == this;

			// Accept belongs to another user, remove from them
			accept.getUser().removeAccept(accept);
			// Remove from us
			removeAccept(accept);
		}
	}

	/**
	 * Remove all of the user's accepts, and all accepts of the user.
	 */
	public void clearAccepts()
	{
		for (Accept accept : accepts)
		{
			if (accept.getUser() == this)
			{
				continue;
			}

			// Accept belongs to another user
			accept.getUser().removeAccept(accept);
		}

		accepts.clear();
	}

	public void addInvite(Invite i)
	{
		if (invites.size() >= IRC.INVITE_HISTORY)
		{
			invites.remove(0);
		}

		invites.add(i);
	}

	public void removeInvite(Invite i)
	{
		invites.remove(i);
	}

	public Invite findInviteToChannel(Channel c)
	{
		for (Invite i : invites)
		{
			if (i.getChannel() == c && i.getTo() == this)
			{
				return i;
			}
		}
		return null;
	}

	private void clearInvites()
	{
		for (Invite i : new ArrayList<>(invites))
		{
			if (i.getTo() == this)
			{
				i.remove();
			}
			else if (i.getFrom() == this)
			{
				i.setFrom(null);
			}
		}
	}

	public void setMode(char mode)
	{
		if (hasMode(mode))
		{
			return;
		}

		char[] newModes = (modes + mode).toCharArray();
		Arrays.sort(newModes);
		modes = new String(newModes);
	}

	public boolean hasMode(UserMode mode)
	{
		return hasMode(mode.getCharacter());
	}

	public boolean hasMode(char mode)
	{
		return modes.contains("" + mode);
	}

	public void unsetMode(char mode)
	{
		modes = modes.replace("" + mode, "");
	}

	public void addChannel(Channel c, Membership mem)
	{
		channels.put(c, mem);
	}

	public void removeChannel(Channel c)
	{
		channels.remove(c);
	}

	public Membership findChannel(Channel c)
	{
		return channels.get(c);
	}

	public Collection<Membership> getChannels()
	{
		return channels.values();
	}
}
