/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.users;

import io.netty.channel.ChannelFuture;
import net.rizon.plexus.Client;
import net.rizon.plexus.Message;
import net.rizon.plexus.Messageable;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.commands.list.ListTask;
import net.rizon.plexus.config.Oper;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalUser extends User implements Messageable
{
	private static final Logger logger = LoggerFactory.getLogger(LocalUser.class);
	
	private Client client;
	private long idle = Util.currentTime(); // last time a message was sent
	private Oper oper; /* oline tied to the user, if they are an oper */
	private boolean captured; /* like shun, ignore all commands from this user */
	private ListTask listTask; /* if this user is /listing, the list info */
	private long lastNickChange;
	private int numberOfNickChanges;
	private long lastKnock;

	public LocalUser(Plexus plexus)
	{
		super(plexus);
		
		getPlexus().getClientManager().addLocalUser(this);
	}
	
	@Override
	protected void destroy()
	{
		if (this.listTask != null)
		{
			getPlexus().getListManager().remove(this);
			this.listTask = null;
		}
		
		ModeManager modeManager = getPlexus().getModeManager();
		if (hasMode(modeManager.getOper()))
			getPlexus().getClientManager().removeLocalOper(this);
		
		getPlexus().getClientManager().removeLocalUser(this);
		
		super.destroy();
	}
	
	@Override
	public void exit(String reason)
	{
		super.exit(reason);

		// for local clients we can immediately stop processing,
		// send ERROR, and queue a close
		this.client.error(reason);
	}
	
	@Override
	public boolean isLocal() { return true; }

	@Override
	public String getMessageName()
	{
		return this.getName();
	}

	@Override
	public ChannelFuture writeNumeric(Numeric numeric, Object... args)
	{
		return this.client.writeNumeric(numeric, args);
	}

	@Override
	public ChannelFuture writeNotice(String message, Object... args)
	{
		return this.client.writeNotice(message, args);
	}

	@Override
	public ChannelFuture writeMessage(Message message)
	{
		return this.client.writeMessage(message);
	}

	public Client getClient()
	{
		return client;
	}

	public void setClient(Client client)
	{
		this.client = client;
	}

	public long getIdle()
	{
		return idle;
	}

	public void setIdle(long idle)
	{
		this.idle = idle;
	}

	public Oper getOper()
	{
		return oper;
	}

	public void setOper(Oper oper)
	{
		this.oper = oper;
	}

	public boolean isCaptured()
	{
		return captured;
	}

	public void setCaptured(boolean captured)
	{
		this.captured = captured;
	}

	public ListTask getListTask()
	{
		return listTask;
	}

	public void setListTask(ListTask listTask)
	{
		this.listTask = listTask;
	}

	public long getLastNickChange()
	{
		return lastNickChange;
	}

	public void setLastNickChange(long lastNickChange)
	{
		this.lastNickChange = lastNickChange;
	}

	public int getNumberOfNickChanges()
	{
		return numberOfNickChanges;
	}

	public void setNumberOfNickChanges(int numberOfNickChanges)
	{
		this.numberOfNickChanges = numberOfNickChanges;
	}

	public long getLastKnock()
	{
		return lastKnock;
	}

	public void setLastKnock(long lastKnock)
	{
		this.lastKnock = lastKnock;
	}
}
