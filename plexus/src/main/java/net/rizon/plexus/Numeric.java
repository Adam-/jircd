/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import net.rizon.plexus.numerics.ErrAcceptFull;
import net.rizon.plexus.numerics.ErrAlreadyRegistered;
import net.rizon.plexus.numerics.ErrBadChanName;
import net.rizon.plexus.numerics.ErrBadChannelKey;
import net.rizon.plexus.numerics.ErrBanListIsFull;
import net.rizon.plexus.numerics.ErrBannedFromChan;
import net.rizon.plexus.numerics.ErrCanNotSendToChan;
import net.rizon.plexus.numerics.ErrCannotDoCommand;
import net.rizon.plexus.numerics.ErrChanBanReason;
import net.rizon.plexus.numerics.ErrChanOpPrivsNeeded;
import net.rizon.plexus.numerics.ErrChanOpen;
import net.rizon.plexus.numerics.ErrChannelIsFull;
import net.rizon.plexus.numerics.ErrErroneusNickname;
import net.rizon.plexus.numerics.ErrHelpNotFound;
import net.rizon.plexus.numerics.ErrInviteOnlyChan;
import net.rizon.plexus.numerics.ErrKnockOnChan;
import net.rizon.plexus.numerics.ErrListSyntax;
import net.rizon.plexus.numerics.ErrNeedMoreParams;
import net.rizon.plexus.numerics.ErrNeedReggedNick;
import net.rizon.plexus.numerics.ErrNickTooFast;
import net.rizon.plexus.numerics.ErrNicknameInUse;
import net.rizon.plexus.numerics.ErrNoCTCP;
import net.rizon.plexus.numerics.ErrNoCtrlsOnChan;
import net.rizon.plexus.numerics.ErrNoMOTD;
import net.rizon.plexus.numerics.ErrNoOperHost;
import net.rizon.plexus.numerics.ErrNoPrivileges;
import net.rizon.plexus.numerics.ErrNoPrivs;
import net.rizon.plexus.numerics.ErrNoSuchChannel;
import net.rizon.plexus.numerics.ErrNoSuchNick;
import net.rizon.plexus.numerics.ErrNonOnReg;
import net.rizon.plexus.numerics.ErrNotOnChannel;
import net.rizon.plexus.numerics.ErrOperOnlyChan;
import net.rizon.plexus.numerics.ErrPasswdMismatch;
import net.rizon.plexus.numerics.ErrTooManyChannels;
import net.rizon.plexus.numerics.ErrTooManyKnock;
import net.rizon.plexus.numerics.ErrUnknownCommand;
import net.rizon.plexus.numerics.ErrUnknownMode;
import net.rizon.plexus.numerics.ErrUserNotInChannel;
import net.rizon.plexus.numerics.ErrUserOnChannel;
import net.rizon.plexus.numerics.ErrUsersDontMatch;
import net.rizon.plexus.numerics.ErrWasNoSuchNick;
import net.rizon.plexus.numerics.ErrWrongPong;
import net.rizon.plexus.numerics.ErrYoureBannedCreep;
import net.rizon.plexus.numerics.RplAcceptList;
import net.rizon.plexus.numerics.RplAdminEmail;
import net.rizon.plexus.numerics.RplAdminLoc1;
import net.rizon.plexus.numerics.RplAdminLoc2;
import net.rizon.plexus.numerics.RplAdminMe;
import net.rizon.plexus.numerics.RplAway;
import net.rizon.plexus.numerics.RplBanList;
import net.rizon.plexus.numerics.RplChannelModeIs;
import net.rizon.plexus.numerics.RplCreated;
import net.rizon.plexus.numerics.RplCreationTime;
import net.rizon.plexus.numerics.RplEndOfAccept;
import net.rizon.plexus.numerics.RplEndOfBanList;
import net.rizon.plexus.numerics.RplEndOfExceptList;
import net.rizon.plexus.numerics.RplEndOfHelp;
import net.rizon.plexus.numerics.RplEndOfInfo;
import net.rizon.plexus.numerics.RplEndOfInvexList;
import net.rizon.plexus.numerics.RplEndOfMOTD;
import net.rizon.plexus.numerics.RplEndOfNames;
import net.rizon.plexus.numerics.RplEndOfStats;
import net.rizon.plexus.numerics.RplEndOfWho;
import net.rizon.plexus.numerics.RplEndOfWhois;
import net.rizon.plexus.numerics.RplEndOfWhowas;
import net.rizon.plexus.numerics.RplExceptList;
import net.rizon.plexus.numerics.RplGlobalUsers;
import net.rizon.plexus.numerics.RplHelpStart;
import net.rizon.plexus.numerics.RplHelpText;
import net.rizon.plexus.numerics.RplISupport;
import net.rizon.plexus.numerics.RplInfo;
import net.rizon.plexus.numerics.RplInviteList;
import net.rizon.plexus.numerics.RplInviting;
import net.rizon.plexus.numerics.RplIsCaptured;
import net.rizon.plexus.numerics.RplIsOn;
import net.rizon.plexus.numerics.RplIsUncaptured;
import net.rizon.plexus.numerics.RplKnockDlvr;
import net.rizon.plexus.numerics.RplLUserChannels;
import net.rizon.plexus.numerics.RplLUserClient;
import net.rizon.plexus.numerics.RplLUserMe;
import net.rizon.plexus.numerics.RplLUserOp;
import net.rizon.plexus.numerics.RplLUserUnknown;
import net.rizon.plexus.numerics.RplList;
import net.rizon.plexus.numerics.RplListEnd;
import net.rizon.plexus.numerics.RplListStart;
import net.rizon.plexus.numerics.RplLoad2Hi;
import net.rizon.plexus.numerics.RplLocalUsers;
import net.rizon.plexus.numerics.RplMOTD;
import net.rizon.plexus.numerics.RplMOTDStart;
import net.rizon.plexus.numerics.RplMyInfo;
import net.rizon.plexus.numerics.RplNamesReply;
import net.rizon.plexus.numerics.RplNoTopic;
import net.rizon.plexus.numerics.RplNowAway;
import net.rizon.plexus.numerics.RplRehashing;
import net.rizon.plexus.numerics.RplStatsCommands;
import net.rizon.plexus.numerics.RplStatsConn;
import net.rizon.plexus.numerics.RplStatsDLine;
import net.rizon.plexus.numerics.RplStatsDebug;
import net.rizon.plexus.numerics.RplStatsILine;
import net.rizon.plexus.numerics.RplStatsKLine;
import net.rizon.plexus.numerics.RplStatsOLine;
import net.rizon.plexus.numerics.RplStatsPLine;
import net.rizon.plexus.numerics.RplStatsUptime;
import net.rizon.plexus.numerics.RplStatsYLine;
import net.rizon.plexus.numerics.RplTargNotify;
import net.rizon.plexus.numerics.RplTargUModeG;
import net.rizon.plexus.numerics.RplTime;
import net.rizon.plexus.numerics.RplTopic;
import net.rizon.plexus.numerics.RplTopicWhoTime;
import net.rizon.plexus.numerics.RplUModeGMsg;
import net.rizon.plexus.numerics.RplUModeIs;
import net.rizon.plexus.numerics.RplUnAway;
import net.rizon.plexus.numerics.RplUserHost;
import net.rizon.plexus.numerics.RplVersion;
import net.rizon.plexus.numerics.RplWelcome;
import net.rizon.plexus.numerics.RplWhoReply;
import net.rizon.plexus.numerics.RplWhoisActually;
import net.rizon.plexus.numerics.RplWhoisCertFP;
import net.rizon.plexus.numerics.RplWhoisChannels;
import net.rizon.plexus.numerics.RplWhoisIdle;
import net.rizon.plexus.numerics.RplWhoisLoggedIn;
import net.rizon.plexus.numerics.RplWhoisModes;
import net.rizon.plexus.numerics.RplWhoisOperator;
import net.rizon.plexus.numerics.RplWhoisRealIP;
import net.rizon.plexus.numerics.RplWhoisRegNick;
import net.rizon.plexus.numerics.RplWhoisSSL;
import net.rizon.plexus.numerics.RplWhoisServer;
import net.rizon.plexus.numerics.RplWhoisUser;
import net.rizon.plexus.numerics.RplWhowasUser;
import net.rizon.plexus.numerics.RplYourHost;
import net.rizon.plexus.numerics.RplYoureOper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Numeric
{
	RPL_WELCOME(001, RplWelcome.class),
	RPL_YOURHOST(002, RplYourHost.class),
	RPL_CREATED(003, RplCreated.class),
	RPL_MYINFO(004, RplMyInfo.class),
	RPL_ISUPPORT(005, RplISupport.class),
	RPL_STATSCOMMANDS(212, RplStatsCommands.class),
	RPL_STATSILINE(215, RplStatsILine.class),
	RPL_STATSKLINE(216, RplStatsKLine.class),
	RPL_STATSYLINE(218, RplStatsYLine.class),
	RPL_ENDOFSTATS(219, RplEndOfStats.class),
	RPL_STATSPLINE(220, RplStatsPLine.class),
	RPL_UMODEIS(221, RplUModeIs.class),
	RPL_STATSDLINE(225, RplStatsDLine.class),
	RPL_STATSUPTIME(242, RplStatsUptime.class),
	RPL_STATSOLINE(243, RplStatsOLine.class),
	RPL_STATSDEBUG(249, RplStatsDebug.class),
	RPL_STATSCONN(250, RplStatsConn.class),
	RPL_LUSERCLIENT(251, RplLUserClient.class),
	RPL_LUSEROP(252, RplLUserOp.class),
	RPL_LUSERUNKNOWN(253, RplLUserUnknown.class),
	RPL_LUSERCHANNELS(254, RplLUserChannels.class),
	RPL_LUSERME(255, RplLUserMe.class),
	RPL_ADMINME(256, RplAdminMe.class),
	RPL_ADMINLOC1(257, RplAdminLoc1.class),
	RPL_ADMINLOC2(258, RplAdminLoc2.class),
	RPL_ADMINEMAIL(259, RplAdminEmail.class),
	RPL_LOAD2HI(263, RplLoad2Hi.class),
	RPL_LOCALUSERS(265, RplLocalUsers.class),
	RPL_GLOBALUSERS(266, RplGlobalUsers.class),
	RPL_WHOISCERTFP(276, RplWhoisCertFP.class),
	RPL_ACCEPTLIST(281, RplAcceptList.class),
	RPL_ENDOFACCEPT(282, RplEndOfAccept.class),
	RPL_AWAY(301, RplAway.class),
	RPL_USERHOST(302, RplUserHost.class),
	RPL_ISON(303, RplIsOn.class),
	RPL_UNAWAY(305, RplUnAway.class),
	RPL_NOWAWAY(306, RplNowAway.class),
	RPL_WHOISREGNICK(307, RplWhoisRegNick.class),
	RPL_WHOISMODES(310, RplWhoisModes.class),
	RPL_WHOISUSER(311, RplWhoisUser.class),
	RPL_WHOISSERVER(312, RplWhoisServer.class),
	RPL_WHOISOPERATOR(313, RplWhoisOperator.class),
	RPL_WHOWASUSER(314, RplWhowasUser.class),
	RPL_ENDOFWHO(315, RplEndOfWho.class),
	RPL_WHOISIDLE(317, RplWhoisIdle.class),
	RPL_ENDOFWHOIS(318, RplEndOfWhois.class),
	RPL_WHOISCHANNELS(319, RplWhoisChannels.class),
	RPL_LISTSTART(321, RplListStart.class),
	RPL_LIST(322, RplList.class),
	RPL_LISTEND(323, RplListEnd.class),
	RPL_CHANNELMODEIS(324, RplChannelModeIs.class),
	RPL_CREATIONTIME(329, RplCreationTime.class),
	RPL_WHOISLOGGEDIN(330, RplWhoisLoggedIn.class),
	RPL_NOTOPIC(331, RplNoTopic.class),
	RPL_TOPIC(332, RplTopic.class),
	RPL_TOPICWHOTIME(333, RplTopicWhoTime.class),
	RPL_WHOISACTUALLY(338, RplWhoisActually.class),
	RPL_INVITING(341, RplInviting.class),
	RPL_EXCEPTLIST(342, RplExceptList.class),
	RPL_INVITELIST(346, RplInviteList.class),
	RPL_ENDOFINVEXLIST(347, RplEndOfInvexList.class),
	RPL_ENDOFEXCEPTLIST(349, RplEndOfExceptList.class),
	RPL_VERSION(351, RplVersion.class),
	RPL_WHOREPLY(352, RplWhoReply.class),
	RPL_NAMEREPLY(353, RplNamesReply.class),
	RPL_ENDOFNAMES(366, RplEndOfNames.class),
	RPL_BANLIST(367, RplBanList.class),
	RPL_ENDOFBANLIST(368, RplEndOfBanList.class),
	RPL_ENDOFWHOWAS(369, RplEndOfWhowas.class),
	RPL_INFO(371, RplInfo.class),
	RPL_MOTD(372, RplMOTD.class),
	RPL_ENDOFINFO(374, RplEndOfInfo.class),
	RPL_MOTDSTART(375, RplMOTDStart.class),
	RPL_ENDOFMOTD(376, RplEndOfMOTD.class),
	RPL_YOUREOPER(381, RplYoureOper.class),
	RPL_REHASHING(382, RplRehashing.class),
	RPL_TIME(391, RplTime.class),
	ERR_NOSUCHNICK(401, ErrNoSuchNick.class),
	ERR_NOSUCHCHANNEL(403, ErrNoSuchChannel.class),
	ERR_CANNOTSENDTOCHAN(404, ErrCanNotSendToChan.class),
	ERR_TOOMANYCHANNELS(405, ErrTooManyChannels.class),
	ERR_WASNOSUCHNICK(406, ErrWasNoSuchNick.class),
	ERR_NOCTRLSONCHAN(408, ErrNoCtrlsOnChan.class),
	ERR_UNKNOWNCOMMAND(421, ErrUnknownCommand.class),
	ERR_NOMOTD(422, ErrNoMOTD.class),
	ERR_ERRONEUSNICKNAME(432, ErrErroneusNickname.class),
	ERR_NICKNAMEINUSE(433, ErrNicknameInUse.class),
	ERR_NICKTOOFAST(438, ErrNickTooFast.class),
	ERR_USERNOTINCHANNEL(441, ErrUserNotInChannel.class),
	ERR_NOTONCHANNEL(442, ErrNotOnChannel.class),
	ERR_USERONCHANNEL(443, ErrUserOnChannel.class),
	ERR_ACCEPTFULL(456, ErrAcceptFull.class),
	ERR_NEEDMOREPARAMS(461, ErrNeedMoreParams.class),
	ERR_ALREADYREGISTRED(462, ErrAlreadyRegistered.class),
	ERR_PASSWDMISMATCH(464, ErrPasswdMismatch.class),
	ERR_YOUREBANNEDCREEP(465, ErrYoureBannedCreep.class),
	ERR_CHANNELISFULL(471, ErrChannelIsFull.class),
	ERR_UNKNOWNMODE(472, ErrUnknownMode.class),
	ERR_INVITEONLYCHAN(473, ErrInviteOnlyChan.class),
	ERR_BANNEDFROMCHAN(474, ErrBannedFromChan.class),
	ERR_BADCHANNELKEY(475, ErrBadChannelKey.class),
	ERR_OPERONLYCHAN(476, ErrOperOnlyChan.class),
	ERR_NEEDREGGEDNICK(477, ErrNeedReggedNick.class),
	ERR_BANLISTFULL(478, ErrBanListIsFull.class),
	ERR_BADCHANNAME(479, ErrBadChanName.class),
	ERR_NOPRIVILEGES(481, ErrNoPrivileges.class),
	ERR_CHANOPRIVSNEEDED(482, ErrChanOpPrivsNeeded.class),
	ERR_CHANBANREASON(485, ErrChanBanReason.class),
	ERR_NONONREG(486, ErrNonOnReg.class),
	ERR_NOOPERHOST(491, ErrNoOperHost.class),
	ERR_NOCTCP(492, ErrNoCTCP.class),
	ERR_USERSDONTMATCH(502, ErrUsersDontMatch.class),
	ERR_WRONGPONG(513, ErrWrongPong.class),
	ERR_LISTSYNTAX(521, ErrListSyntax.class),
	ERR_HELPNOTFOUND(524, ErrHelpNotFound.class),
	RPL_WHOISSSL(671, RplWhoisSSL.class),
	RPL_WHOISREALIP(672, RplWhoisRealIP.class),
	RPL_HELPSTART(704, RplHelpStart.class),
	RPL_HELPTXT(705, RplHelpText.class),
	RPL_ENDOFHELP(706, RplEndOfHelp.class),
	RPL_KNOCKDLVR(711, RplKnockDlvr.class),
	ERR_TOOMANYKNOCK(712, ErrTooManyKnock.class),
	ERR_CHANOPEN(713, ErrChanOpen.class),
	ERR_KNOCKONCHAN(714, ErrKnockOnChan.class),
	RPL_TARGUMODEG(716, RplTargUModeG.class),
	RPL_TARGNOTIFY(717, RplTargNotify.class),
	RPL_UMODEGMSG(718, RplUModeGMsg.class),
	ERR_NOPRIVS(723, ErrNoPrivs.class),
	RPL_ISCAPTURED(727, RplIsCaptured.class),
	RPL_ISUNCAPTURED(728, RplIsUncaptured.class),
	ERR_CANNOTDOCOMMAND(972, ErrCannotDoCommand.class);

	private static final Logger logger = LoggerFactory.getLogger(Numeric.class);

	private final int numeric;
	private Class<? extends Message> clazz;

	Numeric(int numeric, Class<? extends Message> clazz)
	{
		this.numeric = numeric;
		this.clazz = clazz;
	}

	public int getNumeric()
	{
		return numeric;
	}

	public Class<? extends Message> getClazz()
	{
		return clazz;
	}

	public Message buildMessage(Entity source, Object... args)
	{
		Class<?>[] classes = new Class<?>[args.length + 1];
		Object[] objects = new Object[args.length + 1];

		classes[0] = Entity.class;
		objects[0] = source;

		for (int i = 0; i < args.length; ++i)
		{
			classes[i + 1] = args[i].getClass();
			objects[i + 1] = args[i];
		}

		try
		{
			Constructor<? extends Message> constructor = clazz.getConstructor(classes);
			return constructor.newInstance(objects);
		}
		catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
		{
			logger.warn("Unable to construct numeric", ex);
			return null;
		}
	}
}
