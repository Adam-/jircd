/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

import java.util.LinkedList;
import java.util.List;
import net.rizon.plexus.util.Util;

public class Auth implements Validatable
{
	private List<String> user;
	private String password;
	// XXX certfp
	private String spoof;
	private Allow allow;
	private List<String> flags;
	
	public boolean matches(String username, String host, String ip)
	{
		for (String s : user)
			if (Util.match(username + "@" + host, s) || Util.match(username + "@" + ip, s))
				return true;
		
		return false;
	}
	
	public String buildFlags()
	{
		StringBuilder sb = new StringBuilder();
		for (String s : flags)
		{
			switch (s)
			{
				case "need_password":
					sb.append('&');
					break;
				case "webirc":
					sb.append('?');
					break;
			}
		}
		return sb.toString();
	}
	
	@Override
	public void validate() throws ConfigException
	{
		if (allow == null)
			throw new ConfigException("auth:allow must be set to valid allow reference");
		
		if (user == null)
			user = new LinkedList<String>();
		if (flags == null)
			flags = new LinkedList<String>();
	}

	public List<String> getUser()
	{
		return user;
	}

	public void setUser(List<String> user)
	{
		this.user = user;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getSpoof()
	{
		return spoof;
	}

	public void setSpoof(String spoof)
	{
		this.spoof = spoof;
	}

	public Allow getAllow()
	{
		return allow;
	}

	public void setAllow(Allow allow)
	{
		this.allow = allow;
	}

	public List<String> getFlags()
	{
		return flags;
	}

	public void setFlags(List<String> flags)
	{
		this.flags = flags;
	}


}