/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

public class ServerHide
{
	private boolean disable_remote_commands;
	private boolean hide_servers;
	private String hidden_name;

	/**
	 * @return the disable_remote_commands
	 */
	public boolean isDisable_remote_commands()
	{
		return disable_remote_commands;
	}

	/**
	 * @param disable_remote_commands the disable_remote_commands to set
	 */
	public void setDisable_remote_commands(boolean disable_remote_commands)
	{
		this.disable_remote_commands = disable_remote_commands;
	}

	/**
	 * @return the hide_servers
	 */
	public boolean isHide_servers()
	{
		return hide_servers;
	}

	/**
	 * @param hide_servers the hide_servers to set
	 */
	public void setHide_servers(boolean hide_servers)
	{
		this.hide_servers = hide_servers;
	}

	/**
	 * @return the hidden_name
	 */
	public String getHidden_name()
	{
		return hidden_name;
	}

	/**
	 * @param hidden_name the hidden_name to set
	 */
	public void setHidden_name(String hidden_name)
	{
		this.hidden_name = hidden_name;
	}
}