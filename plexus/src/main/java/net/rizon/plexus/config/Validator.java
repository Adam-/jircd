/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

import java.util.Collection;
import net.rizon.plexus.IRC;

class Validator
{
	static <T> void validateNotNull(String name, T obj) throws ConfigException
	{
		if (obj == null)
			throw new ConfigException(name + " must be defined");
	}
	
	static void validateNotEmpty(String name, String obj) throws ConfigException
	{
		validateNotNull(name, obj);
		if (obj.isEmpty())
			throw new ConfigException(name + " must not be empty");
	}
	
	static <T extends Collection<?>> void validateNotEmpty(String name, T obj) throws ConfigException
	{
		validateNotNull(name, obj);
		if (obj.isEmpty())
			throw new ConfigException(name + " must not be empty");
	}
	
	static void validateNotZero(String name, int i) throws ConfigException
	{
		if (i == 0)
			throw new ConfigException(name + " must be non zero");
	}
	
	static void validateIP(String name, String ip) throws ConfigException
	{
		if (!IRC.isValidIP(ip))
			throw new ConfigException(name + " (" + ip + ") is not a valid IP");
	}
	
	static void validatePort(String name, int i) throws ConfigException
	{
		if (i < 0 || i > 65535)
			throw new ConfigException(name + " (" + i + ") is not a valid port");
	}
}