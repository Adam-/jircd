/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

import java.util.List;
import net.rizon.plexus.Client;

public class Config
{
	private ServerInfo serverinfo;
	private Admin admin;
	private List<Listen> listen;
	private List<Allow> allow;
	private List<Auth> auth;
	private List<Oper> oper;
	private General general;
	private ServerHide serverhide;
	private Channel channel;
	private SSL ssl;
	private Cluster cluster;
	
	private <T extends Validatable> void validateList(List<T> list) throws ConfigException
	{
		if (list == null)
			return;
		for (T t : list)
			t.validate();
	}
	
	public void validate() throws ConfigException
	{
		Validator.validateNotNull("serverinfo", serverinfo);
		Validator.validateNotNull("admin", admin);
		Validator.validateNotNull("general", general);
		Validator.validateNotNull("channel", channel);
		
		serverinfo.validate();
		admin.validate();
		
		validateList(listen);
		validateList(allow);
		validateList(auth);
		validateList(oper);
		
		general.validate();
		channel.validate();
	}

	public Oper findOLine(String name)
	{
		for (Oper o : oper)
			if (o.getName().equals(name))
				return o;
		return null;
	}
	
	public Auth findILine(Client client)
	{
		for (Auth a : auth)
		{
			if (a.getPassword() != null)
			{
				boolean match = client.getPassword() != null && client.getPassword().equals(a.getPassword()); // XXX crypto
				if (!match)
				{
					// If this block doesn't require a password we can just try the next block
					if (!a.getFlags().contains("need_password"))
						continue;
					client.exit("Bad password");
					return null;
				}
			}
			
			// certfp
			
			if (a.matches(client.getUsername(), client.getHost(), client.getIp()))
				return a;
		}
		
		return null;
	}

	public ServerInfo getServerinfo()
	{
		return serverinfo;
	}

	public void setServerinfo(ServerInfo serverinfo)
	{
		this.serverinfo = serverinfo;
	}

	public Admin getAdmin()
	{
		return admin;
	}

	public void setAdmin(Admin admin)
	{
		this.admin = admin;
	}

	public List<Listen> getListen()
	{
		return listen;
	}

	public void setListen(List<Listen> listen)
	{
		this.listen = listen;
	}

	public List<Allow> getAllow()
	{
		return allow;
	}

	public void setAllow(List<Allow> allow)
	{
		this.allow = allow;
	}

	public List<Auth> getAuth()
	{
		return auth;
	}

	public void setAuth(List<Auth> auth)
	{
		this.auth = auth;
	}

	public List<Oper> getOper()
	{
		return oper;
	}

	public void setOper(List<Oper> oper)
	{
		this.oper = oper;
	}

	public General getGeneral()
	{
		return general;
	}

	public void setGeneral(General general)
	{
		this.general = general;
	}

	public ServerHide getServerhide()
	{
		return serverhide;
	}

	public void setServerhide(ServerHide serverhide)
	{
		this.serverhide = serverhide;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}

	public SSL getSsl()
	{
		return ssl;
	}

	public void setSsl(SSL ssl)
	{
		this.ssl = ssl;
	}

	public Cluster getCluster()
	{
		return cluster;
	}

	public void setCluster(Cluster cluster)
	{
		this.cluster = cluster;
	}
}