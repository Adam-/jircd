/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

public class General implements Validatable
{
	private String user_umodes;
	private String user_chmodes;
	private boolean account_whois;
	private String modes_on_connect;
	private int ts_warn_delta;
	private int ts_max_delta;
	private boolean ping_cookie;
	private String cloak_key1;
	private String cloak_key2;
	private String cloak_key3;
	private boolean cloak_whois_actually;
	private int max_accept;
	private boolean opers_bypass_callerid;
	private boolean disable_auth;
	private int pace_wait_simple;
	private int pace_wait;
	private int max_nick_time;
	private int max_nick_changes;
	private String motd;
	private String ban_reason;
	private String oper_only_stats;
	private String admin_only_stats;
	
	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotNull("general:user_umodes", user_umodes);
		Validator.validateNotNull("general:user_chmodes", user_chmodes);
		
		if (modes_on_connect != null && modes_on_connect.indexOf("+") != -1)
			throw new ConfigException("general:modes_on_connect must not contain a +");
	}

	/**
	 * @return the user_umodes
	 */
	public String getUser_umodes()
	{
		return user_umodes;
	}

	/**
	 * @param user_umodes the user_umodes to set
	 */
	public void setUser_umodes(String user_umodes)
	{
		this.user_umodes = user_umodes;
	}

	/**
	 * @return the user_chmodes
	 */
	public String getUser_chmodes()
	{
		return user_chmodes;
	}

	/**
	 * @param user_chmodes the user_chmodes to set
	 */
	public void setUser_chmodes(String user_chmodes)
	{
		this.user_chmodes = user_chmodes;
	}

	/**
	 * @return the account_whois
	 */
	public boolean isAccount_whois()
	{
		return account_whois;
	}

	/**
	 * @param account_whois the account_whois to set
	 */
	public void setAccount_whois(boolean account_whois)
	{
		this.account_whois = account_whois;
	}

	/**
	 * @return the modes_on_connect
	 */
	public String getModes_on_connect()
	{
		return modes_on_connect;
	}

	/**
	 * @param modes_on_connect the modes_on_connect to set
	 */
	public void setModes_on_connect(String modes_on_connect)
	{
		this.modes_on_connect = modes_on_connect;
	}

	/**
	 * @return the ts_warn_delta
	 */
	public int getTs_warn_delta()
	{
		return ts_warn_delta;
	}

	/**
	 * @param ts_warn_delta the ts_warn_delta to set
	 */
	public void setTs_warn_delta(int ts_warn_delta)
	{
		this.ts_warn_delta = ts_warn_delta;
	}

	/**
	 * @return the ts_max_delta
	 */
	public int getTs_max_delta()
	{
		return ts_max_delta;
	}

	/**
	 * @param ts_max_delta the ts_max_delta to set
	 */
	public void setTs_max_delta(int ts_max_delta)
	{
		this.ts_max_delta = ts_max_delta;
	}

	/**
	 * @return the ping_cookie
	 */
	public boolean isPing_cookie()
	{
		return ping_cookie;
	}

	/**
	 * @param ping_cookie the ping_cookie to set
	 */
	public void setPing_cookie(boolean ping_cookie)
	{
		this.ping_cookie = ping_cookie;
	}

	/**
	 * @return the cloak_key1
	 */
	public String getCloak_key1()
	{
		return cloak_key1;
	}

	/**
	 * @param cloak_key1 the cloak_key1 to set
	 */
	public void setCloak_key1(String cloak_key1)
	{
		this.cloak_key1 = cloak_key1;
	}

	/**
	 * @return the cloak_key2
	 */
	public String getCloak_key2()
	{
		return cloak_key2;
	}

	/**
	 * @param cloak_key2 the cloak_key2 to set
	 */
	public void setCloak_key2(String cloak_key2)
	{
		this.cloak_key2 = cloak_key2;
	}

	/**
	 * @return the cloak_key3
	 */
	public String getCloak_key3()
	{
		return cloak_key3;
	}

	/**
	 * @param cloak_key3 the cloak_key3 to set
	 */
	public void setCloak_key3(String cloak_key3)
	{
		this.cloak_key3 = cloak_key3;
	}

	/**
	 * @return the cloak_whois_actually
	 */
	public boolean isCloak_whois_actually()
	{
		return cloak_whois_actually;
	}

	/**
	 * @param cloak_whois_actually the cloak_whois_actually to set
	 */
	public void setCloak_whois_actually(boolean cloak_whois_actually)
	{
		this.cloak_whois_actually = cloak_whois_actually;
	}

	/**
	 * @return the max_accept
	 */
	public int getMax_accept()
	{
		return max_accept;
	}

	/**
	 * @param max_accept the max_accept to set
	 */
	public void setMax_accept(int max_accept)
	{
		this.max_accept = max_accept;
	}

	/**
	 * @return the opers_bypass_callerid
	 */
	public boolean isOpers_bypass_callerid()
	{
		return opers_bypass_callerid;
	}

	/**
	 * @param opers_bypass_callerid the opers_bypass_callerid to set
	 */
	public void setOpers_bypass_callerid(boolean opers_bypass_callerid)
	{
		this.opers_bypass_callerid = opers_bypass_callerid;
	}

	/**
	 * @return the disable_auth
	 */
	public boolean isDisable_auth()
	{
		return disable_auth;
	}

	/**
	 * @param disable_auth the disable_auth to set
	 */
	public void setDisable_auth(boolean disable_auth)
	{
		this.disable_auth = disable_auth;
	}

	/**
	 * @return the pace_wait_simple
	 */
	public int getPace_wait_simple()
	{
		return pace_wait_simple;
	}

	/**
	 * @param pace_wait_simple the pace_wait_simple to set
	 */
	public void setPace_wait_simple(int pace_wait_simple)
	{
		this.pace_wait_simple = pace_wait_simple;
	}

	/**
	 * @return the pace_wait
	 */
	public int getPace_wait()
	{
		return pace_wait;
	}

	/**
	 * @param pace_wait the pace_wait to set
	 */
	public void setPace_wait(int pace_wait)
	{
		this.pace_wait = pace_wait;
	}

	/**
	 * @return the max_nick_time
	 */
	public int getMax_nick_time()
	{
		return max_nick_time;
	}

	/**
	 * @param max_nick_time the max_nick_time to set
	 */
	public void setMax_nick_time(int max_nick_time)
	{
		this.max_nick_time = max_nick_time;
	}

	/**
	 * @return the max_nick_changes
	 */
	public int getMax_nick_changes()
	{
		return max_nick_changes;
	}

	/**
	 * @param max_nick_changes the max_nick_changes to set
	 */
	public void setMax_nick_changes(int max_nick_changes)
	{
		this.max_nick_changes = max_nick_changes;
	}

	/**
	 * @return the motd
	 */
	public String getMotd()
	{
		return motd;
	}

	/**
	 * @param motd the motd to set
	 */
	public void setMotd(String motd)
	{
		this.motd = motd;
	}

	/**
	 * @return the ban_reason
	 */
	public String getBan_reason()
	{
		return ban_reason;
	}

	/**
	 * @param ban_reason the ban_reason to set
	 */
	public void setBan_reason(String ban_reason)
	{
		this.ban_reason = ban_reason;
	}

	/**
	 * @return the oper_only_stats
	 */
	public String getOper_only_stats()
	{
		return oper_only_stats;
	}

	/**
	 * @param oper_only_stats the oper_only_stats to set
	 */
	public void setOper_only_stats(String oper_only_stats)
	{
		this.oper_only_stats = oper_only_stats;
	}

	/**
	 * @return the admin_only_stats
	 */
	public String getAdmin_only_stats()
	{
		return admin_only_stats;
	}

	/**
	 * @param admin_only_stats the admin_only_stats to set
	 */
	public void setAdmin_only_stats(String admin_only_stats)
	{
		this.admin_only_stats = admin_only_stats;
	}
}