/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

import java.util.List;
import net.rizon.plexus.util.Util;

public class Oper implements Validatable
{
	private String name;
	private List<String> user;
	private String password;
	private String umodes;
	private List<String> privileges;
	private Allow allow;
	
	public boolean matchesHost(String ident, String host) //XXX cidr
	{
		for (String u : user)
		{
			int i = u.indexOf('@');
			if (i == -1)
			{
				if (Util.match(host, u))
					return true;
			}
			else
			{
				String id = u.substring(0, i), h = u.substring(i + 1);
				if (Util.match(ident,  id) && Util.match(host, h))
					return true;
			}
		}
		
		return false;
	}
	
	public boolean hasPriv(String priv)
	{
		return privileges.contains(priv);
	}
	
	public String getPrivs()
	{
		StringBuilder sb = new StringBuilder();
		for (String s : privileges)
			switch (s)
			{
				case "admin":
					sb.append('A');
					break;
			}
		return sb.toString();
	}
	
	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("oper:name", name);
		Validator.validateNotEmpty("oper:user", user);
		Validator.validateNotEmpty("oper:umodes", umodes);
		Validator.validateNotNull("oper:privileges", privileges);
		Validator.validateNotNull("oper:allow", allow);
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the user
	 */
	public List<String> getUser()
	{
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(List<String> user)
	{
		this.user = user;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return the umodes
	 */
	public String getUmodes()
	{
		return umodes;
	}

	/**
	 * @param umodes the umodes to set
	 */
	public void setUmodes(String umodes)
	{
		this.umodes = umodes;
	}

	/**
	 * @return the privileges
	 */
	public List<String> getPrivileges()
	{
		return privileges;
	}

	/**
	 * @param privileges the privileges to set
	 */
	public void setPrivileges(List<String> privileges)
	{
		this.privileges = privileges;
	}

	/**
	 * @return the allow
	 */
	public Allow getAllow()
	{
		return allow;
	}

	/**
	 * @param allow the allow to set
	 */
	public void setAllow(Allow allow)
	{
		this.allow = allow;
	}
}