/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

import java.util.Collections;
import java.util.List;

public class Cluster
{
	private String address;
	private boolean bootstrap;
	private List<String> addresses = Collections.EMPTY_LIST;
	private int electionTimeout;
	private int heartbeatInterval;
	private int sessionTimeout;

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public boolean isBootstrap()
	{
		return bootstrap;
	}

	public void setBootstrap(boolean bootstrap)
	{
		this.bootstrap = bootstrap;
	}

	public List<String> getAddresses()
	{
		return addresses;
	}

	public void setAddresses(List<String> addresses)
	{
		this.addresses = addresses;
	}

	public int getElectionTimeout()
	{
		return electionTimeout;
	}

	public void setElectionTimeout(int electionTimeout)
	{
		this.electionTimeout = electionTimeout;
	}

	public int getHeartbeatInterval()
	{
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(int heartbeatInterval)
	{
		this.heartbeatInterval = heartbeatInterval;
	}

	public int getSessionTimeout()
	{
		return sessionTimeout;
	}

	public void setSessionTimeout(int sessionTimeout)
	{
		this.sessionTimeout = sessionTimeout;
	}


}
