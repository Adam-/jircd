/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

public class Allow implements Validatable
{
	private String name;
	private int ping_time;
	private int max_channels;
	private int sendq;
	private int recv_allow;
	private int recv_duration;
	private int recv_limit;
	
	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("allow:name", name);
		Validator.validateNotZero("allow:ping_time", ping_time);
		Validator.validateNotZero("allow:max_channels", max_channels);
		Validator.validateNotZero("allow:sendq", sendq);
		Validator.validateNotZero("allow:recv_allow", recv_allow);
		Validator.validateNotZero("allow:recv_duration", recv_duration);
		Validator.validateNotZero("allow:recv_limit", recv_limit);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getPing_time()
	{
		return ping_time;
	}

	public void setPing_time(int ping_time)
	{
		this.ping_time = ping_time;
	}

	public int getMax_channels()
	{
		return max_channels;
	}

	public void setMax_channels(int max_channels)
	{
		this.max_channels = max_channels;
	}

	public int getSendq()
	{
		return sendq;
	}

	public void setSendq(int sendq)
	{
		this.sendq = sendq;
	}

	public int getRecv_allow()
	{
		return recv_allow;
	}

	public void setRecv_allow(int recv_allow)
	{
		this.recv_allow = recv_allow;
	}

	public int getRecv_duration()
	{
		return recv_duration;
	}

	public void setRecv_duration(int recv_duration)
	{
		this.recv_duration = recv_duration;
	}

	public int getRecv_limit()
	{
		return recv_limit;
	}

	public void setRecv_limit(int recv_limit)
	{
		this.recv_limit = recv_limit;
	}

}