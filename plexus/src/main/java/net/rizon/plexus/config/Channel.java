/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

public class Channel implements Validatable
{
	private boolean disable_fake_channels;
	private boolean quiet_on_ban;
	private String modes_on_create;
	private int knock_delay;
	private int knock_delay_channel;
	private int max_bans;
	
	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotNull("channel:modes_on_create", modes_on_create);
	}

	public boolean isDisable_fake_channels()
	{
		return disable_fake_channels;
	}

	public void setDisable_fake_channels(boolean disable_fake_channels)
	{
		this.disable_fake_channels = disable_fake_channels;
	}

	public boolean isQuiet_on_ban()
	{
		return quiet_on_ban;
	}

	public void setQuiet_on_ban(boolean quiet_on_ban)
	{
		this.quiet_on_ban = quiet_on_ban;
	}

	public String getModes_on_create()
	{
		return modes_on_create;
	}

	public void setModes_on_create(String modes_on_create)
	{
		this.modes_on_create = modes_on_create;
	}

	public int getKnock_delay()
	{
		return knock_delay;
	}

	public void setKnock_delay(int knock_delay)
	{
		this.knock_delay = knock_delay;
	}

	public int getKnock_delay_channel()
	{
		return knock_delay_channel;
	}

	public void setKnock_delay_channel(int knock_delay_channel)
	{
		this.knock_delay_channel = knock_delay_channel;
	}

	public int getMax_bans()
	{
		return max_bans;
	}

	public void setMax_bans(int max_bans)
	{
		this.max_bans = max_bans;
	}


}