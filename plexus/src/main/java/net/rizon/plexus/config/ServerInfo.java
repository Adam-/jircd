/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.config;

import net.rizon.plexus.IRC;

public class ServerInfo implements Validatable
{
	private String name;
	private String description;
	private String network_name;
	private String network_desc;
	
	@Override
	public void validate() throws ConfigException
	{
		if (!IRC.isValidServerName(name))
			throw new ConfigException(name + " is not a valid server name");
		Validator.validateNotEmpty("serverinfo:description", description);
		Validator.validateNotEmpty("serverinfo:network_name", network_name);
		Validator.validateNotEmpty("serverinfo:network_desc", network_desc);
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @return the network_name
	 */
	public String getNetwork_name()
	{
		return network_name;
	}

	/**
	 * @param network_name the network_name to set
	 */
	public void setNetwork_name(String network_name)
	{
		this.network_name = network_name;
	}

	/**
	 * @return the network_desc
	 */
	public String getNetwork_desc()
	{
		return network_desc;
	}

	/**
	 * @param network_desc the network_desc to set
	 */
	public void setNetwork_desc(String network_desc)
	{
		this.network_desc = network_desc;
	}
};