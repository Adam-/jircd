/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.io;

import java.util.Arrays;
import java.util.Objects;
import net.rizon.plexus.util.Mask;
import net.rizon.plexus.util.MaskParser;

public class IRCMessage
{
	private String source;
	private String command;
	private String[] params;

	public IRCMessage()
	{
	}

	public IRCMessage(String source, String command, String... params)
	{
		this.source = source;
		this.command = command;
		this.params = params;
	}

	public String getSource()
	{
		return source;
	}

	public String getNick()
	{
		Mask m = MaskParser.parse(source);
		return m.getNick();
	}

	public String getCommand()
	{
		return command;
	}

	public String[] getParams()
	{
		return params;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public void setCommand(String command)
	{
		this.command = command;
	}

	public void setParams(String[] params)
	{
		this.params = params;
	}

	public void push(String param)
	{
		if (params == null)
		{
			params = new String[] { param };
			return;
		}
		
		params = Arrays.copyOf(params, params.length + 1);
		params[params.length - 1] = param;
	}

	public void push(String... params)
	{
		for (String s : params)
			push(s);
	}

	public boolean isNumeric()
	{
		return command.length() == 3 && Character.isDigit(command.charAt(0)) && Character.isDigit(command.charAt(1)) && Character.isDigit(command.charAt(2));
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		if (source != null)
			sb.append('[').append(source).append("] ");
		
		sb.append(command);

		if (params != null)
		{
			for (int i = 0; i < params.length; ++i)
			{
				sb.append(' ');
				if (i + 1 == params.length)
					sb.append(':');
				sb.append(params[i]);
			}
		}
		
		return sb.toString();
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 73 * hash + Objects.hashCode(this.source);
		hash = 73 * hash + Objects.hashCode(this.command);
		hash = 73 * hash + Arrays.deepHashCode(this.params);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final IRCMessage other = (IRCMessage) obj;
		if (!Objects.equals(this.source, other.source))
		{
			return false;
		}
		if (!Objects.equals(this.command, other.command))
		{
			return false;
		}
		if (!Arrays.deepEquals(this.params, other.params))
		{
			return false;
		}
		return true;
	}
}
