/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.io;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.TooLongFrameException;
import net.rizon.plexus.Client;
import net.rizon.plexus.Plexus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientHandler extends SimpleChannelInboundHandler<IRCMessage>
{
	private static final Logger logger = LoggerFactory.getLogger(ClientHandler.class);

	private final Plexus plexus;
	private Client client;

	public ClientHandler(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public Client getClient()
	{
		return client;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception
	{
		synchronized (plexus.getIrc())
		{
			assert client == null;

			plexus.getStats().incrementTotalConnections();

			try
			{
				client = new Client(plexus, ctx.channel());
			}
			catch (Exception ex)
			{
				logger.error("Unable to accept new connections", ex);
				return;
			}
			
			plexus.getClientManager().insertClient(client);

			client.startAuth();
		}
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception
	{
		synchronized (plexus.getIrc())
		{
			client.exit("Connection closed");
			
			plexus.getClientManager().removeClient(client);
		}
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IRCMessage message) throws Exception
	{
		synchronized (plexus.getIrc())
		{
			client.onRead(message);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
	{
		if (cause instanceof TooLongFrameException)
		{
			// thrown by LineBasedFrameDecoder
			logger.debug("Too long of frame from " + client, cause);
			return;
		}

		logger.error("Exception while processing client " + client, cause);
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception
	{
		if (!(evt instanceof ThrottleLimitExceededEvent))
		{
			return;
		}

		client.exit("Excess Flood");
	}
}
