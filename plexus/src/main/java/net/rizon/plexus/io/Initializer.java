/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.io;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LineBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslHandler;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.time.Duration;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Plexus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Initializer extends ChannelInitializer<SocketChannel>
{
	private static final Logger logger = LoggerFactory.getLogger(Initializer.class);
	
	private static final Duration DURATION = Duration.ofSeconds(IRC.DEFAULT_RECV_DURATION);

	private final Plexus plexus;
	private boolean ssl;
	
	public Initializer(Plexus plexus, boolean ssl)
	{
		this.plexus = plexus;
		this.ssl = ssl;
	}
	
	@Override
	protected void initChannel(SocketChannel ch) throws Exception
	{
		ChannelPipeline pipeline = ch.pipeline();
		
		if (ssl)
		{
			SSLContext ctx = init();
			if (ctx == null)
			{
				logger.error("Unable to setup SSL channel");
				return;
			}
			
			SSLEngine engine = ctx.createSSLEngine();
			engine.setUseClientMode(false);
			engine.setWantClientAuth(true); // Allows clients to send us a cert
			
			pipeline.addLast("ssl", new SslHandler(engine));
		}
		
		pipeline.addLast("frameDecoder", new LineBasedFrameDecoder(IRC.MAXBUF));
		pipeline.addLast("stringDecoder", new StringDecoder());
		pipeline.addLast("ircDecoder", new IRCDecoder());
		pipeline.addLast("throttle", new ThrottleHandler(IRC.DEFAULT_RECV_ALLOW_READ, DURATION, IRC.DEFAULT_RECV_QUEUE_LENGTH));

		pipeline.addLast("stringEncoder", new StringEncoder());
		pipeline.addLast("frameEncoder", new LineBasedFrameEncoder());
		pipeline.addLast("ircEncoder", new IRCEncoder());

		//pipeline.addLast("idleStateHandler", new IdleStateHandler(60, 0, 0));

		ClientHandler handler = new ClientHandler(plexus);

		pipeline.addLast(new InboundLoggingHandler(handler));
		pipeline.addLast(new OutboundLoggingHandler(handler));
		
		pipeline.addLast("clientHandler", handler);

	}
	
	private SSLContext init()
	{
		net.rizon.plexus.config.SSL conf = plexus.getConf().getSsl();
		
		if (conf == null)
		{
			logger.info("SSL is not configured, not enabling");
			return null;
		}
		
		try
		{
			KeyStore ks = KeyStore.getInstance("JKS");
			try (FileInputStream kIs = new FileInputStream(conf.getKeystore()))
			{
				ks.load(kIs, conf.getPassword().toCharArray());
			}
			
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, conf.getPassword().toCharArray());
			
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(kmf.getKeyManagers(), new TrustManager[] { new TrustManager() }, null);
			return ctx;
		}
		catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException | CertificateException | IOException | UnrecoverableKeyException e)
		{
			logger.error("Unable to initialize SSL", e);
		}
		
		return null;
	}

}
