/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.io;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.util.Arrays;
import java.util.List;
import net.rizon.plexus.IRC;

@Sharable
public class IRCDecoder extends MessageToMessageDecoder<String>
{
	@Override
	protected void decode(ChannelHandlerContext ctx, String message, List<Object> out) throws Exception
	{
		String source = null;
		String command;
		String[] params = new String[IRC.MAXARGS];

		int pos = 0;
		int argc = 0;

		if (message.startsWith(":"))
		{
			pos = message.indexOf(' ');

			if (pos == -1)
			{
				pos = message.length();
			}

			source = message.substring(1, pos);
		}

		while (pos < message.length() && message.charAt(pos) == ' ')
		{
			++pos;
		}

		if (pos == message.length())
		{
			return;
		}

		int next = message.indexOf(' ', pos);
		if (next == -1)
		{
			next = message.length();
		}

		command = message.substring(pos, next);
		pos = next;

		for (;;)
		{
			while (pos < message.length() && message.charAt(pos) == ' ')
			{
				++pos;
			}

			if (pos == message.length())
			{
				break;
			}

			if (message.charAt(pos) == ':')
			{
				++pos;
				next = message.length();
			}
			else
			{
				next = message.indexOf(' ', pos);
				if (next == -1)
				{
					next = message.length();
				}
			}

			String param = message.substring(pos, next);
			pos = next;

			params[argc++] = param;

			if (argc >= IRC.MAXARGS)
			{
				break;
			}
		}

		params = Arrays.copyOf(params, argc, String[].class);

		IRCMessage ircMessage = new IRCMessage(source, command, params);
		out.add(ircMessage);
	}
}
