/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.io;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.concurrent.ScheduledFuture;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThrottleHandler extends SimpleChannelInboundHandler<IRCMessage>
{
	private static final Logger logger = LoggerFactory.getLogger(ThrottleHandler.class);

	/**
	 * number of messages allowed to be read in duration
	 */
	private int allow;
	/**
	 * duration
	 */
	private Duration duration;
	/**
	 * max size queue is allowed to grow before the exceed event is is fired
	 */
	private int queueLimit;

	/**
	 * number of messages read so far
	 */
	private int num;
	/**
	 * last time num was reset
	 */
	private Instant last = Instant.now();

	private final Deque<IRCMessage> queue = new ArrayDeque<>();
	private ScheduledFuture<?> floodTask;

	public ThrottleHandler(int allow, Duration duration, int queueLimit)
	{
		this.allow = allow;
		this.duration = duration;
		this.queueLimit = queueLimit;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IRCMessage message) throws Exception
	{
		checkResetNum();

		if (num >= allow)
		{
			if (queue.size() >= queueLimit)
			{
				logger.debug("Throttle limit exceeded");

				ctx.fireUserEventTriggered(new ThrottleLimitExceededEvent());
				return;
			}

			queue.push(message);

			if (floodTask != null)
			{
				logger.debug("Queueing message {} because limit has been reached", message);
				return;
			}

			assert queue.size() == 1;

			logger.debug("Scheduling flood task for queued message {}", message);

			// schedule callback
			EventExecutor loop = ctx.executor();
			floodTask = loop.schedule(new FloodTask(ctx), 1, TimeUnit.SECONDS);

			return;
		}

		if (queue.isEmpty())
		{
			++num;

			ctx.fireChannelRead(message);
			return;
		}

		logger.debug("Queueing message {} ({} others)", message, queue.size());

		queue.add(message);

		// let the flood task process this later
		assert floodTask != null;
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception
	{
		if (floodTask != null)
		{
			floodTask.cancel(false);
			floodTask = null;
		}

		super.channelInactive(ctx);
	}

	private void checkResetNum()
	{
		Instant now = Instant.now();
		Duration difference = Duration.between(last, now);

		if (difference.compareTo(duration) > -1)
		{
			num = 0;
			last = now;
		}
	}

	private void process(ChannelHandlerContext ctx)
	{
		while (num < allow)
		{
			if (queue.isEmpty())
			{
				break;
			}

			IRCMessage message = queue.pop();
			++num;

			logger.debug("Processing throttled message {}", message);

			ctx.fireChannelRead(message);
		}
	}

	protected final class FloodTask implements Runnable
	{
		private final ChannelHandlerContext ctx;

		public FloodTask(ChannelHandlerContext ctx)
		{
			this.ctx = ctx;
		}

		@Override
		public void run()
		{
			floodTask = null;

			if (!ctx.channel().isOpen())
			{
				return;
			}

			logger.debug("Processing flood task");

			checkResetNum();
			process(ctx);

			if (queue.isEmpty() == false)
			{
				logger.debug("Rescheduling flood task ({} other messages)", queue.size());

				// schedule another try
				EventExecutor loop = ctx.executor();
				floodTask = loop.schedule(this, 1, TimeUnit.SECONDS);
			}
		}

	}

	public int getAllow()
	{
		return allow;
	}

	public void setAllow(int allow)
	{
		this.allow = allow;
	}

	public Duration getDuration()
	{
		return duration;
	}

	public void setDuration(Duration duration)
	{
		this.duration = duration;
	}

	public int getQueueLimit()
	{
		return queueLimit;
	}

	public void setQueueLimit(int queueLimit)
	{
		this.queueLimit = queueLimit;
	}
}
