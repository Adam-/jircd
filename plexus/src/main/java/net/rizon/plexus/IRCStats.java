/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

public class IRCStats
{
	private long totalSentMessages;
	private long totalRecvMessages;

	/**
	 * number of invisible clients.
	 */
	private int invisible;

	private int maxLocal;
	private int maxTotal;

	/**
	 * total number of connections received.
	 */
	private int totalConnections;

	/**
	 * max number of open connections.
	 */
	private int maxConnections;

	public long getTotalSentMessages()
	{
		return totalSentMessages;
	}

	public long getTotalRecvMessages()
	{
		return totalRecvMessages;
	}

	public void incrementTotalSentMessages()
	{
		++totalSentMessages;
	}

	public void incrementTotalRecvMessages()
	{
		++totalRecvMessages;
	}

	public int getInvisible()
	{
		return invisible;
	}

	public void incrementInvisible()
	{
		++invisible;
	}

	public void decrementInvisible()
	{
		--invisible;
	}

	public int getMaxLocal()
	{
		return maxLocal;
	}

	public void setMaxLocal(int maxLocal)
	{
		this.maxLocal = maxLocal;
	}

	public int getMaxTotal()
	{
		return maxTotal;
	}

	public void setMaxTotal(int maxTotal)
	{
		this.maxTotal = maxTotal;
	}

	public int getTotalConnections()
	{
		return totalConnections;
	}

	public void incrementTotalConnections()
	{
		++totalConnections;
	}

	public int getMaxConnections()
	{
		return maxConnections;
	}

	public void setMaxConnections(int maxConnections)
	{
		this.maxConnections = maxConnections;
	}
}
