/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import io.atomix.copycat.server.session.ServerSession;
import java.util.ArrayList;
import java.util.UUID;
import net.rizon.plexus.replication.commands.QuitServer;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server implements Entity
{
	private static final Logger logger = LoggerFactory.getLogger(Server.class);
	
	private Plexus plexus;

	private ServerSession session;
	private UUID id;
	private String name = "unknown.";
	private String description = "unknown";

	public Server(Plexus plexus)
	{
		this.plexus = plexus;
	}

	@Override
	public String toString()
	{
		return "Server{" + "session=" + session + ", id=" + id + ", name=" + name + ", description=" + description + '}';
	}

	public ServerSession getSession()
	{
		return session;
	}

	public void setSession(ServerSession session)
	{
		this.session = session;
	}

	@Override
	public UUID getId()
	{
		return id;
	}

	public void setId(UUID id)
	{
		this.id = id;
	}

	@Override
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public String getSource()
	{
		return name;
	}

	@Override
	public void exit(String reason)
	{
		QuitServer qs = new QuitServer();
		qs.setSource(plexus.getMe().getId());
		qs.setUuid(id);
		qs.setReason(reason);

		plexus.getClient().submit(qs);
	}

	public void quit(Entity source, String reason)
	{
		// quit all users on the server
		for (User user : new ArrayList<>(plexus.getIrc().getUsers().values()))
		{
			if (user.getServer() != this)
			{
				continue;
			}

			logger.debug("User {} quits because of quitting server {}", user, this);

			// the server quit events assume all users on the server
			// quit, so don't need to broadcast this
			user.quit(reason);
			plexus.getIrc().removeUser(user);
		}
	}

	public boolean isHidden()
	{
		return false;
	}
}
