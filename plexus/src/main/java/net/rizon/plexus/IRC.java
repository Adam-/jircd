/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.users.User;
import net.rizon.plexus.util.Util;
import org.apache.commons.validator.routines.InetAddressValidator;

public class IRC
{
	public static final int NICKLEN = 30,
		USERLEN = 10,
		HOSTLEN = 63,
		CHANLEN = 50,
		DEFAULT_MAX_CHANNELS = 75,
		INVITE_HISTORY = 10,
		MAXBUF = 512,
		SIDLEN = 3,
		UIDLEN = 9,
		MAXMODEPARAMS = 4,
		KICKLEN = 160,
		CONNECT_TIMEOUT = 30,
		AWAYLEN = 160,
		DEFAULT_RECV_ALLOW_READ = 16, // default number of messages that can be processed per DEFAULT_DURATION
		DEFAULT_RECV_DURATION = 1,
		DEFAULT_RECV_QUEUE_LENGTH = 16, // number of messages that can build up before excess flood
		DEFAULT_SENDQ = 1024, // Sendq for unrecognized clients with no iline
		SO_BACKLOG = 128,
		MAXARGS = 15;
	
	private final Plexus plexus;

	public IRC(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public static boolean match(String str, String pattern)
	{
		return Util.match(toUpperCase(str), toUpperCase(pattern));
	}

	public static char toUpperCase(char c)
	{
		if (c == '{' || c == '}' || c == '|' || (c >= 'a' && c <= 'z'))
		{
			return (char) (c - 32);
		}
		else
		{
			return c;
		}
	}

	public static char toLowerCase(char c)
	{
		if (c == '[' || c == ']' || c == '\\' || (c >= 'A' && c <= 'Z'))
		{
			return (char) (c + 32);
		}
		else
		{
			return c;
		}
	}

	public static String toUpperCase(String s)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); ++i)
		{
			sb.append(toUpperCase(s.charAt(i)));
		}
		return sb.toString();
	}

	public static String toLowerCase(String s)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); ++i)
		{
			sb.append(toLowerCase(s.charAt(i)));
		}
		return sb.toString();
	}

	public static boolean equalsIgnoreCase(String s1, String s2)
	{
		return toUpperCase(s1).equalsIgnoreCase(toUpperCase(s2));
	}

	public static boolean isLetter(char c)
	{
		return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
	}

	public static boolean isDigit(char c)
	{
		return c >= '0' && c <= '9';
	}

	public static boolean isLetterOrDigit(char c)
	{
		return isLetter(c) || isDigit(c);
	}

	public static boolean isValidNick(String nick)
	{
		if (nick.isEmpty() || nick.length() > NICKLEN)
		{
			return false;
		}

		for (int i = 0; i < nick.length(); ++i)
		{
			char c = nick.charAt(i);
			if (!isLetter(c) && "[]\\`_^{|}".indexOf(c) == -1 && (i == 0 || (!isDigit(c) && c != '-')))
			{
				return false;
			}
		}

		return true;
	}

	public static boolean isValidUsername(String user)
	{
		if (user.isEmpty() || user.length() > USERLEN)
		{
			return false;
		}

		if (!isLetterOrDigit(user.charAt(0)))
		{
			return false;
		}

		for (int i = 1; i < user.length(); ++i)
		{
			char c = user.charAt(i);
			if (!isLetterOrDigit(c) && "$-.[\\]^_`{|}".indexOf(c) == -1)
			{
				return false;
			}
		}

		return true;
	}

	public static boolean isValidHostname(String host, boolean color)
	{
		if (host.isEmpty() || host.startsWith(":") || host.startsWith(".") || host.length() > HOSTLEN)
		{
			return false;
		}

		for (int i = 0; i < host.length(); ++i)
		{
			char c = host.charAt(i);

			if (color && (c == 2 || c == 3 || c == 15 || c == 31))
			{
				continue;
			}

			if (!isLetterOrDigit(c) && "-.:".indexOf(c) == -1)
			{
				return false;
			}
		}

		return true;
	}

	public static boolean isValidIP(String ip)
	{
		if (ip.isEmpty() || ip.startsWith(":"))
		{
			return false;
		}

		return InetAddressValidator.getInstance().isValid(ip);
	}

	public boolean isValidChannel(String channel)
	{
		if (channel.isEmpty() || channel.length() > CHANLEN)
		{
			return false;
		}

		if (channel.charAt(0) != '#')
		{
			return false;
		}

		for (int i = 1; i < channel.length(); ++i)
		{
			char c = channel.charAt(i);
			if (c == 7 || c == ',' || c == ' ')
			{
				return false;
			}

			if (plexus.getConf().getChannel().isDisable_fake_channels())
			{
				switch (c)
				{
					case 2: // bold
					case 3: // color
					case 15: // plain text 
					case 22: // reverse
					case 29: // italic
					case 31: // underline
					case 160: // nbsp
						return false;
				}
			}
		}

		return true;
	}

	public static boolean isValidServerName(String sname)
	{
		return sname.indexOf('.') > 0 && isLetter(sname.charAt(0));
	}

	private final Map<String, User> users = new HashMap<>();
	private final Map<UUID, User> usersById = new HashMap<>();
	private final Map<String, Channel> channels = new HashMap<>();
	private final Map<UUID, Server> servers = new HashMap<>();

	public void addUser(User user)
	{
		users.put(IRC.toLowerCase(user.getName()), user);
		usersById.put(user.getId(), user);
	}

	public void removeUser(User user)
	{
		users.remove(IRC.toLowerCase(user.getName()));
		usersById.remove(user.getId());
	}

	public User findUser(String name)
	{
		return users.get(IRC.toLowerCase(name));
	}

	public User findUser(UUID id)
	{
		return usersById.get(id);
	}

	public void addChannel(Channel channel)
	{
		channels.put(IRC.toLowerCase(channel.getName()), channel);
	}

	public void removeChannel(Channel channel)
	{
		channels.remove(IRC.toLowerCase(channel.getName()));
	}

	public Channel findChannel(String name)
	{
		return channels.get(IRC.toLowerCase(name));
	}
	
	public void addServer(Server server)
	{
		servers.put(server.getId(), server);
	}
	
	public void removeServer(Server server)
	{
		servers.remove(server.getId());
	}
	
	public Server findServer(UUID id)
	{
		return servers.get(id);
	}

	public Map<String, User> getUsers()
	{
		return users;
	}

	public Map<String, Channel> getChannels()
	{
		return channels;
	}

	public Map<UUID, Server> getServers()
	{
		return servers;
	}

	public Entity findEntity(UUID uid)
	{
		User u = findUser(uid);
		if (u != null)
			return u;

		Server s = findServer(uid);
		if (s != null)
			return s;

		return null;
	}
	
	public void clear()
	{
		users.clear();
		usersById.clear();
		channels.clear();
		servers.clear();

		Server me = plexus.getMe();
		addServer(me);
	}
}
