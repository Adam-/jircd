/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.command;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.commands.Accept;
import net.rizon.plexus.commands.Away;
import net.rizon.plexus.commands.Cap;
import net.rizon.plexus.commands.Capture;
import net.rizon.plexus.commands.ChgHost;
import net.rizon.plexus.commands.ChgIdent;
import net.rizon.plexus.commands.DLine;
import net.rizon.plexus.commands.Die;
import net.rizon.plexus.commands.Globops;
import net.rizon.plexus.commands.Help;
import net.rizon.plexus.commands.Info;
import net.rizon.plexus.commands.Invite;
import net.rizon.plexus.commands.IsOn;
import net.rizon.plexus.commands.Join;
import net.rizon.plexus.commands.KLine;
import net.rizon.plexus.commands.Kick;
import net.rizon.plexus.commands.Kill;
import net.rizon.plexus.commands.Knock;
import net.rizon.plexus.commands.LUsers;
import net.rizon.plexus.commands.List;
import net.rizon.plexus.commands.Locops;
import net.rizon.plexus.commands.MOTD;
import net.rizon.plexus.commands.Mode;
import net.rizon.plexus.commands.Names;
import net.rizon.plexus.commands.Nick;
import net.rizon.plexus.commands.Notice;
import net.rizon.plexus.commands.Oper;
import net.rizon.plexus.commands.Operwall;
import net.rizon.plexus.commands.Part;
import net.rizon.plexus.commands.Pass;
import net.rizon.plexus.commands.Ping;
import net.rizon.plexus.commands.Pong;
import net.rizon.plexus.commands.Privmsg;
import net.rizon.plexus.commands.Protoctl;
import net.rizon.plexus.commands.Quit;
import net.rizon.plexus.commands.Rehash;
import net.rizon.plexus.commands.Resv;
import net.rizon.plexus.commands.Stats;
import net.rizon.plexus.commands.Time;
import net.rizon.plexus.commands.Topic;
import net.rizon.plexus.commands.UnDLine;
import net.rizon.plexus.commands.UnKLine;
import net.rizon.plexus.commands.UnResv;
import net.rizon.plexus.commands.Uncapture;
import net.rizon.plexus.commands.Userhost;
import net.rizon.plexus.commands.Users;
import net.rizon.plexus.commands.Version;
import net.rizon.plexus.commands.Wallops;
import net.rizon.plexus.commands.WebIRC;
import net.rizon.plexus.commands.Who;
import net.rizon.plexus.commands.Whois;
import net.rizon.plexus.commands.Whowas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandManager
{
	private static final Logger logger = LoggerFactory.getLogger(CommandManager.class);

	private final Plexus plexus;
	private final Map<String, Class<? extends Command>> commands = new HashMap<>();
	private final Map<Class<? extends Command>, CommandStats> commandStats = new HashMap<>();

	public CommandManager(Plexus plexus)
	{
		this.plexus = plexus;
		
		insert("Accept", Accept.class);
		insert("Away", Away.class);
		insert("Cap", Cap.class);
		insert("Capture", Capture.class);
		insert("ChgHost", ChgHost.class);
		insert("ChgIdent", ChgIdent.class);
		insert("Die", Die.class);
		insert("DLine", DLine.class);
		insert("Error", net.rizon.plexus.commands.Error.class);
		insert("Globops", Globops.class);
		insert("Help", Help.class);
		insert("Info", Info.class);
		insert("Invite", Invite.class);
		insert("IsOn", IsOn.class);
		insert("Join", Join.class);
		insert("Kick", Kick.class);
		insert("Kill", Kill.class);
		insert("KLine", KLine.class);
		insert("Knock", Knock.class);
		insert("Locops", Locops.class);
		insert("List", List.class);
		insert("LUsers", LUsers.class);
		insert("Mode", Mode.class);
		insert("MOTD", MOTD.class);
		insert("Names", Names.class);
		insert("Nick", Nick.class);
		insert("Notice", Notice.class);
		insert("Oper", Oper.class);
		insert("Operwall", Operwall.class);
		insert("Part", Part.class);
		insert("Pass", Pass.class);
		insert("Ping", Ping.class);
		insert("Pong", Pong.class);
		insert("Privmsg", Privmsg.class);
		insert("Protoctl", Protoctl.class);
		insert("Quit", Quit.class);
		insert("Rehash", Rehash.class);
		insert("Resv", Resv.class);
		insert("Stats", Stats.class);
		insert("Time", Time.class);
		insert("Topic", Topic.class);
		insert("Uncapture", Uncapture.class);
		insert("UnDLine", UnDLine.class);
		insert("UnKLine", UnKLine.class);
		insert("UnResv", UnResv.class);
		insert("User", net.rizon.plexus.commands.User.class);
		insert("Userhost", Userhost.class);
		insert("Users", Users.class);
		insert("Version", Version.class);
		insert("Wallops", Wallops.class);
		insert("WebIRC", WebIRC.class);
		insert("Who", Who.class);
		insert("Whois", Whois.class);
		insert("Whowas", Whowas.class);
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	private void insert(String name, Class<? extends Command> clazz)
	{
		commands.put(name.toUpperCase(), clazz);
		commandStats.put(clazz, new CommandStats(name, clazz));
	}

	public Command findCommand(String name)
	{
		try
		{
			Class<? extends Command> clazz = commands.get(name.toUpperCase());
			if (clazz == null)
			{
				return null;
			}

			Command command = clazz.newInstance();
			command.setManager(this);
			command.setPlexus(plexus);
			return command;
		}
		catch (InstantiationException | IllegalAccessException ex)
		{
			logger.warn("Unable to create command " + name, ex);
			return null;
		}
	}
	
	public Collection<CommandStats> getStats()
	{
		return commandStats.values();
	}
	
	public CommandStats getStats(Class<? extends Command> commandClass)
	{
		return commandStats.get(commandClass);
	}
}
