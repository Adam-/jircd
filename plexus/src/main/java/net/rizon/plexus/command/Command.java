/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.command;

import java.lang.reflect.Field;
import java.util.Arrays;
import net.rizon.plexus.Client;
import net.rizon.plexus.Entity;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Command
{
	private static final Logger logger = LoggerFactory.getLogger(Command.class);

	private Plexus plexus;
	private final String name;
	private CommandManager manager;

	private boolean requiresOper;

	private boolean allowEmptyLastArg;
	private boolean worksThroughCapture;

	public Command(final String name)
	{
		this.name = name;
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	public String getName()
	{
		return name;
	}

	public void setPlexus(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public CommandManager getManager()
	{
		return manager;
	}

	public void setManager(CommandManager manager)
	{
		this.manager = manager;
	}

	public boolean isRequiresOper()
	{
		return requiresOper;
	}

	public final void setRequiresOper(boolean requiresOper)
	{
		this.requiresOper = requiresOper;
	}

	public boolean isAllowEmptyLastArg()
	{
		return allowEmptyLastArg;
	}

	public final void setAllowEmptyLastArg(boolean allowEmptyLastArg)
	{
		this.allowEmptyLastArg = allowEmptyLastArg;
	}

	public boolean isWorksThroughCapture()
	{
		return worksThroughCapture;
	}

	public final void setWorksThroughCapture(boolean worksThroughCapture)
	{
		this.worksThroughCapture = worksThroughCapture;
	}

	public int getGlobalDelay()
	{
		return 0;
	}

	public void executeUnregistered(Client client)
	{
	}

	public abstract void execute(LocalUser user);

	private Field[] getFields()
	{
		return Arrays.stream(this.getClass().getDeclaredFields())
			.filter(f -> f.getAnnotation(Parameter.class) != null)
			.sorted((f1, f2) -> Integer.compare(f1.getAnnotation(Parameter.class).index(), f2.getAnnotation(Parameter.class).index()))
			.toArray(i -> new Field[i]);
	}

	public void unpack(String[] params)
	{
		Field[] fields = getFields();

		for (int paramCount = 0, fieldCount = 0; paramCount < params.length && fieldCount < fields.length; ++paramCount, ++fieldCount)
		{
			Field f = fields[fieldCount];
			Class<?> type = f.getType();

			f.setAccessible(true);

			Parameter parameter = f.getAnnotation(Parameter.class);

			try
			{
				Object value;

				if (type == Entity.class)
				{
					value = getPlexus().getIrc().findUser(params[paramCount]);
				}
				else if (User.class.isAssignableFrom(type))
				{
					value = getPlexus().getIrc().findUser(params[paramCount]);
				}
//				else if (Server.class.isAssignableFrom(type))
//				{
//					value = getPlexus().getIrc().findServer(params[paramCount]);
//				}
				else if (type == Channel.class)
				{
					value = getPlexus().getIrc().findChannel(params[paramCount]);
				}
				else if (parameter.targets())
				{
					value = params[paramCount].split(",");
				}
				else if (type == String[].class)
				{
					value = Arrays.copyOfRange(params, paramCount, params.length);
					f.set(this, value);
					break;
				}
				else if (type == Integer.class)
				{
					try
					{
						value = Integer.valueOf(params[paramCount]);
					}
					catch (NumberFormatException e)
					{
						value = null;
					}
				}
				else if (type == Long.class)
				{
					try
					{
						value = Long.valueOf(params[paramCount]);
					}
					catch (NumberFormatException e)
					{
						value = null;
					}
				}
				else
				{
					value = params[paramCount];
				}

				f.set(this, value);
			}
			catch (IllegalArgumentException | IllegalAccessException ex)
			{
				logger.warn(null, ex);
			}
		}
	}

	public boolean hasRequiredFields()
	{
		Field[] fields = getFields();

		for (Field f : fields)
		{
			Parameter p = f.getAnnotation(Parameter.class);
			f.setAccessible(true);

			try
			{
				Object value = f.get(this);

				if (p.required() && value == null)
				{
					return false;
				}

				if (!this.isAllowEmptyLastArg() && value instanceof String && ((String) value).isEmpty())
				{
					return false;
				}
			}
			catch (IllegalArgumentException | IllegalAccessException ex)
			{
				logger.warn(null, ex);
			}
		}

		return true;
	}
}
