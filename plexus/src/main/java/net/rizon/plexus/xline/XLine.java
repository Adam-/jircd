/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.xline;

import java.util.Objects;
import net.rizon.plexus.Client;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.users.LocalUser;

public abstract class XLine
{
	private final XLineManager manager;
	private final XLineType type;
	
	private String mask;
	private String creator;
	private String reason;
	private String oreason;
	private long created;
	private long expires;

	public XLine(XLineManager manager, XLineType type)
	{
		this.manager = manager;
		this.type = type;
	}

	@Override
	public String toString()
	{
		return "XLine{" + "manager=" + manager + ", type=" + type + ", mask=" + mask + ", creator=" + creator + ", reason=" + reason + ", oreason=" + oreason + ", created=" + created + ", expires=" + expires + '}';
	}

	@Override
	public int hashCode()
	{
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.type);
		hash = 53 * hash + Objects.hashCode(this.mask);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final XLine other = (XLine) obj;
		if (!Objects.equals(this.mask, other.mask))
		{
			return false;
		}
		if (this.type != other.type)
		{
			return false;
		}
		return true;
	}

	public XLineManager getManager()
	{
		return manager;
	}

	public Plexus getPlexus()
	{
		return manager.getPlexus();
	}

	public XLineType getType()
	{
		return type;
	}

	public String getMask()
	{
		return mask;
	}

	public void setMask(String mask)
	{
		this.mask = mask;
	}

	public String getCreator()
	{
		return creator;
	}

	public void setCreator(String creator)
	{
		this.creator = creator;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public String getOreason()
	{
		return oreason;
	}

	public void setOreason(String oreason)
	{
		this.oreason = oreason;
	}

	public long getCreated()
	{
		return created;
	}

	public void setCreated(long created)
	{
		this.created = created;
	}

	public long getExpires()
	{
		return expires;
	}

	public void setExpires(long expires)
	{
		this.expires = expires;
	}
	
	public abstract boolean matches(LocalUser u);
	public abstract boolean matches(Client c);
	public abstract void onExpire();
	public abstract void onMatch(LocalUser u);
	public abstract void onMatch(Client c);
}