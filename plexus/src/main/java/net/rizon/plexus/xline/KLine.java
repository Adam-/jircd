/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.xline;

import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.users.LocalUser;

public class KLine extends XLine
{
	private String user;
	private String host;
	
	public KLine(XLineManager manager)
	{
		super(manager, XLineType.KLINE);
	}

	public String getUser()
	{
		return user;
	}

	public String getHost()
	{
		return host;
	}
	
	@Override
	public void setMask(String mask)
	{
		super.setMask(mask);
		
		int i = mask.indexOf('@');
		if (i != -1)
		{
			this.user = mask.substring(0, i);
			this.host = mask.substring(i + 1);
		}
		else
		{
			this.user = "*";
			this.host = mask;
		}
	}
	
	private boolean matches(String username, String ip, String host)
	{
		return IRC.match(username, this.user) && (IRC.match(ip, this.host) || IRC.match(host, this.host));
	}

	@Override
	public boolean matches(LocalUser u)
	{
		return matches(u.getUsername(), u.getIp(), u.getHost());
	}

	@Override
	public boolean matches(Client c)
	{
		return matches(c.getUsername(), c.getIp(), c.getHost());
	}
	
	@Override
	public void onExpire()
	{
		getPlexus().getClientManager().sendToOps(null, "Temporary k-line for [%s@%s] expired", user, host);
	}

	@Override
	public void onMatch(LocalUser u)
	{
		getManager().ban(u, this);
	}
	
	@Override
	public void onMatch(Client client)
	{
		getManager().ban(client, this);
	}
}