/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.xline;

import com.google.common.base.Strings;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import net.rizon.plexus.Client;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XLineManager
{
	private static final Logger logger = LoggerFactory.getLogger(XLineManager.class);
	
	private final Plexus plexus;
	
	private final List<XLine> xlines = new ArrayList<>();

	public XLineManager(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	public void init()
	{
		Runnable r = () ->
		{
			try
			{
				expire();
			}
			catch (Throwable ex)
			{
				logger.warn(null, ex);
			}
		};
		plexus.getWorkerGroup().scheduleAtFixedRate(r, 60, 60, TimeUnit.SECONDS);
	}
	
	public void addXline(XLine xline)
	{
		xlines.add(xline);
	}
	
	public void removeXLine(XLine xline)
	{
		xlines.remove(xline);
	}
	
	public XLine findXLine(XLine xline)
	{
		Optional<XLine> o = xlines.stream().filter(x -> xline.equals(x)).findAny();
		return o.isPresent() ? o.get() : null;
	}
	
	public XLine findXline(XLineType type, String mask)
	{
		for (XLine xline : xlines)
		{
			if (xline.getType() != type)
				continue;
			
			if (xline.getMask().equals(mask)) // XXX?
				return xline;
		}
		
		return null;
	}

	public List<XLine> getXlines()
	{
		return xlines;
	}
	
	public List<XLine> getXlinesOfType(XLineType type)
	{
		return xlines.stream().filter(x -> x.getType() == type).collect(Collectors.toList());
	}
	
	public void apply(XLine xl)
	{
		for (LocalUser lu : plexus.getClientManager().getLocalUsers())
		{
			if (xl.matches(lu))
			{
				xl.onMatch(lu);
			}
		}
		
		for (Client c : plexus.getClientManager().getClients())
		{
			if (c.getUser() != null)
				continue;
			
			if (xl.matches(c))
			{
				xl.onMatch(c);
			}
		}
	}

	public void expire()
	{
		long now = Util.currentTime();
		for (Iterator<XLine> it = xlines.iterator(); it.hasNext();)
		{
			XLine xline = it.next();
			
			if (xline.getExpires() == 0 || xline.getExpires() > now)
				continue;
			
			xline.onExpire();
			it.remove();
		}
	}
	
	public XLine findXLine(Client client)
	{
		for (XLine xline : xlines)
		{
			if (xline.matches(client))
			{
				return xline;
			}
		}
		
		return null;
	}

	public void ban(LocalUser user, XLine xl)
	{
		getPlexus().getClientManager().sendToOps(null, "%s active for %s", xl.getType(), user.getMaskRealhost());

		user.writeNumeric(Numeric.ERR_YOUREBANNEDCREEP, xl.getReason());

		String reason = plexus.getConf().getGeneral().getBan_reason();
		if (Strings.isNullOrEmpty(reason))
		{
			reason = xl.getReason();
		}
		
		user.exit(reason);
	}
	
	public void ban(Client client, XLine xl)
	{
		client.exit(xl.getReason());
	}
}
