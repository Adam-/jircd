/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.util;

public class Util
{
	public static boolean match(String str, String pattern)
	{
		int strIdx = 0, wildIdx = 0;
		int cp = 0, mp = 0;

		while (strIdx < str.length() && wildIdx < pattern.length() && pattern.charAt(wildIdx) != '*')
		{
			if (str.charAt(strIdx) != pattern.charAt(wildIdx) && pattern.charAt(wildIdx) != '?')
			{
				return false;
			}

			++wildIdx;
			++strIdx;
		}

		while (strIdx < str.length() && wildIdx < pattern.length())
		{
			if (pattern.charAt(wildIdx) == '*')
			{
				++wildIdx;

				if (wildIdx >= pattern.length())
				{
					return true;
				}

				mp = wildIdx;
				cp = strIdx + 1;
			}
			else if (pattern.charAt(wildIdx) == str.charAt(strIdx) || pattern.charAt(wildIdx) == '?')
			{
				++wildIdx;
				++strIdx;
			}
			else
			{
				wildIdx = mp;
				strIdx = cp++;
			}
		}

		while (wildIdx < pattern.length() && pattern.charAt(wildIdx) == '*')
		{
			++wildIdx;
		}

		return wildIdx >= pattern.length();
	}

	public static int getSeconds(String time) throws NumberFormatException
	{
		int rtime;

		if (time.endsWith("h"))
		{
			time = time.substring(0, time.length() - 1);
			rtime = Integer.parseInt(time);
			rtime = rtime * 60 * 60;
			return rtime;
		}
		else if (time.endsWith("m"))
		{
			time = time.substring(0, time.length() - 1);
			rtime = Integer.parseInt(time);
			rtime = rtime * 60;
			return rtime;
		}
		else if (time.endsWith("d"))
		{
			time = time.substring(0, time.length() - 1);
			rtime = Integer.parseInt(time);
			rtime = rtime * 24 * 60 * 60;
			return rtime;
		}
		else
		{
			rtime = Integer.parseInt(time);
		}

		return rtime;
	}

	public static <T> String toString(T[] items, String sep)
	{
		StringBuffer sb = new StringBuffer();
		for (T i : items)
		{
			if (sb.length() > 0)
			{
				sb.append(sep);
			}
			sb.append(i);
		}
		return sb.toString();
	}

	public static <T> String toString(T[] items)
	{
		return toString(items, " ");
	}

	public static <T> String toString(Iterable<T> items)
	{
		return toString(items, " ");
	}

	public static <T> String toString(Iterable<T> items, String sep)
	{
		StringBuffer sb = new StringBuffer();
		for (T i : items)
		{
			if (sb.length() > 0)
			{
				sb.append(sep);
			}
			sb.append(i);
		}
		return sb.toString();
	}

	public static long currentTime()
	{
		return System.currentTimeMillis() / 1000L;
	}

	public static String base16Encode(byte[] in)
	{
		char base16[] = "0123456789ABCDEF".toCharArray();
		StringBuilder sb = new StringBuilder();

		for (byte b : in)
		{
			sb.append(base16[(b & 0xF0) >> 4]);
			sb.append(base16[b & 0xF]);
		}

		return sb.toString();
	}
}
