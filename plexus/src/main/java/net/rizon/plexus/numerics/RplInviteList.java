/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.numerics;

import net.rizon.plexus.Entity;
import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;

public class RplInviteList extends Message
{
	@Parameter(index = 0)
	private final String channel;

	@Parameter(index = 1)
	private final String mask;

	@Parameter(index = 2)
	private final String creator;

	@Parameter(index = 3)
	private final long when;

	public RplInviteList(Entity source, String channel, String nick, String user, String host, String creator, Long when)
	{
		super(source, Numeric.RPL_INVITELIST);

		this.channel = channel;
		this.mask = nick + "!" + user + "@" + host;
		this.creator = creator;
		this.when = when;
	}

	@Override
	public String toString()
	{
		return "RplInviteList{" + "channel=" + channel + ", mask=" + mask + ", creator=" + creator + ", when=" + when + '}';
	}

	public String getChannel()
	{
		return channel;
	}

	public String getMask()
	{
		return mask;
	}

	public String getCreator()
	{
		return creator;
	}

	public long getWhen()
	{
		return when;
	}
}
