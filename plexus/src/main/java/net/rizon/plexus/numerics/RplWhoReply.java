/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.numerics;

import net.rizon.plexus.Entity;
import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;

public class RplWhoReply extends Message
{
	@Parameter(index = 0)
	private final String channel;

	@Parameter(index = 1)
	private final String username;

	@Parameter(index = 2)
	private final String host;

	@Parameter(index = 3)
	private final String server;

	@Parameter(index = 4)
	private final String name;

	@Parameter(index = 5)
	private final String status;

	@Parameter(index = 6) // XXX this is the last param
	private final int hopcount;

	@Parameter(index = 7)
	private final String info;

	public RplWhoReply(Entity source, String channel, String username, String host, String server, String name, String status, Integer hopcount, String info)
	{
		super(source, Numeric.RPL_WHOREPLY);

		this.channel = channel;
		this.username = username;
		this.host = host;
		this.server = server;
		this.name = name;
		this.status = status;
		this.hopcount = hopcount;
		this.info = info;
	}

	@Override
	public String toString()
	{
		return "RplWhoReply{" + "channel=" + channel + ", username=" + username + ", host=" + host + ", server=" + server + ", name=" + name + ", status=" + status + ", hopcount=" + hopcount + ", info=" + info + '}';
	}

	public String getChannel()
	{
		return channel;
	}

	public String getUsername()
	{
		return username;
	}

	public String getHost()
	{
		return host;
	}

	public String getServer()
	{
		return server;
	}

	public String getName()
	{
		return name;
	}

	public String getStatus()
	{
		return status;
	}

	public int getHopcount()
	{
		return hopcount;
	}

	public String getInfo()
	{
		return info;
	}
}
