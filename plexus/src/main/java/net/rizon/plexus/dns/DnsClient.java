/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.dns;

import io.netty.channel.AddressedEnvelope;
import io.netty.channel.EventLoop;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.handler.codec.dns.DefaultDnsPtrRecord;
import io.netty.handler.codec.dns.DefaultDnsQuestion;
import io.netty.handler.codec.dns.DnsQuestion;
import io.netty.handler.codec.dns.DnsRecordType;
import io.netty.handler.codec.dns.DnsResponse;
import io.netty.handler.codec.dns.DnsSection;
import io.netty.resolver.dns.DnsNameResolver;
import io.netty.resolver.dns.DnsNameResolverBuilder;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.Promise;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DnsClient
{
	private static final Logger logger = LoggerFactory.getLogger(DnsClient.class);

	private static final String HEX = "0123456789abcdef";

	private final EventLoop eventLoop;

	private DnsNameResolver resolver;

	public DnsClient(EventLoop eventLoop)
	{
		this.eventLoop = eventLoop;

		DnsNameResolverBuilder builder = new DnsNameResolverBuilder(eventLoop);
		builder.channelType(NioDatagramChannel.class);

		resolver = builder.build();
	}

	public Future<InetAddress> resolveForward(String host)
	{
		logger.debug("Resolving {}", host);

		Future<InetAddress> future = resolver.resolve(host);
		return future;
	}

	public Future<String> resolveReverse(InetAddress ip)
	{
		String reverse = toPtr(ip);

		logger.debug("Reverse lookup for {} ({})", ip, reverse);
		
		DnsQuestion dnsQuestion = new DefaultDnsQuestion(reverse, DnsRecordType.PTR);
		Promise<String> promise = eventLoop.newPromise();

		Future<AddressedEnvelope<DnsResponse, InetSocketAddress>> future = resolver.query(dnsQuestion);
		future.addListener((Future<AddressedEnvelope<DnsResponse, InetSocketAddress>> f)
			->
			{
				try
				{
					AddressedEnvelope<DnsResponse, InetSocketAddress> ae = f.get();
					DnsResponse response = ae.content();

					int count = response.count(DnsSection.ANSWER);
					if (count <= 0)
					{
						promise.trySuccess(null);
						return;
					}

					DefaultDnsPtrRecord record = (DefaultDnsPtrRecord) response.recordAt(DnsSection.ANSWER, 0);
					String hostname = record.hostname();

					if (hostname == null || hostname.isEmpty())
					{
						promise.trySuccess(null);
						return;
					}

					// remove trailing .
					hostname = hostname.substring(0, hostname.length() - 1);

					promise.trySuccess(hostname);
				}
				catch (InterruptedException | ExecutionException ex)
				{
					promise.tryFailure(ex);
				}

		});

		return promise;
	}

	private String toPtr(InetAddress ip)
	{
		if (ip instanceof Inet6Address)
		{
			StringBuilder sb = new StringBuilder();
			byte[] addr = ip.getAddress();

			for (int j = 15; j >= 0; --j)
			{
				sb.append(HEX.charAt(addr[j] & 0xF));
				sb.append('.');
				sb.append(HEX.charAt((addr[j] & 0xFF) >>> 4));
				sb.append('.');
			}
			
			return sb.toString() + "ip6.arpa";
		}
		else
		{
			byte[] addr = ip.getAddress();
			return (addr[3] & 0xFF) + "." + (addr[2] & 0xFF) + "." + (addr[1] & 0xFF) + "." + (addr[0] & 0xFF) + ".in-addr.arpa";
		}
	}
}
