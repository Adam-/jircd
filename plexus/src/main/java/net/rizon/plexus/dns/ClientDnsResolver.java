/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.dns;

import io.netty.util.concurrent.Future;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutionException;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientDnsResolver
{
	private static final Logger logger = LoggerFactory.getLogger(ClientDnsResolver.class);

	private static final String LOOKING_UP_HOSTNAME = "*** Looking up your hostname...";
	private static final String FAILED_TO_LOOKUP = "*** Couldn't look up your hostname";
	private static final String HOSTNAME_MISMATCH = "*** Your forward and reverse DNS do not match, ignoring hostname.";
	private static final String HOSTNAME_FOUND = "*** Found your hostname";
	private static final String HOSTNAME_TOO_LONG = "*** Your hostname is too long, ignoring hostname";

	private final Client client;
	private final DnsClient dnsClient;
	private final Runnable finish;

	private Future<?> future;
	private String reverse;

	public ClientDnsResolver(Client client, Runnable finish)
	{
		this.client = client;
		dnsClient = client.getPlexus().getDnsClient();
		this.finish = finish;
	}

	public void start()
	{
		client.writeNotice(LOOKING_UP_HOSTNAME);

		InetSocketAddress remoteAddress = (InetSocketAddress) client.getChannel().remoteAddress();

		future = dnsClient.resolveReverse(remoteAddress.getAddress())
			.addListener((Future<String> future) -> reverseComplete(future));
	}

	public void cancel()
	{
		if (future != null)
			future.cancel(true);
	}

	private void reverseComplete(Future<String> future)
	{
		if (client.isExited())
			return;

		if (!future.isSuccess())
		{
			logger.warn(null, future.cause());

			client.writeNotice(FAILED_TO_LOOKUP);
			finish.run();
			return;
		}

		try
		{
			reverse = future.get();
		}
		catch (InterruptedException | ExecutionException ex)
		{
			logger.warn(null, ex);

			client.writeNotice(FAILED_TO_LOOKUP);
			finish.run();
			return;
		}

		if (reverse == null)
		{
			client.writeNotice(FAILED_TO_LOOKUP);
			finish.run();
			return;
		}

		logger.debug("Reverse address for {} resolved to {}", client, reverse);

		// Check forward
		this.future = dnsClient.resolveForward(reverse)
			.addListener((Future<InetAddress> f) -> forwardComplete(f));
	}

	private void forwardComplete(Future<InetAddress> future)
	{
		if (client.isExited())
			return;
		
		if (!future.isSuccess())
		{
			logger.warn(null, future.cause());

			client.writeNotice(FAILED_TO_LOOKUP);
			finish.run();
			return;
		}

		InetAddress forward;
		try
		{
			forward = future.get();
		}
		catch (InterruptedException | ExecutionException ex)
		{
			logger.warn(null, ex);

			client.writeNotice(FAILED_TO_LOOKUP);
			finish.run();
			return;
		}

		logger.debug("Forward address for {} resolved to {}", client, forward);

		InetSocketAddress remoteSocketAddress = (InetSocketAddress) client.getChannel().remoteAddress();
		InetAddress remoteAddress = remoteSocketAddress.getAddress();

		if (!forward.equals(remoteAddress))
		{
			client.writeNotice(HOSTNAME_MISMATCH);
			finish.run();
			return;
		}

		if (reverse.length() > IRC.HOSTLEN)
		{
			client.writeNotice(HOSTNAME_TOO_LONG);
			finish.run();
			return;
		}

		client.writeNotice(HOSTNAME_FOUND);
		client.setHost(reverse);
		finish.run();
	}
}
