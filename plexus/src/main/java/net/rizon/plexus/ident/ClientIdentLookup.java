/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.ident;

import io.netty.channel.Channel;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;

public class ClientIdentLookup
{
	private final Client client;
	private final Runnable finish;
	private final IdentClient identClient;

	private Channel channel;
	private String ident;

	public ClientIdentLookup(Client client, Runnable finish)
	{
		this.client = client;
		this.finish = finish;
		identClient = client.getPlexus().getIdentClient();
	}

	public Client getClient()
	{
		return client;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}

	public String getIdent()
	{
		return ident;
	}

	public void setIdent(String ident)
	{
		this.ident = ident;
	}

	public void start()
	{
		client.writeNotice("*** Checking Ident");

		identClient.connect(this);
	}

	public void cancel()
	{
		channel.close();
	}
	
	public void finish()
	{
		if (this.ident == null)
		{
			client.writeNotice("*** No Ident response");
		}
		else if (!IRC.isValidUsername(this.ident))
		{
			client.writeNotice("*** Invalid Ident response, using your username instead");
		}
		else
		{
			client.writeNotice("*** Got Ident response");
			client.setUsername(this.ident);
		}

		finish.run();
	}
}