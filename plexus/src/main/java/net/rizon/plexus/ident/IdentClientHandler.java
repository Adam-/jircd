/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.ident;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.net.InetSocketAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class IdentClientHandler extends SimpleChannelInboundHandler<String>
{
	private static final Logger logger = LoggerFactory.getLogger(IdentClientHandler.class);

	private final ClientIdentLookup lookup;

	public IdentClientHandler(ClientIdentLookup lookup)
	{
		this.lookup = lookup;
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception
	{
		InetSocketAddress local = (InetSocketAddress) ctx.channel().localAddress(),
			remote = (InetSocketAddress) ctx.channel().remoteAddress();

		int us = local.getPort();
		int them = remote.getPort();

		ctx.writeAndFlush(String.format("%d , %d\r\n", them, us));
	}

	@Override
	public void channelUnregistered(ChannelHandlerContext ctx) throws Exception
	{
		lookup.finish();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, String msg)
	{
		try
		{
			// 6193, 23 : USERID : UNIX : stjohns
			int i = msg.indexOf("USERID");
			if (i == -1)
			{
				return;
			}

			i = msg.lastIndexOf(':');
			if (i == -1 || i + 2 >= msg.length())
			{
				return;
			}

			String userName = msg.substring(i + 2);
			lookup.setIdent(userName);
		}
		finally
		{
			ctx.close();
		}
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception
	{
		// I assume this is the IdleStateEvent
		logger.debug("Ident timeout for {}", lookup);
		ctx.close();
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
	{
		logger.warn("Error in ident pipeline", cause);
		ctx.close();
	}
}
