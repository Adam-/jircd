/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.ident;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.net.InetSocketAddress;
import net.rizon.plexus.Client;
import net.rizon.plexus.Plexus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IdentClient
{
	private static final Logger logger = LoggerFactory.getLogger(IdentClient.class);

	public static final String NO_IDENT_PREFIX = "~";
	private static final int IDENT_CONNECT_TIMEOUT = 2;
	private static final int IDENT_PORT = 113;
	
	private final Bootstrap bootstrap;

	public IdentClient(Plexus plexus)
	{
		bootstrap = new Bootstrap()
			.group(plexus.getWorkerGroup())
			.channel(NioSocketChannel.class)
			.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, IDENT_CONNECT_TIMEOUT * 1000);
	}

	public void connect(ClientIdentLookup lookup)
	{
		Client client = lookup.getClient();
		
		InetSocketAddress remoteAddress = (InetSocketAddress) client.getChannel().remoteAddress();
		InetSocketAddress localAddress = (InetSocketAddress) client.getChannel().localAddress();
		
		bootstrap.handler(new IdentClientInitializer(lookup));
		
		logger.debug("Connecting to {}:{} from {}", remoteAddress.getAddress(), IDENT_PORT, localAddress.getAddress());
		
		ChannelFuture future = bootstrap.connect(
			new InetSocketAddress(remoteAddress.getAddress(), IDENT_PORT),
			new InetSocketAddress(localAddress.getAddress(), 0) // bind to same IP they connected to
		);
		
		lookup.setChannel(future.channel());
	}
}
