/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.commands.KnockChannel;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;

public class Knock extends Command
{
	@Parameter(index = 0, required = true)
	private String channel;

	public Knock()
	{
		super("KNOCK");
	}

	@Override
	public void execute(LocalUser user)
	{
		Channel c = getPlexus().getIrc().findChannel(channel);
		if (c == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHCHANNEL, channel);
			return;
		}

		if (c.findUser(user) != null)
		{
			user.writeNumeric(Numeric.ERR_KNOCKONCHAN, c.getName());
			return;
		}
		
		ModeManager modeManager = getPlexus().getModeManager();

		if (!c.hasMode(modeManager.getInviteOnly())
			&& !c.hasMode(modeManager.getKey())
			&& (!c.hasMode(modeManager.getLimit()) || c.getUsers().size() < c.getLimit()))
		{
			user.writeNumeric(Numeric.ERR_CHANOPEN, c.getName());
			return;
		}

		if (c.hasMode(modeManager.getPriv()) || c.isBanned(user))
		{
			user.writeNumeric(Numeric.ERR_CANNOTSENDTOCHAN, c.getName());
			return;
		}

		if (user.getLastKnock() + getPlexus().getConf().getChannel().getKnock_delay() > Util.currentTime())
		{
			user.writeNumeric(Numeric.ERR_TOOMANYKNOCK, c.getName(), "Too many KNOCKs (user).");
			return;
		}

		if (c.getLastKnock() + getPlexus().getConf().getChannel().getKnock_delay_channel() > Util.currentTime())
		{
			user.writeNumeric(Numeric.ERR_TOOMANYKNOCK, c.getName(), "Too many KNOCKs (channel).");
			return;
		}

		user.setLastKnock(Util.currentTime());
		
		KnockChannel kc = new KnockChannel();
		kc.setSource(user.getId());
		kc.setChannel(c.getName());
		kc.setChannelTs(c.getTs());
		kc.setTs(Instant.now());
		
		getPlexus().getClient().submit(user.getClient(), kc, (res, ex) -> {
			user.writeNumeric(Numeric.RPL_KNOCKDLVR, c.getName());
		});
	}

	public String getChannel()
	{
		return channel;
	}

	public void setChannel(String channel)
	{
		this.channel = channel;
	}
}