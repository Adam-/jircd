/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.messages.UserModeMessage;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.commands.UserModeSet;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Oper extends Command
{
	private static final Logger logger = LoggerFactory.getLogger(Oper.class);

	@Parameter(index = 0, required = true)
	private String name;

	@Parameter(index = 1, required = true)
	private String password;

	public Oper()
	{
		super("OPER");
	}

	@Override
	public void execute(LocalUser user)
	{
		// end grace

		if (user.isOper())
		{
			// omotd?
			user.writeNumeric(Numeric.RPL_YOUREOPER);
			return;
		}

		// svsnoop
		net.rizon.plexus.config.Oper o = getPlexus().getConf().findOLine(name);
		if (o == null)
		{
			logger.debug("Client " + this + " has no oper block");
			user.writeNumeric(Numeric.ERR_NOOPERHOST);
			getPlexus().getClientManager().sendToOps(null, "Failed OPER attempt as %s by %s (%s@%s) - no oper block",
				name, user.getName(), user.getUsername(), user.getHost());
			return;
		}

		if (!o.matchesHost(user.getUsername(), user.getIp()) && !o.matchesHost(user.getUsername(), user.getHost()))
		{
			logger.debug("Client " + this + " mismatched host");
			user.writeNumeric(Numeric.ERR_NOOPERHOST);
			getPlexus().getClientManager().sendToOps(null, "Failed OPER attempt as %s by %s (%s@%s) - host mismatch",
				name, user.getName(), user.getUsername(), user.getHost());
			return;
		}

		if (!o.getPassword().equals(password)) // XXX
		{
			logger.debug("Client " + this + " used an invalid password");
			user.writeNumeric(Numeric.ERR_PASSWDMISMATCH);
			getPlexus().getClientManager().sendToOps(null, "Failed OPER attempt as %s by %s (%s@%s) - password mismatch",
				name, user.getName(), user.getUsername(), user.getHost());
			return;
		}

		user.setOper(o);
		user.getClient().setAllow(o.getAllow());

		getPlexus().getClientManager().sendToOps(null, "%s (%s@%s) is now an operator", user.getName(), user.getUsername(), user.getHost());

		/* modes changes */
		ModeStacker stack = new ModeStacker();
		for (int i = 0; i < o.getUmodes().length(); ++i)
		{
			UserMode um = getPlexus().getModeManager().findUserMode(o.getUmodes().charAt(i));
			if (um == null || um.isRestricted() || user.hasMode(um))
			{
				continue;
			}

			stack.set(um.getCharacter());
		}

		/* send mode changes */
		String changes = stack.getAll();
		if (changes != null)
		{
			UserModeSet ums = new UserModeSet();
			ums.setSource(user.getId());
			ums.setTarget(user.getId());
			ums.setChanges(changes);

			getPlexus().getClient().submit(ums);
		}

		user.writeNumeric(Numeric.RPL_YOUREOPER);
		if (!o.getPrivileges().isEmpty())
		{
			user.writeNotice("*** Oper privileges are %s", Util.toString(o.getPrivileges(), ", "));
		}

		// oper motd
	}
}
