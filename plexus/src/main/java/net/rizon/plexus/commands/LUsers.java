/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.ClientManager;
import net.rizon.plexus.IRC;
import net.rizon.plexus.IRCStats;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Server;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.users.LocalUser;

public class LUsers extends Command
{
	public LUsers()
	{
		super("LUSERS");
	}

	@Override
	public int getGlobalDelay()
	{
		return getPlexus().getConf().getGeneral().getPace_wait_simple();
	}

	@Override
	public void execute(LocalUser user)
	{
		IRC irc = getPlexus().getIrc();
		ClientManager manager = getPlexus().getClientManager();
		IRCStats stats = getPlexus().getStats();

		// XXX.
		int nonhidden = 0;
		for (Server s : irc.getServers().values())
		{
			if (!s.isHidden())
			{
				++nonhidden;
			}
		}

		boolean hide = getPlexus().getConf().getServerhide().isHide_servers() && !user.isOper();

		user.writeNumeric(Numeric.RPL_LUSERCLIENT, irc.getUsers().size() - stats.getInvisible(), stats.getInvisible(), hide ? nonhidden : irc.getServers().size());
		if (manager.getLocalOpers().isEmpty() == false)
		{
			user.writeNumeric(Numeric.RPL_LUSEROP, manager.getLocalOpers().size());
		}
		// unknown connections = total connections - local users
		int unknown = manager.getClients().size() - manager.getLocalUsers().size();
		if (unknown > 0)
		{
			user.writeNumeric(Numeric.RPL_LUSERUNKNOWN, unknown);
		}
		if (!irc.getChannels().isEmpty())
		{
			user.writeNumeric(Numeric.RPL_LUSERCHANNELS, irc.getChannels().size());
		}

		if (hide)
		{
			user.writeNumeric(Numeric.RPL_LUSERME, irc.getUsers().size(), nonhidden);
			user.writeNumeric(Numeric.RPL_LOCALUSERS, irc.getUsers().size(), stats.getMaxTotal());
		}
		else
		{
			user.writeNumeric(Numeric.RPL_LUSERME, manager.getLocalUsers().size(), irc.getServers().size());
			user.writeNumeric(Numeric.RPL_LOCALUSERS, manager.getLocalUsers().size(), stats.getMaxTotal());
		}

		user.writeNumeric(Numeric.RPL_GLOBALUSERS, getPlexus().getIrc().getUsers().size(), stats.getMaxTotal());

		if (!hide)
		{
			user.writeNumeric(Numeric.RPL_STATSCONN, stats.getMaxConnections(), stats.getMaxLocal(), stats.getTotalConnections());
		}
	}
}
