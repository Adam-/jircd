/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.config.Auth;
import net.rizon.plexus.users.LocalUser;

public class WebIRC extends Command
{
	@Parameter(index = 0, required = true)
	private String password;

	@Parameter(index = 1, required = true)
	private String username;

	@Parameter(index = 2, required = true)
	private String hostname;

	@Parameter(index = 3, required = true)
	private String ip;

	public WebIRC()
	{
		super("WEBIRC");
	}

	@Override
	public void executeUnregistered(Client client)
	{
		if (client.getName() != null || client.getUsername() != null
			|| client.getWebircHost() != null || client.getWebircIp() != null)
		{
			client.writeNumeric(Numeric.ERR_CANNOTDOCOMMAND, this.getName(), "Registration already in progress");
			return;
		}

		if (!IRC.isValidUsername(username) || !IRC.isValidHostname(hostname, false) || !IRC.isValidIP(ip))
		{
			client.writeNumeric(Numeric.ERR_CANNOTDOCOMMAND, this.getName(), "Malformed WEBIRC command");
			client.exit("Malformed CGI:IRC command");
			return;
		}

		Auth auth = findAuth(client);
		if (auth == null)
		{
			client.writeNumeric(Numeric.ERR_CANNOTDOCOMMAND, this.getName(), "WEBIRC authentication failed");
			client.exit("WEBIRC authentication failed");
			return;
		}

		client.setWebircIp(ip);
		client.setWebircHost(hostname);

		// dnsbl
	}

	@Override
	public void execute(LocalUser user)
	{
		user.writeNumeric(Numeric.ERR_ALREADYREGISTRED);
	}

	private Auth findAuth(Client client)
	{
		for (Auth a : getPlexus().getConf().getAuth())
		{
			if (!a.getFlags().contains("webirc"))
			{
				continue;
			}

			if (a.getPassword() == null || !a.getPassword().equals(password))
			{
				continue;
			}

			if (a.matches("*", client.getIp(), client.getIp())) // XXX hostname?
			{
				return a;
			}
		}

		return null;
	}
}
