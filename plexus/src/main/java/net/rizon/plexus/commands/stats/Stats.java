/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands.stats;

import java.util.Map;
import java.util.TreeMap;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public abstract class Stats
{
	protected char character;
	private Plexus plexus;
	
	public Stats(char c)
	{
		this.character = c;
		stats.put(c, this);
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	public void setPlexus(Plexus plexus)
	{
		this.plexus = plexus;
	}
	
	public boolean hasPriv(User u)
	{
		if (getPlexus().getConf().getGeneral().getAdmin_only_stats() != null && getPlexus().getConf().getGeneral().getAdmin_only_stats().indexOf(this.character) != -1)
			return u.isAdmin();
		if (getPlexus().getConf().getGeneral().getOper_only_stats() != null && getPlexus().getConf().getGeneral().getOper_only_stats().indexOf(this.character) != -1)
			return u.isOper();
		return true;
	}
	
	public abstract void run(LocalUser user);
	
	private static Map<Character, Stats> stats = new TreeMap<>();
	
	public static Stats getStats(char c)
	{
		return stats.get(c);
	}
}