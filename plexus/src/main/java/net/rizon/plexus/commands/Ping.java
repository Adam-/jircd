/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Message;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.messages.PongMessage;
import net.rizon.plexus.users.LocalUser;

public class Ping extends Command
{
	@Parameter(index = 0, required = true)
	private String origin;

	@Parameter(index = 1)
	private String destination;

	public Ping()
	{
		super("PING");
	}
	
	@Override
	public void execute(LocalUser user)
	{
		String dest = destination != null ? destination : getPlexus().getConf().getServerinfo().getName();
		Message m = new PongMessage(getPlexus().getMe(), dest, origin);
		user.writeMessage(m);
//		if (!user.isOper() || message.destinationName == null || message.destination == Plexus.me)
//		{
//			user.writeMessage(new Pong.PongMessage2(message.destinationName != null ? message.destinationName : Plexus.conf.serverinfo.name, message.origin));
//			return;
//		}
//
//		if (message.destination == null)
//		{
//			user.writeNumeric(Numeric.ERR_NOSUCHSERVER, message.destinationName);
//			return;
//		}
//
//		message.origin = user.name;
//		message.destination.writeMessage(message);
	}
}