/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.commands.TopicChange;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;

public class Topic extends Command
{
	@Parameter(index = 0, required = true)
	private String channel;

	@Parameter(index = 1)
	private String topic;
	
	public Topic()
	{
		super("TOPIC");
		
		this.setAllowEmptyLastArg(true);
	}
	
	@Override
	public void execute(LocalUser user)
	{
		// end flood
		Channel c = getPlexus().getIrc().findChannel(channel);
		
		if (c == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHCHANNEL, channel);
			return;
		}
		
		ModeManager modeManager = getPlexus().getModeManager();
		
		if (topic == null)
		{
			if (c.hasMode(modeManager.getSecret()) && c.findUser(user) == null)
			{
				user.writeNumeric(Numeric.ERR_NOTONCHANNEL, c.getName());
				return;
			}
			
			if (c.getTopic() == null)
			{
				user.writeNumeric(Numeric.RPL_NOTOPIC, c.getName());
				return;
			}
			
			user.writeNumeric(Numeric.RPL_TOPIC, c.getName(), c.getTopic());
			user.writeNumeric(Numeric.RPL_TOPICWHOTIME, c.getName(), c.getTopicSetter(), c.getTopicTime());
			return;
		}
		
		Membership mem = c.findUser(user);
		if (mem == null)
		{
			user.writeNumeric(Numeric.ERR_NOTONCHANNEL, c.getName());
			return;
		}
		
		if (c.hasMode(modeManager.getTopicLimit()) && !mem.hasMode(modeManager.getOwner()) && !mem.hasMode(modeManager.getProtect())
				&& !mem.hasMode(modeManager.getOp()) && !mem.hasMode(modeManager.getHalfop()))
		{
			user.writeNumeric(Numeric.ERR_CHANOPRIVSNEEDED, c.getName());
			return;
		}

		TopicChange tc = new TopicChange();
		tc.setSource(user.getId());
		tc.setChannel(c.getName());
		tc.setTs(c.getTs());

		tc.setTopic(topic.isEmpty() ? null : topic);
		tc.setTopicSetter(user.getName() + "!" + user.getUsername() + "@" + user.getVhost());
		tc.setTopicTs(Util.currentTime());

		getPlexus().getClient().submit(user.getClient(), tc, (res, ex) -> {
			//
		});
	}

	public String getChannel()
	{
		return channel;
	}

	public void setChannel(String channel)
	{
		this.channel = channel;
	}

	public String getTopic()
	{
		return topic;
	}

	public void setTopic(String topic)
	{
		this.topic = topic;
	}
}