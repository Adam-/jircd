/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.replication.commands.JoinUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.xline.XLine;
import net.rizon.plexus.xline.XLineType;

public class Join extends Command
{
	@Parameter(index = 0, required = true, targets = true)
	private String[] channels;

	@Parameter(index = 1, targets = true)
	private String[] keys;

	public Join()
	{
		super("JOIN");
	}

	@Override
	public void execute(LocalUser user)
	{
		int keyCount = 0;

		for (String channel : channels)
		{
			String key = null;

			if (keys != null && keyCount < keys.length)
			{
				key = keys[keyCount++];
			}

			if (!getPlexus().getIrc().isValidChannel(channel))
			{
				user.writeNumeric(Numeric.ERR_BADCHANNAME, channel);
				continue;
			}

			if (!user.getClient().getIline().getFlags().contains("resv_exempt") && !user.isOper())
			{
				XLine resv = getPlexus().getXlineManager().findXline(XLineType.RESV, channel);
				if (resv != null)
				{
					user.writeNumeric(Numeric.ERR_CHANBANREASON, channel, resv.getReason());
					getPlexus().getClientManager().sendToOps(getPlexus().getModeManager().getReject(),
						"Forbidding reserved channel %s from user %s[%s@%s]",
						channel, user.getName(), user.getUsername(), user.getHost());
					return;
				}
			}

			int max_channels = user.getClient().getAllow().getMax_channels();
			if (max_channels == 0)
			{
				max_channels = IRC.DEFAULT_MAX_CHANNELS;
			}
			if (user.getChannels().size() >= max_channels)
			{
				user.writeNumeric(Numeric.ERR_TOOMANYCHANNELS, channel);
				continue;
			}

			Channel c = getPlexus().getIrc().findChannel(channel);
			String giveModes = getPlexus().getConf().getChannel().getModes_on_create();
			
			if (c != null)
			{
				giveModes = null;
				
				if (c.findUser(user) != null)
				{
					continue;
				}

				if (c.isBanned(user) && !c.isExempt(user))
				{
					user.writeNumeric(Numeric.ERR_BANNEDFROMCHAN, c.getName());
					continue;
				}

				if (c.hasMode(getPlexus().getModeManager().getInviteOnly()))
				{
					if (user.findInviteToChannel(c) == null)
					{
						// XXX check +I
						user.writeNumeric(Numeric.ERR_INVITEONLYCHAN, c.getName());
						continue;
					}
				}

				if (c.hasMode(getPlexus().getModeManager().getOperOnnly()) && !user.isOper())
				{
					user.writeNumeric(Numeric.ERR_OPERONLYCHAN, c.getName());
					continue;
				}

				if (c.hasMode(getPlexus().getModeManager().getLimit()) && c.getLimit() > 0 && c.getUsers().size() >= c.getLimit())
				{
					user.writeNumeric(Numeric.ERR_CHANNELISFULL, c.getName());
					continue;
				}

				if (c.hasMode(getPlexus().getModeManager().getCm_regOnly()) && !user.hasMode(getPlexus().getModeManager().getRegistered()))
				{
					user.writeNumeric(Numeric.ERR_NEEDREGGEDNICK, c.getName(), "join", c.getName());
					continue;
				}

				if (c.hasMode(getPlexus().getModeManager().getSslOnly()) && !user.hasMode(getPlexus().getModeManager().getSsl()))
				{
					user.writeNumeric(Numeric.ERR_CHANBANREASON, c.getName(), "SSL is required");
					continue;
				}

				if (c.hasMode(getPlexus().getModeManager().getKey()) && (c.getKey() == null || key == null || !c.getKey().equals(key)))
				{
					user.writeNumeric(Numeric.ERR_BADCHANNELKEY, c.getName());
					continue;
				}
			}

			JoinUser ju = new JoinUser();
			ju.setChannel(channel);
			ju.setUser(user.getId());
			ju.setModes(giveModes);
			ju.setTs(Instant.now());

			getPlexus().getClient().submit(user.getClient(), ju, (res, ex) -> {
				Channel ch = getPlexus().getIrc().findChannel(channel);

				if (ch == null)
				{
					// I don't think this is possible
					user.writeNotice("Lost channel");
					return;
				}

				if (ex != null)
				{
					user.writeNotice("Unable to join channel {}: {}", channel, ex.getMessage());
					return;
				}

				net.rizon.plexus.channels.Invite i = user.findInviteToChannel(c);
				if (i != null)
				{
					i.remove();
				}

				if (ch.getTopic() != null)
				{
					Topic cmd = (Topic) Join.this.getManager().findCommand("TOPIC");
					if (cmd != null)
					{
						cmd.setChannel(ch.getName());
						
						cmd.execute(user);
					}
				}

				Names cmd = (Names) Join.this.getManager().findCommand("NAMES");
				if (cmd != null)
				{
					cmd.setChannel(ch.getName());
					cmd.execute(user);
				}
			});
		}
	}
}
