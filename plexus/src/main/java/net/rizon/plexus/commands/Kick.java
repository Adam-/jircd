/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.commands.KickUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Kick extends Command
{
	@Parameter(index = 0, required = true)
	private String channel;

	@Parameter(index = 1, required = true)
	private String target;

	@Parameter(index = 2)
	private String reason;

	public Kick()
	{
		super("KICK");
	}

	@Override
	public void execute(LocalUser user)
	{
		Channel c = getPlexus().getIrc().findChannel(channel);

		if (c == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHCHANNEL, channel);
			return;
		}

		// endgrace

		Membership mem = c.findUser(user);
		if (mem == null)
		{
			user.writeNumeric(Numeric.ERR_NOTONCHANNEL, c.getName());
			return;
		}
		
		ModeManager modeManager = getPlexus().getModeManager();

		if (!mem.hasMode(modeManager.getOwner()) && !mem.hasMode(modeManager.getProtect())
			&& !mem.hasMode(modeManager.getOp()) && !mem.hasMode(modeManager.getHalfop()))
		{
			user.writeNumeric(Numeric.ERR_CHANOPRIVSNEEDED, c.getName());
			return;
		}

		User u = getPlexus().getIrc().findUser(target);
		if (u == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
			return;
		}

		Membership target_mem = c.findUser(u);
		if (target_mem == null)
		{
			user.writeNumeric(Numeric.ERR_USERNOTINCHANNEL, u.getName(), c.getName());
			return;
		}

		// ulined or is services, deny

		// netadmin target and not self

		if (user != u)
		{
			boolean targetOwner = target_mem.hasMode(modeManager.getOwner()),
				targetProtect = target_mem.hasMode(modeManager.getProtect()),
				targetOp = target_mem.hasMode(modeManager.getOp()),
				targetHalfop = target_mem.hasMode(modeManager.getHalfop());
			
			boolean sourceOwner = mem.hasMode(modeManager.getOwner()),
				sourceProtect = mem.hasMode(modeManager.getProtect()),
				sourceOp = mem.hasMode(modeManager.getOp()),
				sourceHalfop = mem.hasMode(modeManager.getHalfop());

			if (targetOwner || targetProtect)
			{
				if (!sourceOwner)
				{
					// disallow anyone but owners from kicking +qa
					user.writeNumeric(Numeric.ERR_CHANOPRIVSNEEDED, c.getName());
					return;
				}
			}

			if (sourceHalfop && !sourceOp && !sourceProtect && !sourceOwner)
			{
				if (targetOp || (c.hasMode(modeManager.getPriv()) && targetHalfop))
				{
					// disallow +h on +o and +h on +h with +p
					user.writeNumeric(Numeric.ERR_CHANOPRIVSNEEDED, c.getName());
					return;
				}
			}
		}


		if (reason == null)
			reason = target;

		if (reason.length() > IRC.KICKLEN)
			reason = reason.substring(0, IRC.KICKLEN);

		KickUser ku = new KickUser();
		ku.setSource(user.getId());
		ku.setTarget(u.getId());
		ku.setChannel(c.getName());
		ku.setChannelTs(c.getTs());
		ku.setTs(Instant.now());
		ku.setReason(reason);

		getPlexus().getClient().submit(user.getClient(), ku, (res, ex) -> {
			if (ex == null)
				return;

			user.writeNotice("Unable to kick user: {}", ex.getMessage());
		});
	}
}