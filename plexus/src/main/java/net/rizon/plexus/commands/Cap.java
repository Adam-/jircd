/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.util.ArrayList;
import java.util.List;
import net.rizon.plexus.Client;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.messages.CapMessage;
import net.rizon.plexus.users.Capability;
import net.rizon.plexus.users.LocalUser;

public class Cap extends Command
{
	@Parameter(index = 0, required = true)
	private String subcommand; // ack, clear, end, list, ls, nak, req

	@Parameter(index = 1)
	private String args;

	public Cap()
	{
		super("CAP");
	}

	public String getSubcommand()
	{
		return subcommand;
	}

	public void setSubcommand(String subcommand)
	{
		this.subcommand = subcommand;
	}

	public String[] getArgs()
	{
		return args.split("\\s+");
	}

	public void setArgs(String args)
	{
		this.args = args;
	}

	private void doCap(LocalUser user, Client client)
	{
		String name = client.getMessageName();

		if (subcommand.equalsIgnoreCase("CLEAR"))
		{
			String buf = "";
			for (Capability cap : Capability.values())
				if (client.hasCap(cap))
					buf += "-" + cap.getName() + " ";
			client.writeMessage(new CapMessage(getPlexus().getMe(), name, "ACK", buf.trim()));

			client.clearCaps();
		}
		else if (subcommand.equalsIgnoreCase("END"))
		{
			if (user != null)
				return;

			client.setCap(false);
			client.register();
		}
		else if (subcommand.equalsIgnoreCase("LIST"))
		{
			/* show caps the client has enabled */
			String buf = "";
			for (Capability cap : Capability.values())
				if (client.hasCap(cap))
					buf += cap.getName() + " ";
			client.writeMessage(new CapMessage(getPlexus().getMe(), name, "LIST", buf.trim()));
		}
		else if (subcommand.equalsIgnoreCase("LS"))
		{
			if (user == null)
				client.setCap(true);

			/* show all available caps */
			String buf = "";
			for (Capability cap : Capability.values())
				buf += cap.getName() + " ";
			client.writeMessage(new CapMessage(getPlexus().getMe(), name, "LS", buf.trim()));
		}
		else if (subcommand.equalsIgnoreCase("REQ"))
		{
			String ack = "";
			boolean nak = false;

			/* Track the caps we are adding and removing */
			List<Capability> capsToAdd = new ArrayList<>();
			List<Capability> capsToDel = new ArrayList<>();

			for (String s : args.split("\\s+"))
			{
				/* Check if it's prefixed */
				boolean removing = !s.isEmpty() && s.charAt(0) == '-';
				Capability cap;

				if (removing)
					cap = Capability.find(s.substring(1));
				else
					cap = Capability.find(s);

				if (cap != null)
				{
					ack += s + " ";
					if (removing)
						capsToDel.add(cap);
					else
						capsToAdd.add(cap);
				}
				else
				{
					/* If it's an invalid cap, bail */
					nak = true;
					break;
				}
			}

			/* If we received an invalid cap, we must NAK
			 * the request and not modify the client's capabilities. */
			if (nak)
			{
				client.writeMessage(new CapMessage(getPlexus().getMe(), name, "NAK", args));
			}
			else if (!ack.isEmpty())
			{
				for (Capability cap : capsToAdd)
					client.addCap(cap);
				for (Capability cap : capsToDel)
					client.delCap(cap);
				client.writeMessage(new CapMessage(getPlexus().getMe(), name, "ACK", args));
			}
		}
	}

	@Override
	public void executeUnregistered(Client client)
	{
		this.doCap(null, client);
	}

	@Override
	public void execute(LocalUser user)
	{
		this.doCap(user, user.getClient());
	}
}
