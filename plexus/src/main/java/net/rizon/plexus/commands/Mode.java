/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.util.Arrays;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.StackerLine;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.commands.ChannelModeSet;
import net.rizon.plexus.replication.commands.UserModeSet;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Mode extends Command
{
	@Parameter(index = 0, required = true)
	private String target;

	@Parameter(index = 1)
	private String[] params;

	public Mode()
	{
		super("MODE");
	}

	@Override
	public void execute(LocalUser user)
	{
		String modes = null;
		String[] modeParams = null;
		
		if (params != null)
		{
			modes = params[0];
			
			if (params.length >= 2)
			{
				modeParams = Arrays.copyOfRange(params, 1, params.length);
			}
		}

		if (getPlexus().getIrc().isValidChannel(target))
		{
			Channel c = getPlexus().getIrc().findChannel(target);
			if (c == null)
			{
				user.writeNumeric(Numeric.ERR_NOSUCHCHANNEL, target);
				return;
			}

			if (modes == null)
			{
				user.writeNumeric(Numeric.RPL_CHANNELMODEIS, c.getName(), c.getModeString(user.findChannel(c) != null));
				user.writeNumeric(Numeric.RPL_CREATIONTIME, c.getName(), c.getTs());
				return;
			}

			Membership mem = c.findUser(user);
			if (mem == null)
			{
				user.writeNumeric(Numeric.ERR_NOTONCHANNEL, c.getName());
				return;
			}

			boolean shownUnkMode = false, shownError = false;

			boolean add = true;
			int paramIdx = 0;
			ModeStacker stacker = new ModeStacker();
			for (int i = 0; i < modes.length(); ++i)
			{
				char m = modes.charAt(i);
				if (m == '+')
				{
					add = true;
					continue;
				}
				else if (m == '-')
				{
					add = false;
					continue;
				}

				ChannelMode mode = getPlexus().getModeManager().findChannelMode(m);

				if (mode == null)
				{
					if (!shownUnkMode)
					{
						user.writeNumeric(Numeric.ERR_UNKNOWNMODE, m);
						shownUnkMode = true;
					}
					continue;
				}

				boolean requiresParameter = mode.isParameter() || mode.isList() || mode.getPrefix() > 0;
				if (requiresParameter && !add && mode.isMinusNoArg())
				{
					requiresParameter = false;
				}

				String param = null;
				if (requiresParameter)
				{
					if (modeParams != null && paramIdx < modeParams.length)
					{
						param = modeParams[paramIdx++];
					}

					if (paramIdx > IRC.MAXMODEPARAMS)
					{
						continue;
					}

					param = mode.paramValid(user, c, mem, add, param);

					if (param == null)
					{
						continue;
					}
				}

				if (mode.hasMode(c, param) == add)
				{
					continue;
				}

				if (!mode.canSet(user, c, mem, add, param, shownError))
				{
					shownError = true;
					continue;
				}

				if (add)
				{
					stacker.set(mode.getCharacter(), param);
				}
				else
				{
					stacker.unset(mode.getCharacter(), param);
				}
			}

			StackerLine changes = stacker.getLine(0);
			if (changes != null)
			{
				ChannelModeSet cms = new ChannelModeSet();
				cms.setSource(user.getId());
				cms.setTarget(c.getName());
				cms.setTargetTs(c.getTs());
				cms.setChanges(changes.getModes());
				cms.setParameters(changes.getParams());

				getPlexus().getClient().submit(user.getClient(), cms, (res, ex) -> {

				});
			}
		}
		else if (IRC.isValidNick(target))
		{
			User u = getPlexus().getIrc().findUser(target);
			if (u == null)
			{
				user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
				return;
			}

			if (u != user)
			{
				user.writeNumeric(Numeric.ERR_USERSDONTMATCH);
				return;
			}

			if (modes == null)
			{
				user.writeNumeric(Numeric.RPL_UMODEIS, "+" + user.getModes());
				return;
			}

			boolean shownUnkMode = false, shownError = false;
			boolean add = true;
			ModeStacker stack = new ModeStacker();
			for (int i = 0; i < modes.length(); ++i)
			{
				char m = modes.charAt(i);
				if (m == '+')
				{
					add = true;
					continue;
				}
				else if (m == '-')
				{
					add = false;
					continue;
				}

				UserMode mode = getPlexus().getModeManager().findUserMode(m);
				if (mode == null)
				{
					if (!shownUnkMode)
					{
						shownUnkMode = true;
						user.writeNumeric(Numeric.ERR_UNKNOWNMODE, m);
					}
					continue;
				}

				if (user.hasMode(mode) == add)
				{
					continue;
				}

				if (!mode.canSet(user, add, shownError))
				{
					shownError = true;
					continue;
				}

				if (add)
				{
					stack.set(mode.getCharacter());
				}
				else
				{
					stack.unset(mode.getCharacter());
				}
			}

			String changes = stack.getAll();
			if (changes != null)
			{
				UserModeSet ums = new UserModeSet();
				ums.setSource(user.getId());
				ums.setTarget(user.getId());
				ums.setChanges(changes);

				getPlexus().getClient().submit(user.getClient(), ums, (res, ex) -> {

				});
			}
		}
	}
}
