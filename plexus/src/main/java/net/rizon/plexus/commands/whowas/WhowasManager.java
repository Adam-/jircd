/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands.whowas;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import net.rizon.plexus.IRC;
import net.rizon.plexus.users.User;

public class WhowasManager
{
	private static final int HISTORY_LENGTH = 32768;

	private final Map<String, List<Whowas>> whowas = new HashMap<>();
	private final Whowas[] whowasQ = new Whowas[HISTORY_LENGTH];
	private int pos = 0;

	public void add(User user)
	{
		if (pos >= whowasQ.length)
		{
			pos = 0;
		}

		Whowas w = whowasQ[pos];
		if (w != null)
		{
			List<Whowas> l = whowas.get(IRC.toUpperCase(w.getName()));
			assert !l.isEmpty();
			l.remove(w);
			if (l.isEmpty())
			{
				whowas.remove(IRC.toUpperCase(w.getName()));
			}
		}
		else
		{
			w = whowasQ[pos] = new Whowas();
		}

		++pos;

		w.setName(user.getName());
		w.setUsername(user.getUsername());
		w.setHost(user.getHost());
		w.setVhost(user.getVhost());
		w.setGecos(user.getGecos());
		w.setAccount(user.getAccount());
		w.setServer(user.getServer().getName());
		w.setHide(user.getServer().isHidden());
		w.setLogoff(new Date());

		List<Whowas> l = whowas.get(IRC.toUpperCase(w.getName()));
		if (l == null)
		{
			l = new LinkedList<>();
			whowas.put(IRC.toUpperCase(w.getName()), l);
		}
		l.add(w);
	}

	public Collection<Whowas> getWhowasFor(String name)
	{
		return whowas.get(IRC.toUpperCase(name));
	}
}
