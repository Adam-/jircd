/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.users.LocalUser;

public class Stats extends Command
{
	static
	{	
		new net.rizon.plexus.commands.stats.Deny('D', false);
		new net.rizon.plexus.commands.stats.Deny('d', true);
		
		new net.rizon.plexus.commands.stats.Auth('I');
		new net.rizon.plexus.commands.stats.Auth('i');
		
		new net.rizon.plexus.commands.stats.KLines('K', false);
		new net.rizon.plexus.commands.stats.KLines('k', true);
		
		new net.rizon.plexus.commands.stats.Message('M');
		new net.rizon.plexus.commands.stats.Message('m');
		
		new net.rizon.plexus.commands.stats.Oper('O');
		new net.rizon.plexus.commands.stats.Oper('o');
		
		new net.rizon.plexus.commands.stats.OperedUp('p');
		new net.rizon.plexus.commands.stats.Ports('P');
		
		new net.rizon.plexus.commands.stats.Uptime('u');
		
		new net.rizon.plexus.commands.stats.Allow('y');
		new net.rizon.plexus.commands.stats.Allow('Y');
		
		new net.rizon.plexus.commands.stats.Memory('z');
		new net.rizon.plexus.commands.stats.Memory('Z');
	}
	
	@Parameter(index = 0, required = true)
	private String what;
	
	public Stats()
	{
		super("STATS");
	}
	
	@Override
	public int getGlobalDelay() { return getPlexus().getConf().getGeneral().getPace_wait(); }
	
	@Override
	public void execute(LocalUser user)
	{
		char c = what.charAt(0);
		
		net.rizon.plexus.commands.stats.Stats stats = net.rizon.plexus.commands.stats.Stats.getStats(c);
		if (stats != null)
		{
			stats.setPlexus(getPlexus());

			if (!stats.hasPriv(user))
			{
				user.writeNumeric(Numeric.ERR_NOPRIVILEGES);
				return;
			}
			
			stats.run(user);
		}
		
		user.writeNumeric(Numeric.RPL_ENDOFSTATS, c);
	}
}