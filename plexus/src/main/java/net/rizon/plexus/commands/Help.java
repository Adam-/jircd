/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.users.LocalUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Help extends Command
{
	private static final Logger logger = LoggerFactory.getLogger(Help.class);

	private static TreeMap<String, String[]> uhelp = new TreeMap<>(),
		ohelp = new TreeMap<>();

	static
	{
		File userHelp = new File("help/users"), operHelp = new File("help/opers");

		for (File f : userHelp.listFiles())
		{
			try
			{
				BufferedReader in = new BufferedReader(new FileReader(f));
				ArrayList<String> list = new ArrayList<String>();
				for (String s; (s = in.readLine()) != null;)
					list.add(s);
				String[] a = new String[list.size()];
				list.toArray(a);

				if (a.length > 0)
					uhelp.put(f.getName().toUpperCase(), a);
				in.close();
			}
			catch (IOException ex)
			{
				logger.warn("Unable to load help file " + f.getName(), ex);
			}
		}

		for (File f : operHelp.listFiles())
		{
			try
			{
				BufferedReader in = new BufferedReader(new FileReader(f));
				ArrayList<String> list = new ArrayList<String>();
				for (String s; (s = in.readLine()) != null;)
					list.add(s);
				String[] a = new String[list.size()];
				list.toArray(a);

				if (a.length > 0)
					ohelp.put(f.getName().toUpperCase(), a);
				in.close();
			}
			catch (IOException ex)
			{
				logger.warn("Unable to load help file " + f.getName(), ex);
			}
		}
	}

	@Parameter(index = 0)
	private String what;

	public Help()
	{
		super("HELP");
	}

	@Override
	public int getGlobalDelay() { return getPlexus().getConf().getGeneral().getPace_wait_simple(); }

	@Override
	public void execute(LocalUser user)
	{
		// this has a throttle

		String topic = what;
		if (topic == null)
			topic = "INDEX";
		else
			topic = topic.toUpperCase();

		String[] help;
		if (user.isOper())
		{
			help = ohelp.get(topic);
			if (help == null)
				help = uhelp.get(topic);
		}
		else
			help = uhelp.get(topic);

		if (help == null)
		{
			user.writeNumeric(Numeric.ERR_HELPNOTFOUND, topic);
			return;
		}

		boolean start = false;
		for (String s : help)
		{
			if (s.startsWith("#"))
				continue;

			if (start)
				user.writeNumeric(Numeric.RPL_HELPSTART, topic, s);
			else
				user.writeNumeric(Numeric.RPL_HELPTXT, topic, s);
		}

		user.writeNumeric(Numeric.RPL_HELPTXT, topic, "");
		user.writeNumeric(Numeric.RPL_ENDOFHELP, topic);
	}
}