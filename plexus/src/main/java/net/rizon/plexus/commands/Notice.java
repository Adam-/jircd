/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.messages.MessageMessage;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.commands.MessageChannel;
import net.rizon.plexus.replication.commands.MessageUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Notice extends Command
{
	@Parameter(index = 0, required = true, targets = true)
	private String[] targets;

	@Parameter(index = 1, required = true)
	private String message;
	
	public Notice()
	{
		super("NOTICE");
	}
		
	@Override
	public void execute(LocalUser user)
	{
		for (String target : targets)
		{
			Channel c = null;
			User u = null;

			ModeManager modeManager = getPlexus().getModeManager();

			if (getPlexus().getIrc().isValidChannel(target))
			{
				c = getPlexus().getIrc().findChannel(target);
				
				if (c == null)
				{
					user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
					continue;
				}
				
				Membership mem = c.findUser(user);
				if (mem == null && c.hasMode(modeManager.getNoExternal()))
					continue;
				
				if (user.isNetAdmin())
					; // Net admins can override anything
				// Users with any status (voice+) can override the following
				else if (mem != null && mem.getModes() == null)
				{
					if (modeManager.getModerated().checkMessage(user, c, false))
						continue;
					
					if (!modeManager.getModReg().checkMessage(user, c, false))
						continue;
					
					if (!modeManager.getCm_noCtcp().checkMessage(user, c, false, message))
						continue;
					
					if (!modeManager.getNoCtrl().checkMessage(user, c, false, message))
						continue;
				}
			}
			else if (IRC.isValidNick(target))
			{
				u = getPlexus().getIrc().findUser(target);
				
				if (u == null)
				{
					user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
					continue;
				}
				
				if (!modeManager.getUm_noCtcp().checkMessage(user, u, false, message))
					continue;
				
				if (!modeManager.getUm_regOnly().checkMessage(user, u, false))
					continue;
			}
			else
			{
				/* status msg, server notice, user@server, etc */
			}
			
			if (c != null)
			{
				// resv
				
				// +b, +m, +M, +C, +c, +N
				
				// msg throttle flooding
				
				// idle time

				MessageChannel event = new MessageChannel();
				event.setSource(user.getId());
				event.setCommand(MessageMessage.Command.PRIVMSG);
				event.setChannel(c.getName());
				event.setMessage(message);
				event.setTime(Instant.now());
				
				getPlexus().getClient().submit(event);
			}
			else if (u != null)
			{
				// +R, +C
				
				// away
				
				if (!modeManager.getCallerId().onMessage(user, u, false) || !modeManager.getSoftCallerId().onMessage(user, u, false))
					return;

				// idle time
				
				//flood check

				if (u.isLocal())
				{
					Message m = new MessageMessage(user, MessageMessage.Command.NOTICE, u.getName(), message);
					((LocalUser) u).writeMessage(m);
				}
				else
				{
					MessageUser event = new MessageUser();
					event.setSource(user.getId());
					event.setCommand(MessageMessage.Command.NOTICE);
					event.setTarget(u.getId());
					event.setMessage(message);
					event.setTime(Instant.now());
					
					getPlexus().getClient().submit(event);
				}
			}
		}
	}
}