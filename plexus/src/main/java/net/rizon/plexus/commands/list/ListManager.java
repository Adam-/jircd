/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;

public class ListManager
{
	private final List<LocalUser> listingUsers = new ArrayList<>();
	private Channel[] channels;

	public void process()
	{
		for (Iterator<LocalUser> it = listingUsers.iterator(); it.hasNext();)
		{
			LocalUser user = it.next();

			list(user.getListTask(), user, false);

			if (user.getListTask() == null)
			{
				it.remove();
			}
		}

		if (listingUsers.isEmpty())
		{
			channels = null;
		}
	}

	public void rebuild(IRC irc)
	{
		channels = new Channel[irc.getChannels().size()];
		irc.getChannels().values().toArray(channels);
	}

	public void clear()
	{
		if (listingUsers.isEmpty())
		{
			channels = null;
		}
	}

	public void add(LocalUser user)
	{
		listingUsers.add(user);
	}

	public void remove(LocalUser user)
	{
		listingUsers.remove(user);
	}

	public void list(ListTask task, LocalUser user, boolean no_masked_channels)
	{
		if (!no_masked_channels)
		{
			for (int i = task.getLastIndex(); i < channels.length; ++i)
			{
				if (user.getClient().isExceedingSendQ())
				{
					task.setLastIndex(i);
					return;
				}

				listOneChannel(task, user, channels[i]);
			}
		}
		else
		{
			for (String s : task.getShowMask())
			{
				Channel c = user.getPlexus().getIrc().findChannel(s);
				if (c != null)
				{
					listOneChannel(task, user, c);
				}
			}
		}

		user.writeNumeric(Numeric.RPL_LISTEND);

		user.setListTask(null);
	}

	private void listOneChannel(ListTask task, LocalUser user, Channel channel)
	{
		ModeManager modeManager = user.getPlexus().getModeManager();
		
		if (channel.hasMode(modeManager.getSecret()) && channel.findUser(user) == null && !user.isAdmin())
		{
			return;
		}

		if (channel.getUsers().size() < task.getUsersMin() || channel.getUsers().size() > task.getUsersMax())
		{
			return;
		}

		if (channel.getTs() < task.getCreatedMin() || channel.getTs() > task.getCreatedMax())
		{
			return;
		}

		if (channel.getTopicTime() < task.getTopictsMin() || (channel.getTopicTime() != 0 && channel.getTopicTime() > task.getTopictsMax()))
		{
			return;
		}

		for (String s : task.getShowMask())
		{
			if (!Util.match(channel.getName(), s))
			{
				return;
			}
		}

		for (String s : task.getHideMask())
		{
			if (Util.match(channel.getName(), s))
			{
				return;
			}
		}

		String modes = channel.getModeString(channel.findUser(user) != null);
		if (modes.equals("+"))
		{
			modes = "";
		}
		else
		{
			modes = "[" + modes + "] ";
		}

		user.writeNumeric(Numeric.RPL_LIST, channel.getName(), channel.getUsers().size(), modes + (channel.getTopic() != null ? channel.getTopic() : ""));
	}
}
