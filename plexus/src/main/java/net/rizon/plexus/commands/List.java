/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.commands.list.ListManager;
import net.rizon.plexus.commands.list.ListTask;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;

public class List extends Command
{
	@Parameter(index = 0)
	private String options;

	@Override
	public int getGlobalDelay() { return getPlexus().getConf().getGeneral().getPace_wait(); }

	public List()
	{
		super("LIST");
	}

	@Override
	public void execute(LocalUser user)
	{
		// XXX heavy throttle

		if (user.getListTask() != null)
			user.writeNumeric(Numeric.RPL_LISTEND);

		ListTask task = new ListTask();
		boolean no_masked_channels = true;
		
		user.setListTask(task);

		if (options != null)
		{
			boolean error = false;
			String[] opt = options.split(",");
			for (String s : opt)
			{
				if (s.isEmpty())
					continue;
				switch (s.charAt(0))
				{
					case '<':
						try
						{
							task.setUsersMax(Integer.parseInt(s.substring(1)));
						}
						catch (NumberFormatException ex)
						{
							error = true;
						}
						if (task.getUsersMax() <= 0)
							error = true;
						break;
					case '>':
						try
						{
							task.setUsersMin(Integer.parseInt(s.substring(1)));
						}
						catch (NumberFormatException ex)
						{
							error = true;
						}
						if (task.getUsersMin() < 0)
							error = true;
						break;
					case '-':
						break;
					case 'C':
					case 'c':
						if (s.length() > 2)
							switch (s.charAt(1))
							{
								case '<':
									try
									{
										int max = Integer.parseInt(s.substring(2));
										if (max >= 0)
										{
											max = (int) Util.currentTime() - 60 * max;
											task.setCreatedMax(max);
										}
										else
										{
											error = true;
										}
									}
									catch (NumberFormatException nex)
									{
										error = true;
									}
									break;
								case '>':
									try
									{
										int min = Integer.parseInt(s.substring(2));
										if (min >= 0)
										{
											min = (int) Util.currentTime() - 60 * min;
											task.setCreatedMin(min);
										}
										else
										{
											error = true;
										}
									}
									catch (NumberFormatException nex)
									{
										error = true;
									}
									break;
								default:
									error = true;
							}
						else
							error = true;
						break;
					case 'T':
					case 't':
						if (s.length() > 2)
							switch (s.charAt(0))
							{
								case '<':
									try
									{
										int min = Integer.parseInt(s.substring(2));
										if (min >= 0)
										{
											min = (int) Util.currentTime() - 60 * min;
											task.setTopictsMin(min);
										}
										else
										{
											error = true;
										}
									}
									catch (NumberFormatException ex)
									{
										error = true;
									}
									break;
								case '>':
									try
									{
										int max = Integer.parseInt(s.substring(2));
										if (max >= 0)
										{
											max = (int) Util.currentTime() - 60 * max;
											task.setTopictsMax(max);
										}
										else
										{
											error = true;
										}
									}
									catch (NumberFormatException ex)
									{
										error = true;
									}
									break;
								default:
									error = true;
							}
						else
							error = true;
						break;
					default:
					{
						java.util.List<String> list = task.getShowMask();
						boolean show = true;
						if (s.charAt(0) == '!')
						{
							s = s.substring(1);
							show = false;
							list = task.getHideMask();
						}

						if (s.indexOf('*') != -1) // XXX more wildcards than * ?
						{
							if (show)
								no_masked_channels = false;
						}
						else if (!getPlexus().getIrc().isValidChannel(s))
							error = true;
						if (!error)
							list.add(s);
					}
				}
			}

			if (error)
			{
				user.setListTask(null);
				user.writeNumeric(Numeric.ERR_LISTSYNTAX);
				return;
			}
		}

		ListManager manager = getPlexus().getListManager();
		manager.rebuild(getPlexus().getIrc());

		user.writeNumeric(Numeric.RPL_LISTSTART);
		manager.list(task, user, no_masked_channels && !task.getShowMask().isEmpty());

		if (user.getListTask() != null)
			manager.add(user);
		else
			manager.clear();
	}
}