/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.replication.commands.UserAway;
import net.rizon.plexus.users.LocalUser;

public class Away extends Command
{
	@Parameter(index = 0)
	private String message;

	public Away()
	{
		super("AWAY");
	}

	@Override
	public void execute(LocalUser user)
	{
		// XXX flood endgrace

		if (message == null)
		{
			if (user.getAway() != null)
			{
				UserAway away = new UserAway();

				away.setUser(user.getId());
				away.setWhen(Instant.now());

				getPlexus().getClient().submit(away);
			}
			else
			{
				user.writeNumeric(Numeric.RPL_UNAWAY);
			}
			
			return;
		}

		// XXX pace wait based on user's last away
		if (message.length() > IRC.AWAYLEN)
		{
			message = message.substring(0, IRC.AWAYLEN);
		}

		UserAway away = new UserAway();

		away.setUser(user.getId());
		away.setWhen(Instant.now());
		away.setMessage(message);
		
		getPlexus().getClient().submit(user.getClient(), away);
	}
}
