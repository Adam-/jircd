/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.NumericBuilder;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.users.Capability;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Names extends Command
{
	@Parameter(index = 0, required = true)
	private String channel;
	
	public Names()
	{
		super("NAMES");	
	}
	
	@Override
	public void execute(LocalUser user)
	{
		Channel c = getPlexus().getIrc().findChannel(channel);
		if (c == null)
		{
			user.writeNumeric(Numeric.RPL_ENDOFNAMES, channel);
			return;
		}
		
		// :me.name 353 user.name [@*=] channel.name :
		// @ = secret, * = priv, otherwise =
		char t;
		if (c.hasMode(getPlexus().getModeManager().getSecret()))
			t = '@';
		else if (c.hasMode(getPlexus().getModeManager().getPriv()))
			t = '*';
		else
			t = '=';
		
		boolean multiPrefix = user.getClient().hasCap(Capability.MULTI_PREFIX), uhnames = user.getClient().hasCap(Capability.USERHOST_IN_NAMES);
		
		if (!c.hasMode(getPlexus().getModeManager().getSecret()) || user.findChannel(c) != null)
		{
			NumericBuilder nb = new NumericBuilder(Numeric.RPL_NAMEREPLY, user, t, c.getName());
			
			for (Membership mem : c.getUsers())
			{
				User u = mem.getUser();
				String prefix;
				if (mem.getModes() != null)
					prefix = multiPrefix ? mem.getPrefixes() : mem.getPrefixes().substring(0, 1);
				else
					prefix = "";
				
				String entry;
				if (uhnames)
					entry = u.getName() + "!" + u.getUsername() + "@" + u.getVhost();
				else
					entry = u.getName();
				
				nb.add(prefix + entry);
			}
			
			nb.send();
		}
		
		user.writeNumeric(Numeric.RPL_ENDOFNAMES, c.getName());
	}

	public String getChannel()
	{
		return channel;
	}

	public void setChannel(String channel)
	{
		this.channel = channel;
	}
}