/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.commands.InviteUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Invite extends Command
{
	@Parameter(index = 0, required = true)
	private String targetUser;

	@Parameter(index = 1, required = true)
	private String channel;

	public Invite()
	{
		super("INVITE");
	}

	@Override
	public void execute(LocalUser user)
	{
		// end grace

		Channel c = getPlexus().getIrc().findChannel(channel);
		User target = getPlexus().getIrc().findUser(targetUser);

		if (target == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHNICK, targetUser);
			return;
		}

		if (c == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHCHANNEL, channel);
			return;
		}

		Membership mem = c.findUser(user);
		if (mem == null)
		{
			user.writeNumeric(Numeric.ERR_NOTONCHANNEL, c.getName());
			return;
		}
		
		ModeManager modeManager = getPlexus().getModeManager();

		if (!c.hasMode(modeManager.getInviteOnly()) || c.hasMode(modeManager.getPriv()))
		{
			if (!mem.hasMode(modeManager.getOwner()) && !mem.hasMode(modeManager.getProtect()) &&
					!mem.hasMode(modeManager.getOp()) && !mem.hasMode(modeManager.getHalfop()))
			{
				user.writeNumeric(Numeric.ERR_CHANOPRIVSNEEDED, c.getName());
				return;
			}
		}

		if (c.findUser(target) != null)
		{
			user.writeNumeric(Numeric.ERR_USERONCHANNEL, target.getName(), c.getName());
			return;
		}

		// XXX anti spam stuff, +g, etc.

		user.writeNumeric(Numeric.RPL_INVITING, target.getName(), c.getName());

		if (target.getAway() != null)
			user.writeNumeric(Numeric.RPL_AWAY, target.getName(), target.getAway());

		InviteUser iu = new InviteUser();
		iu.setSource(user.getId());
		iu.setDest(target.getId());
		iu.setChannel(c.getName());
		iu.setChannelTs(c.getTs());
		iu.setTs(Instant.now());

		getPlexus().getClient().submit(user.getClient(), iu);
	}
}