/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.replication.commands.NickChange;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import net.rizon.plexus.util.Util;
import net.rizon.plexus.xline.XLine;
import net.rizon.plexus.xline.XLineType;

public class Nick extends Command
{
	@Parameter(index = 0, required = true)
	private String nick;

	public Nick()
	{
		super("NICK");
	}

	@Override
	public void executeUnregistered(Client client)
	{
		if (nick.length() > IRC.NICKLEN)
			nick = nick.substring(0, IRC.NICKLEN);

		if (!IRC.isValidNick(nick))
		{
			client.writeNumeric(Numeric.ERR_ERRONEUSNICKNAME, nick);
			return;
		}

		if (client.getIline() == null || !client.getIline().getFlags().contains("resv_exempt"))
		{
			XLine resv = getPlexus().getXlineManager().findXline(XLineType.RESV, nick);
			if (resv != null)
			{
				client.writeNumeric(Numeric.ERR_ERRONEUSNICKNAME, nick, resv.getReason());
				getPlexus().getClientManager().sendToOps(getPlexus().getModeManager().getReject(),
					"Forbidding reserved nick %s from user %s[%s@%s]",
					nick, client.getName() != null ? client.getName() : "unknown", client.getUsername() != null ? client.getUser() : "unknown", client.getHost());
				return;
			}
		}

		if (getPlexus().getIrc().findUser(nick) != null)
		{
			client.writeNumeric(Numeric.ERR_NICKNAMEINUSE, nick);
			return;
		}

		client.setName(nick);

		client.register();
	}

	@Override
	public void execute(LocalUser user)
	{
		if (!IRC.isValidNick(nick))
		{
			user.writeNumeric(Numeric.ERR_ERRONEUSNICKNAME, nick);
			return;
		}

		// endgrace

		if (!user.getClient().getIline().getFlags().contains("resv_exempt") && !user.isOper())
		{
			XLine resv = getPlexus().getXlineManager().findXline(XLineType.RESV, nick);
			if (resv != null)
			{
				user.writeNumeric(Numeric.ERR_ERRONEUSNICKNAME, nick, resv.getReason());
				getPlexus().getClientManager().sendToOps(getPlexus().getModeManager().getReject(),
					"Forbidding reserved nick %s from user %s[%s@%s]",
					nick, user.getName(), user.getUsername(), user.getHost());
				return;
			}
		}

		User target = getPlexus().getIrc().findUser(nick);
		if (target != null)
		{
			if (target != user)
			{
				user.writeNumeric(Numeric.ERR_NICKNAMEINUSE, nick);
				return;
			}

			if (nick.equals(user.getName()))
				return;
		}

		if (user.getLastNickChange() + getPlexus().getConf().getGeneral().getMax_nick_time() < Util.currentTime())
			user.setNumberOfNickChanges(0);
		if (getPlexus().getConf().getGeneral().getMax_nick_changes() > 0 && !user.isOper() && user.getNumberOfNickChanges() > getPlexus().getConf().getGeneral().getMax_nick_changes())
		{
			user.writeNumeric(Numeric.ERR_NICKTOOFAST, user.getName(), nick, getPlexus().getConf().getGeneral().getMax_nick_time());
			return;
		}
		user.setLastNickChange(Util.currentTime());
		user.setNumberOfNickChanges(user.getNumberOfNickChanges() + 1);

		NickChange nc = new NickChange();
		nc.setSource(user.getId());
		nc.setSourceTs(Instant.ofEpochSecond(user.getTs()));
		nc.setNewNick(nick);
		nc.setTs(Instant.now());

		getPlexus().getClient().submit(user.getClient(), nc, (res, ex) -> {
			if (ex == null)
				return;

			user.writeNotice("Unable to change nick: ", ex.getMessage());
		});
	}

	public String getNick()
	{
		return nick;
	}

	public void setNick(String nick)
	{
		this.nick = nick;
	}
}

