/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.time.Instant;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.messages.MessageMessage;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.commands.MessageChannel;
import net.rizon.plexus.replication.commands.MessageUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import net.rizon.plexus.util.Util;

public class Privmsg extends Command
{
	@Parameter(index = 0, required = true, targets = true)
	private String[] targets;

	@Parameter(index = 1, required = true)
	private String message;
	
	public Privmsg()
	{
		super("PRIVMSG");
	}
	
	@Override
	public void execute(LocalUser user)
	{
		for (String target : targets)
		{
			Channel c = null;
			User u = null;
			
			ModeManager modeManager = getPlexus().getModeManager();
			
			if (getPlexus().getIrc().isValidChannel(target))
			{
				c = getPlexus().getIrc().findChannel(target);
				
				if (c == null)
				{
					user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
					continue;
				}
			}
			else if (IRC.isValidNick(target))
			{
				u = getPlexus().getIrc().findUser(target);
				
				if (u == null)
				{
					user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
					continue;
				}
			}
			else
			{
				/* status msg, server notice, user@server, etc */
				
				// server notice requires na
			}
			
			if (c != null)
			{
				// resv
				
				Membership mem = c.findUser(user);
				if (mem == null && c.hasMode(modeManager.getNoExternal()))
				{
					user.writeNumeric(Numeric.ERR_CANNOTSENDTOCHAN, c.getName());
					continue;
				}
				
				if (user.isNetAdmin())
					; // Net admins can override anything
				// Users with any status (voice+) can override the following
				else if (mem != null && mem.getModes() == null)
				{
					// msg throttle flooding
					
					// XXX +b, +N
					
					if (!modeManager.getModerated().checkMessage(user, c, true))
						continue;

					if (!modeManager.getModReg().checkMessage(user, c, true))
						continue;
					
					if (!modeManager.getCm_noCtcp().checkMessage(user, c, true, message))
						continue;
					
					if (!modeManager.getNoCtrl().checkMessage(user, c, true, message))
						continue;
				}
				
				user.setIdle(Util.currentTime());
				
				MessageChannel event = new MessageChannel();
				event.setSource(user.getId());
				event.setCommand(MessageMessage.Command.PRIVMSG);
				event.setChannel(c.getName());
				event.setMessage(message);
				event.setTime(Instant.now());
				
				getPlexus().getClient().submit(event);
			}
			else if (u != null)
			{
				if (!modeManager.getUm_noCtcp().checkMessage(user, u, true, message))
					return;
				
				if (!modeManager.getUm_regOnly().checkMessage(user, u, true))
					return;
				
				if (u.getAway() != null)
					user.writeNumeric(Numeric.RPL_AWAY, u.getName(), u.getAway());
				
				if (!modeManager.getCallerId().onMessage(user, u, true) || !modeManager.getSoftCallerId().onMessage(user, u, true))
					return;
				
				//flood check
				
				user.setIdle(Util.currentTime());
				
				if (u.isLocal())
				{
					Message m = new MessageMessage(user, MessageMessage.Command.PRIVMSG, u.getName(), message);
					((LocalUser) u).writeMessage(m);
				}
				else
				{
					MessageUser event = new MessageUser();
					event.setSource(user.getId());
					event.setCommand(MessageMessage.Command.PRIVMSG);
					event.setTarget(u.getId());
					event.setMessage(message);
					event.setTime(Instant.now());
					
					getPlexus().getClient().submit(event);
				}
			}
		}
	}
}