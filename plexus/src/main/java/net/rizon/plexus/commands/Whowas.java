/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.users.LocalUser;

public class Whowas extends Command
{
	@Parameter(index = 0, required = true)
	private String nick;

	@Parameter(index = 1)
	private Integer limit;

	public Whowas()
	{
		super("WHOWAS");
	}

	@Override
	public int getGlobalDelay() { return getPlexus().getConf().getGeneral().getPace_wait(); }

	private static final DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");

	@Override
	public void execute(LocalUser user)
	{
		Collection<net.rizon.plexus.commands.whowas.Whowas> w = getPlexus().getWhowas().getWhowasFor(nick);

		if (w == null)
		{
			user.writeNumeric(Numeric.ERR_WASNOSUCHNICK, nick);
			user.writeNumeric(Numeric.RPL_ENDOFWHOWAS, nick);
			return;
		}

		int shown = 0;
		for (net.rizon.plexus.commands.whowas.Whowas whowas : w)
		{
			if (limit > 0 && ++shown > limit)
				break;

			user.writeNumeric(Numeric.RPL_WHOWASUSER, whowas.getName(), whowas.getUsername(), user.isOper() ? whowas.getHost() : whowas.getVhost(), whowas.getGecos());
			if (whowas.getAccount() != null)
				user.writeNumeric(Numeric.RPL_WHOISLOGGEDIN, whowas.getName(), whowas.getAccount());

			String visableServerName;
			if (!user.isOper() && (whowas.isHide() || getPlexus().getConf().getServerhide().isHide_servers()))
				visableServerName = getPlexus().getConf().getServerhide().getHidden_name();
			else
				visableServerName = whowas.getServer();

			user.writeNumeric(Numeric.RPL_WHOISSERVER, whowas.getName(), visableServerName, format.format(whowas.getLogoff()));
		}

		user.writeNumeric(Numeric.RPL_ENDOFWHOWAS, nick);
	}
}