/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.NumericBuilder;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import net.rizon.plexus.util.Util;

public class Whois extends Command
{
	@Parameter(index = 0, required = true)
	private String server;

	@Parameter(index = 1)
	private String target;

	public Whois()
	{
		super("WHOIS");
	}

	private void doWhois(LocalUser user, User target)
	{
		user.writeNumeric(Numeric.RPL_WHOISUSER, target.getName(), target.getUsername(), target.getVhost(), target.getGecos());

		NumericBuilder nb = new NumericBuilder(Numeric.RPL_WHOISCHANNELS, user, target.getName());
		for (Membership m : target.getChannels())
		{
			Channel c = m.getChannel();
			String prefix = m.getPrefixes();

			nb.add(prefix + c.getName());
		}
		nb.send();

		if (!user.isOper() && user != target && (getPlexus().getConf().getServerhide().isHide_servers() || target.getServer().isHidden()))
			user.writeNumeric(Numeric.RPL_WHOISSERVER, target.getName(), getPlexus().getConf().getServerhide().getHidden_name(), getPlexus().getConf().getServerinfo().getNetwork_desc());
		else
			user.writeNumeric(Numeric.RPL_WHOISSERVER, target.getName(), target.getServer().getName(), target.getServer().getDescription());

		if (target.getAway() != null)
			user.writeNumeric(Numeric.RPL_AWAY, target.getName(), target.getAway());

		ModeManager modeManager = getPlexus().getModeManager();

		// caller id
		if (!modeManager.getCallerId().onWhois(user, target))
			modeManager.getSoftCallerId().onWhois(user, target);

		if (target.isOper())
		{
			String buf = "is an IRC Operator";
			if (target.hasMode(modeManager.getService()))
				buf += " - Network Service";
			else if (target.hasMode(modeManager.getNetadmin()))
				buf += " - Network Administrator";
			else if (target.hasMode(modeManager.getAdmin()))
				buf += " - Server Administrator";

			user.writeNumeric(Numeric.RPL_WHOISOPERATOR, target.getName(), buf);
		}

		// captured
		if (target.isLocal() && user.isOper())
		{
			LocalUser lu = (LocalUser) target;
			if (lu.isCaptured())
				user.writeNumeric(Numeric.RPL_ISCAPTURED, target.getName());
		}

		// +R
		modeManager.getRegistered().whois(user, target);

		if (getPlexus().getConf().getGeneral().isAccount_whois() && target.getAccount() != null)
			user.writeNumeric(Numeric.RPL_WHOISLOGGEDIN, target.getName(), target.getAccount());

		// SSL
		modeManager.getSsl().whois(user, target);

		modeManager.getWebirc().whois(user, target);

		// umodes, auth flags
		if (user == target || user.isOper())
			user.writeNumeric(Numeric.RPL_WHOISMODES, target.getName(), "+" + target.getModes(), target.getAuthflags() != null ? target.getAuthflags() : "[none]");

		// is actually
		if (user == target || user.isOper())
			user.writeNumeric(Numeric.RPL_WHOISACTUALLY, target.getName(), "is actually " + target.getUsername() + "@" + target.getHost() + " [" + target.getIp() + "]");

		if (modeManager.getCloak().isEnabled() && getPlexus().getConf().getGeneral().isCloak_whois_actually() && target.getChost() != null)
		{
			if (!target.getChost().equals(target.getCip()) && (user == target || user.isOper()))
				user.writeNumeric(Numeric.RPL_WHOISACTUALLY, target.getName(), "has cloak " + target.getChost() + "[ " + target.getCip() + "]");
			else if (!target.getVhost().equals(target.getCip()))
				user.writeNumeric(Numeric.RPL_WHOISACTUALLY, target.getName(), "has cloak " + target.getCip());
		}

		// idle
		if (user == target || user.isOper() || !user.hasMode(modeManager.getHideChannels()))
			if (target.isLocal())
			{
				long now = Util.currentTime();
				LocalUser targetLocal = (LocalUser) target;

				user.writeNumeric(Numeric.RPL_WHOISIDLE, target.getName(), now - targetLocal.getIdle(), target.getSignon());
			}

		// tell the user they are being whoised
		if (user != target && target.isOper() && target.hasMode(modeManager.getSpy()))
			target.writeNotice("*** Notice -- %s (%s@%s) is doing a whois on you [%s]",
					user.getName(), user.getUsername(), user.getHost(), user.getServer().getName());

		user.writeNumeric(Numeric.RPL_ENDOFWHOIS, target.getName());
	}

	@Override
	public void execute(LocalUser user)
	{
		if (target == null)
		{
			target = server;
			server = null;
		}

		User u = getPlexus().getIrc().findUser(target);

		if (u == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
			user.writeNumeric(Numeric.RPL_ENDOFWHOIS, target);
			return;
		}

		this.doWhois(user, u);
	}
}
