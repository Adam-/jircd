/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.replication.commands.XLineAdd;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;
import net.rizon.plexus.xline.XLine;
import net.rizon.plexus.xline.XLineManager;
import net.rizon.plexus.xline.XLineType;

public class Resv extends Command
{
	@Parameter(index = 0, required = true)
	private String[] params;
	
	public Resv()
	{
		super("RESV");
		
		this.setRequiresOper(true);
	}
	
	@Override
	public void execute(LocalUser user)
	{
		if (!user.getOper().hasPriv("resv"))
		{
			user.writeNumeric(Numeric.ERR_NOPRIVS, this.getName());
			return;
		}
		
		// RESV [time] mask :reason | oper reason

		int time;
		int curIdx = 0;
		try
		{
			time = Util.getSeconds(params[0]);
			++curIdx;
		}
		catch (NumberFormatException ex)
		{
			time = 0;
		}
		
		String mask = params[curIdx++];
		
		if (curIdx >= params.length)
		{
			user.writeNumeric(Numeric.ERR_NEEDMOREPARAMS, this.getName());
			return;
		}
		
		// hybrid accepts ON server.name here, requiring remote ban oper priv
		
		String reason = params[curIdx];
		for (int i = curIdx + 1; i < params.length; ++i)
			reason += " " + params[i];
		
		int i = reason.lastIndexOf('|');
		String oreason = null;
		if (i != -1)
		{
			oreason = reason.substring(i + 1).trim();
			reason = reason.substring(0, i).trim();
		}
		
		XLineManager xlineManager = getPlexus().getXlineManager();
		long now = Util.currentTime();
		
		net.rizon.plexus.xline.Resv xl = new net.rizon.plexus.xline.Resv(xlineManager);
		xl.setMask(mask);
		xl.setCreator(user.getName());
		xl.setCreated(now);
		xl.setExpires(time > 0 ? now + time : 0);
		xl.setReason(reason);
		xl.setOreason(oreason);
		
		XLine e = xlineManager.findXLine(xl);
		if (e != null)
		{
			user.writeNotice("%s is already RESV: %s", e.getMask(), e.getReason());
			return;
		}
		
		if (time != 0)
		{
			user.writeNotice("Added temporary %d min. RESV [%s]", time / 60, xl.getMask());
		}
		else
		{
			user.writeNotice("Added RESV [%s]", xl.getMask());
		}
		
		// apply it now, allow handler to add it
		xlineManager.apply(xl);

		XLineAdd xa = new XLineAdd();
		xa.setSource(user.getId());
		xa.setXlineType(XLineType.RESV);
		xa.setMask(xl.getMask());
		xa.setCreator(xl.getCreator());
		xa.setReason(xl.getReason());
		xa.setOreason(xl.getOreason());
		xa.setCreated(xl.getCreated());
		xa.setExpires(xl.getExpires());

		getPlexus().getClient().submit(xa);
	}
}