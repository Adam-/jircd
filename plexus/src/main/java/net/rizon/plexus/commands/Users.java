/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.IRCStats;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.users.LocalUser;

public class Users extends Command
{
	public Users()
	{
		super("USERS");
	}

	@Override
	public int getGlobalDelay()
	{
		return getPlexus().getConf().getGeneral().getPace_wait_simple();
	}

	@Override
	public void execute(LocalUser user)
	{
		IRCStats stats = getPlexus().getStats();

		if (!user.isOper() && getPlexus().getConf().getServerhide().isHide_servers())
		{
			user.writeNumeric(Numeric.RPL_LOCALUSERS, getPlexus().getIrc().getUsers().size(), stats.getMaxTotal());
		}
		else
		{
			user.writeNumeric(Numeric.RPL_LOCALUSERS, getPlexus().getClientManager().getLocalUsers().size(), stats.getMaxLocal());
		}

		user.writeNumeric(Numeric.RPL_GLOBALUSERS, getPlexus().getIrc().getUsers().size(), stats.getMaxTotal());
	}
}
