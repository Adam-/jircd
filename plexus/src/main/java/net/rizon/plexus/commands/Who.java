/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Who extends Command
{
	@Parameter(index = 0, required = true)
	private String target;

	@Parameter(index = 1)
	private String flags;

	public Who()
	{
		super("WHO");
	}

	@Override
	public void execute(LocalUser user)
	{
		boolean opersOnly = flags != null ? flags.charAt(0) == 'o' : false;

		if (target.equals("*"))
		{
			return;
		}

		if (!getPlexus().getIrc().isValidChannel(target))
		{
			return;
		}

		Channel chan = getPlexus().getIrc().findChannel(target);
		if (chan != null)
		{
			boolean isMember = user.findChannel(chan) != null;
			ModeManager modeManager = getPlexus().getModeManager();

			for (Membership mem : chan.getUsers())
			{
				if (isMember || user.isAdmin()
					|| (!user.hasMode(modeManager.getInvisible()) && !user.isServices() && !user.hasMode(modeManager.getHideChannels())))
				{
					User u = mem.getUser();

					if (opersOnly && !u.isOper())
					{
						continue;
					}

					String prefix = mem.getPrefixes();
					if (prefix.length() > 1)
					{
						prefix = prefix.substring(0, 1);
					}

					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.append(u.getAway() != null ? 'G' : 'H');
					stringBuilder.append(u.hasMode(modeManager.getRegistered()) ? "r" : "");
					stringBuilder.append(u.isOper() ? "*" : "");
					stringBuilder.append(prefix);

					String serverName;
					if (!user.isOper() && (getPlexus().getConf().getServerhide().isHide_servers() || u.getServer().isHidden()))
						serverName = getPlexus().getConf().getServerhide().getHidden_name();
					else
						serverName = u.getServer().getName();

					user.writeNumeric(Numeric.RPL_WHOREPLY,
						chan.getName(),
						u.getUsername(),
						u.getVhost(),
						serverName,
						u.getName(),
						stringBuilder.toString(),
						0,
						user.getGecos());
				}
			}
		}

		user.writeNumeric(Numeric.RPL_ENDOFWHO, target);
	}
}
