/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.commands;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.Parameter;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.replication.commands.CaptureUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Capture extends Command
{
	@Parameter(index = 0, required = true)
	private String target;

	public Capture()
	{
		super("CAPTURE");

		this.setRequiresOper(true);
	}

	@Override
	public void execute(LocalUser user)
	{
		if (!user.isNetAdmin())
		{
			user.writeNumeric(Numeric.ERR_NOPRIVS, this.getName());
			return;
		}

		User u = getPlexus().getIrc().findUser(target);
		if (u == null)
		{
			user.writeNumeric(Numeric.ERR_NOSUCHNICK, target);
			return;
		}
		
		if (u.isOper())
		{
			user.writeNumeric(Numeric.ERR_NOPRIVS, this.getName());
			return;
		}

		CaptureUser cu = new CaptureUser();
		cu.setSource(user.getId());
		cu.setUser(u.getId());
		cu.setAdd(true);

		getPlexus().getClient().submit(cu);

		user.writeNumeric(Numeric.RPL_ISCAPTURED, u.getName());
	}
}