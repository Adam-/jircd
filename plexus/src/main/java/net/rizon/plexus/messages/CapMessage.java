/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.messages;

import java.util.Objects;
import net.rizon.plexus.Entity;
import net.rizon.plexus.Message;
import net.rizon.plexus.Parameter;

public class CapMessage extends Message
{
	@Parameter(index = 0)
	private String target;

	@Parameter(index = 1)
	private String subcommand;

	@Parameter(index = 2)
	private String what;

	public CapMessage(Entity source, String target, String subcommand, String what)
	{
		super(source, "CAP");

		this.target = target;
		this.subcommand = subcommand;
		this.what = what;
	}

	@Override
	public String toString()
	{
		return "CapResponse{" + "target=" + target + ", subcommand=" + subcommand + ", what=" + what + '}';
	}

	@Override
	public int hashCode()
	{
		int hash = 3;
		hash = 41 * hash + Objects.hashCode(this.target);
		hash = 41 * hash + Objects.hashCode(this.subcommand);
		hash = 41 * hash + Objects.hashCode(this.what);
		return hash;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final CapMessage other = (CapMessage) obj;
		if (!Objects.equals(this.target, other.target))
		{
			return false;
		}
		if (!Objects.equals(this.subcommand, other.subcommand))
		{
			return false;
		}
		if (!Objects.equals(this.what, other.what))
		{
			return false;
		}
		return true;
	}
}
