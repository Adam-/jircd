/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import com.google.common.base.Strings;
import io.netty.channel.Channel;
import io.netty.channel.ChannelConfig;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.handler.ssl.SslHandler;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.UUID;
import net.rizon.plexus.command.Command;
import net.rizon.plexus.command.CommandStats;
import net.rizon.plexus.config.Allow;
import net.rizon.plexus.config.Auth;
import net.rizon.plexus.dns.ClientDnsResolver;
import net.rizon.plexus.ident.ClientIdentLookup;
import net.rizon.plexus.ident.IdentClient;
import net.rizon.plexus.io.IRCMessage;
import net.rizon.plexus.io.SSL;
import net.rizon.plexus.io.ThrottleHandler;
import net.rizon.plexus.messages.ErrorMessage;
import net.rizon.plexus.messages.MessageMessage;
import net.rizon.plexus.messages.UserModeMessage;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.commands.IntroduceUser;
import net.rizon.plexus.users.Capability;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.util.Util;
import net.rizon.plexus.xline.XLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* A locally connected socket */
public class Client implements Messageable
{
	private static final Logger logger = LoggerFactory.getLogger(Client.class);

	private final Plexus plexus;
	private LocalUser user;

	private String ip;
	private String host; // resolved host, if no host, then host = ip
	private boolean exited;

	private String name;

	private String username;
	private String clientHost;
	private String serverHost;
	private String gecos;

	private String password;

	private String webircIp;
	private String webircHost;

	private int caps;

	private long serial;

	/**
	 * when this client was first created
	 */
	private long firstTime;
	/** last time something was received from this client */
	private long lastTime;
	/** has a PING sent out with no reply */
	private boolean pinged;

	private Integer pingCookie;

	/**
	 * Set when the client is waiting for a command to finish executing, and
	 * no other commands should be processed.
	 */
	private boolean waitingForCommand;

	/** in client capability negotiation
	 */
	private boolean cap;

	/** iline tied to this client, required to register
	 */
	private Auth iline;

	/** allow/class tied to this client
	 */
	private Allow allow;

	/** pending ident lookup
	 */
	private ClientIdentLookup ident;

	/** pending dns lookup
	 */
	private ClientDnsResolver dns;

	/**
	 * Socket for the client
	 */
	private final Channel channel;

	private long sentMessages;
	private long recvMessages;

	private final Deque<IRCMessage> in = new ArrayDeque<>();

	public Client(Plexus plexus, Channel channel) throws IOException
	{
		this.plexus = plexus;
		this.channel = channel;

		InetSocketAddress remote = (InetSocketAddress) channel.remoteAddress();
		this.ip = this.host = remote.getAddress().getHostAddress();
		this.firstTime = this.lastTime = Util.currentTime();
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	@Override
	public String toString()
	{
		return "Client{name=" + name + "}";
	}

	@Override
	public ChannelFuture writeMessage(Message message)
	{
		return this.write(message);
	}

	@Override
	public ChannelFuture writeNumeric(Numeric numeric, Object... args)
	{
		Message message = numeric.buildMessage(plexus.getMe(), args);
		return this.writeMessage(message);
	}

	@Override
	public ChannelFuture writeNotice(String message, Object... args)
	{
		return this.writeMessage(new MessageMessage(plexus.getMe(),
			MessageMessage.Command.NOTICE,
			this.getMessageName(),
			String.format(message, args)));
	}

	public boolean isUser()
	{
		return user != null;
	}

	public void addCap(Capability cap)
	{
		this.caps |= (1 << cap.ordinal());
	}

	public void delCap(Capability cap)
	{
		this.caps &= ~(1 << cap.ordinal());
	}

	public boolean hasCap(Capability cap)
	{
		return (this.caps & (1 << cap.ordinal())) != 0;
	}

	public void clearCaps()
	{
		caps = 0;
	}

	/* exits this client by calling exit on the associated entry */
	public void exit(String reason)
	{
		if (exited)
		{
			return;
		}
		exited = true;

		if (user == null)
		{
			error(reason); // fast exit without needing to modify state
		}
		else
		{
			user.exit(reason);
		}
	}

	/**
	 * send ERROR to the client and queue close()
	 * @param reason
	 */
	public void error(String reason)
	{
		exited = true;

		if (this.dns != null)
		{
			this.dns.cancel();
		}

		if (this.ident != null)
		{
			this.ident.cancel();
		}

		ChannelFuture future = this.writeMessage(new ErrorMessage("Closing link: " + ip + " (" + reason + ")"));
		if (future != null)
		{
			/* client gets removed from client manager once the channel goes inactive from this */
			future.addListener(ChannelFutureListener.CLOSE);
		}
	}

	public void startAuth()
	{
		// XXX throttle check

		// XXX dline exempt, dline check?

		this.dns = new ClientDnsResolver(this, this::finishDns);
		this.dns.start();

		if (!plexus.getConf().getGeneral().isDisable_auth())
		{
			this.ident = new ClientIdentLookup(this, this::finishIdent);
			this.ident.start();
		}
	}

	private void parse(IRCMessage message)
	{
		String command = message.getCommand();
		String[] params = message.getParams();

		Command cmd = plexus.getCommandManager().findCommand(command);
		if (cmd == null)
		{
			if (this.user != null)
			{
				this.writeNumeric(Numeric.ERR_UNKNOWNCOMMAND, command);
			}

			return;
		}

		CommandStats stats = plexus.getCommandManager().getStats(cmd.getClass());
		stats.inc();

		// XXX cmd throttle?
		if (this.isUser())
		{
			if (this.user.isCaptured() && !cmd.isWorksThroughCapture())
			{
				return;
			}

			if (cmd.isRequiresOper() && !this.user.isOper())
			{
				this.writeNumeric(Numeric.ERR_NOPRIVILEGES);
				return;
			}

			if (!this.user.isOper() && stats.getLastUsed().toEpochMilli() / 1000L + cmd.getGlobalDelay() > Util.currentTime())
			{
				user.writeNumeric(Numeric.RPL_LOAD2HI);
				return;
			}

			stats.resetLastUsed();

			try
			{
				cmd.unpack(params);

				if (!cmd.hasRequiredFields())
				{
					this.writeNumeric(Numeric.ERR_NEEDMOREPARAMS, cmd.getName());
					return;
				}

				cmd.execute(this.user);
			}
			catch (Exception ex)
			{
				logger.warn("Unable to process command", ex);
			}
		}
		else
		{
			stats.resetLastUsed();

			try
			{
				cmd.unpack(params);

				if (!cmd.hasRequiredFields())
				{
					return;
				}

				cmd.executeUnregistered(this);
			}
			catch (Exception ex)
			{
				logger.warn("Unable to process command", ex);
			}
		}
	}

	private void finishDns()
	{
		dns = null;

		logger.debug("dns auth for {} completed", this);

		register();
	}

	private void finishIdent()
	{
		ident = null;

		logger.debug("ident lookup for {} completed", this);

		register();
	}

	/* attempt to register this client as a user */
	public void register()
	{
		assert this.user == null;

		if (this.dns != null) // DNS hasn't finished yet
		{
			return;
		}

		if (this.ident != null) // Ident hasnt finished yet
		{
			return;
		}

		if (this.getName() == null || this.username == null || this.cap)
		{
			return;
		}

		if (this.pingCookie != null)
		{
			return;
		}

		if (plexus.getIrc().findUser(this.getName()) != null)
		{
			this.writeNumeric(Numeric.ERR_NICKNAMEINUSE, this.getName());
			return;
		}

		// iline, other auth
		if (this.username.length() > IRC.USERLEN)
		{
			this.username = this.username.substring(0, IRC.USERLEN);
		}
		if (!IRC.isValidUsername(this.username.startsWith(IdentClient.NO_IDENT_PREFIX) ? this.username.substring(1) : this.username))
		{
			this.exit("Invalid username [" + this.username + "]");
			return;
		}

		UUID uid = UUID.randomUUID();
		if (plexus.getIrc().findUser(uid) != null)
		{
			this.exit("ID collision");
			return;
		}

		// kline, xline, dnsbl?
		XLine xl = plexus.getXlineManager().findXLine(this);
		if (xl != null)
		{
			xl.onMatch(this);
			return;
		}

		this.iline = plexus.getConf().findILine(this);
		if (this.iline == null)
		{
			if (this.exited) // findILine can exit the client
			{
				return;
			}

			plexus.getClientManager().sendToOps(plexus.getModeManager().getUnauth(),
				"Unauthorized client connection from %s [%s]",
				this.getName(), this.ip);
			this.exit("You are not authorized to use this server");
			return;
		}

		setAllow(iline.getAllow());

		// server full? class full?
		String userHost = host;
		String userIp = ip;
		String userModes = "";
		String cloakedIp = null;
		String cloakedHost = null;
		String userVhost = userHost;
		boolean cloakUser = true;

		if (this.webircIp != null && this.webircHost != null)
		{
			userVhost = userHost = this.webircHost;
			userIp = this.webircIp;

			userModes += plexus.getModeManager().getWebirc().getCharacter();

			this.writeNotice("*** CGI:IRC Host spoofing active");

		}
		else if (this.iline.getSpoof() != null)
		{
			userVhost = userHost = this.iline.getSpoof();
			userIp = "255.255.255.255";
			cloakUser = false;

			this.writeNotice("*** Spoofing your IP. congrats.");
		}

		if (cloakUser)
		{
			cloakedIp = plexus.getModeManager().getCloak().cloak(userIp);
			cloakedHost = plexus.getModeManager().getCloak().cloak(userHost);
		}

		if (plexus.getConf().getGeneral().getModes_on_connect() != null)
		{
			for (int i = 0; i < plexus.getConf().getGeneral().getModes_on_connect().length(); ++i)
			{
				char c = plexus.getConf().getGeneral().getModes_on_connect().charAt(i);
				UserMode um = plexus.getModeManager().findUserMode(c);

				if (um == null || um.isRestricted())
				{
					continue;
				}

				userModes += um.getCharacter();
			}

			if (userModes.contains("" + plexus.getModeManager().getCloak().getCharacter()) && cloakUser)
			{
				userVhost = cloakedHost;

				this.writeNotice("*** Your host is masked (%s)", cloakedHost);
			}
		}

		IntroduceUser iu = new IntroduceUser();
		iu.setNickname(this.getName());
		iu.setSignon(Util.currentTime());
		iu.setModes(userModes);
		iu.setUsername(this.username);
		iu.setHostname(userHost);
		iu.setIp(userIp);
		iu.setCloakedHost(cloakedHost);
		iu.setCloakedIp(cloakedIp);
		iu.setVhost(userVhost);
		iu.setId(uid);
		iu.setRealhost(this.host);
		iu.setGecos(this.gecos);
		iu.setServerId(plexus.getMe().getId());

		plexus.getClient().<UUID>submit(this, iu, this::register);
	}

	/**
	 * Called when the user gets registered on the cluster
	 *
	 * @param id uid of the user
	 * @param ex exception, if one occurred
	 */
	private void register(UUID id, Throwable ex)
	{
		if (ex != null)
		{
			logger.warn("Client " + this + " failed to register", ex);

			this.exit(ex.getMessage());
			return;
		}

		if (id == null)
		{
			// could be a collision
			this.exit("Unable to register");
			return;
		}

		this.user = (LocalUser) plexus.getIrc().findUser(id);
		if (this.user == null)
		{
			logger.warn("User registered with cluster but can't be found");

			this.exit("Lost user");
			return;
		}

		assert this.user.getClient() == null;
		this.user.setClient(this);

		// max glocal users
		IRCStats stats = plexus.getStats();
		int curLocal = plexus.getClientManager().getLocalUsers().size();
		int maxLocal = stats.getMaxLocal();

		if (curLocal > maxLocal)
		{
			stats.setMaxLocal(curLocal);;

			if (curLocal % 10 == 0)
			{
				plexus.getClientManager().sendToOps(null, "New max local clients: %d", curLocal);
			}
		}

		// k/d line exempt
		// resv exempt
		// limits exempt
		// can flood
		SslHandler handler = this.channel.pipeline().get(SslHandler.class);
		if (handler != null)
		{
			this.user.setMode(plexus.getModeManager().getSsl().getCharacter());

			this.writeNotice("*** Connected securely via %s %s", handler.engine().getSession().getProtocol(), handler.engine().getSession().getCipherSuite());
			String certfp = SSL.getCertFP(handler);
			if (certfp != null)
			{
				this.writeNotice("*** Your client certificate fingerprint is %s", certfp);
				this.user.setCertfp(certfp);
			}
		}

		this.writeNumeric(Numeric.RPL_WELCOME, plexus.getConf().getServerinfo().getNetwork_name(), this.getName());
		this.writeNumeric(Numeric.RPL_YOURHOST, plexus.getConf().getServerinfo().getName(), "plexus-dev");
		this.writeNumeric(Numeric.RPL_CREATED, plexus.getStart());

		plexus.getSupported().showNewClient(user);

		// lusers
		{
			Command c = plexus.getCommandManager().findCommand("LUSERS");
			if (c != null)
			{
				c.execute(user);
			}
		}

		// motd
		{
			Command c = plexus.getCommandManager().findCommand("MOTD");
			if (c != null)
			{
				c.execute(user);
			}
		}

		// Show modes
		String umodes = user.getModes();
		if (!Strings.isNullOrEmpty(umodes))
		{
			this.writeMessage(new UserModeMessage(null, this.user, "+" + umodes));
		}
	}

	public boolean isExceedingSendQ()
	{
		return channel.isWritable() == false;
	}

	public void onRead(IRCMessage message)
	{
		this.lastTime = Util.currentTime();
		this.pinged = false;

		this.in.addLast(message);

		while (this.process());
	}

	private ChannelFuture write(Message message)
	{
		if (!channel.isActive())
		{
			return null;
		}

		IRCMessage ircmessage = message.pack(this);

		if (!channel.isWritable())
		{
			this.exit("Max SendQ exceeded");
			this.channel.close(); // force a close now
			return null;
		}

		IRCStats stats = plexus.getStats();

		++this.sentMessages;
		stats.incrementTotalSentMessages();

		ChannelFuture future = this.channel.writeAndFlush(ircmessage);

		return future;
	}

	private boolean process()
	{
		IRCStats stats = plexus.getStats();

		while (!this.exited)
		{
			// to emulate synchronous ircds when the client does
			// a command that takes some time for the result
			// to be returned
			if (this.isWaitingForCommand())
			{
				break;
			}

			if (this.in.isEmpty())
			{
				break;
			}

			IRCMessage message = this.in.removeFirst();

			++this.recvMessages;
			stats.incrementTotalRecvMessages();

			this.parse(message);
		}

		return false;
	}

	@Override
	public String getMessageName()
	{
		return name != null ? name : "*";
	}

	@Override
	public boolean isOper()
	{
		return false;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public LocalUser getUser()
	{
		return user;
	}

	public void setUser(LocalUser user)
	{
		this.user = user;
	}

	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getClientHost()
	{
		return clientHost;
	}

	public void setClientHost(String clientHost)
	{
		this.clientHost = clientHost;
	}

	public String getServerHost()
	{
		return serverHost;
	}

	public void setServerHost(String serverHost)
	{
		this.serverHost = serverHost;
	}

	public String getGecos()
	{
		return gecos;
	}

	public void setGecos(String gecos)
	{
		this.gecos = gecos;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public boolean isWaitingForCommand()
	{
		return waitingForCommand;
	}

	public void setWaitingForCommand(boolean waitingForCommand)
	{
		this.waitingForCommand = waitingForCommand;
	}

	public Auth getIline()
	{
		return iline;
	}

	public void setIline(Auth iline)
	{
		this.iline = iline;
	}

	public Allow getAllow()
	{
		return allow;
	}

	public void setAllow(Allow allow)
	{
		this.allow = allow;

		// set socket watermarks
		ChannelConfig config = channel.config();
		config.setWriteBufferWaterMark(new WriteBufferWaterMark(allow.getSendq(), allow.getSendq()));

		// set recvq limits
		ThrottleHandler throttleHandler = channel.pipeline().get(ThrottleHandler.class);
		throttleHandler.setAllow(allow.getRecv_allow());
		throttleHandler.setDuration(Duration.ofSeconds(allow.getRecv_duration()));
		throttleHandler.setQueueLimit(allow.getRecv_limit());
	}

	public Channel getChannel()
	{
		return channel;
	}

	public Integer getPingCookie()
	{
		return pingCookie;
	}

	public void setPingCookie(Integer pingCookie)
	{
		this.pingCookie = pingCookie;
	}

	public String getWebircIp()
	{
		return webircIp;
	}

	public void setWebircIp(String webircIp)
	{
		this.webircIp = webircIp;
	}

	public String getWebircHost()
	{
		return webircHost;
	}

	public void setWebircHost(String webircHost)
	{
		this.webircHost = webircHost;
	}

	public boolean isExited()
	{
		return exited;
	}

	public long getFirstTime()
	{
		return firstTime;
	}

	public void setFirstTime(long firstTime)
	{
		this.firstTime = firstTime;
	}

	public long getLastTime()
	{
		return lastTime;
	}

	public void setLastTime(long lastTime)
	{
		this.lastTime = lastTime;
	}

	public boolean isPinged()
	{
		return pinged;
	}

	public void setPinged(boolean pinged)
	{
		this.pinged = pinged;
	}

	public boolean isCap()
	{
		return cap;
	}

	public void setCap(boolean cap)
	{
		this.cap = cap;
	}

	public long getSerial()
	{
		return serial;
	}

	public void setSerial(long serial)
	{
		this.serial = serial;
	}
}
