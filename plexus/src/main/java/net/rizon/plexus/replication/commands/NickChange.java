/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.commands;

import java.time.Instant;
import java.util.UUID;
import net.rizon.plexus.replication.PlexusCommand;

public class NickChange implements PlexusCommand
{
	private UUID source;
	private Instant sourceTs;
	private String newNick;
	private Instant ts;

	@Override
	public String toString()
	{
		return "NickChange{" + "source=" + source + ", sourceTs=" + sourceTs + ", newNick=" + newNick + ", ts=" + ts + '}';
	}

	public UUID getSource()
	{
		return source;
	}

	public void setSource(UUID source)
	{
		this.source = source;
	}

	public Instant getSourceTs()
	{
		return sourceTs;
	}

	public void setSourceTs(Instant sourceTs)
	{
		this.sourceTs = sourceTs;
	}

	public String getNewNick()
	{
		return newNick;
	}

	public void setNewNick(String newNick)
	{
		this.newNick = newNick;
	}

	public Instant getTs()
	{
		return ts;
	}

	public void setTs(Instant ts)
	{
		this.ts = ts;
	}
}
