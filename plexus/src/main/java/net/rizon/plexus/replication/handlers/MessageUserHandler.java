/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.messages.MessageMessage;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.MessageUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class MessageUserHandler extends CommitHandler<MessageUser>
{
	public MessageUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<MessageUser>> commit)
	{
		MessageUser mu = commit.command().getCommand();
		try
		{
			User target = getIrc().findUser(mu.getTarget());
			if (target == null || !target.isLocal())
			{
				return null;
			}

			User source = getIrc().findUser(mu.getSource());
			if (source == null)
			{
				return null;
			}

			LocalUser localTarget = (LocalUser) target;

			if (!checkAccept(source, localTarget, mu.getCommand() == MessageMessage.Command.PRIVMSG))
			{
				return null;
			}

			MessageMessage message = new MessageMessage(source, mu.getCommand(), target.getName(), mu.getMessage());
			localTarget.writeMessage(message);
			return null;
		}
		finally
		{
			commit.release();
		}
	}

	private boolean checkAccept(User source, LocalUser target, boolean privmsg)
	{
		ModeManager modeManager = getPlexus().getModeManager();
		
		if (!modeManager.getCallerId().onMessage(source, target, privmsg) || !modeManager.getSoftCallerId().onMessage(source, target, privmsg))
		{
			return false;
		}
		
		return true;
	}
}
