/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.serializer;

import com.google.gson.Gson;
import io.atomix.catalyst.buffer.BufferInput;
import io.atomix.catalyst.buffer.BufferOutput;
import io.atomix.catalyst.serializer.Serializer;
import io.atomix.catalyst.serializer.TypeSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class GsonTypeSerializer<T> implements TypeSerializer<T>
{
	private static final Logger logger = LoggerFactory.getLogger(GsonTypeSerializer.class);

	private final Gson gson;

	public GsonTypeSerializer(Gson gson)
	{
		this.gson = gson;
	}

	@Override
	public void write(T object, BufferOutput buffer, Serializer serializer)
	{
		try
		{
			String s = gson.toJson(object);

			logger.trace("Serialized {}", s);

			buffer.writeString(s);
		}
		catch (Exception ex)
		{
			logger.error("Unable to serialize object", ex);
			throw ex;
		}
	}

	@Override
	public T read(Class<T> type, BufferInput buffer, Serializer serializer)
	{
		try
		{
			String s = buffer.readString();

			logger.trace("Unserialized {}", s);

			return gson.fromJson(s, type);
		}
		catch (Exception ex)
		{
			logger.error("Unable to unserialize object", ex);
			throw ex;
		}
	}
}
