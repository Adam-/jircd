/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.Entity;
import net.rizon.plexus.Message;
import net.rizon.plexus.messages.UserModeMessage;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.UserModeSet;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserModeSetHandler extends CommitHandler<UserModeSet>
{
	private static final Logger logger = LoggerFactory.getLogger(UserModeSetHandler.class);

	public UserModeSetHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<UserModeSet>> commit)
	{
		try
		{
			UserModeSet ums = commit.command().getCommand();

			Entity source = getIrc().findEntity(ums.getSource());
			User target = getIrc().findUser(ums.getTarget());

			if (target == null)
				return null;

			if (source == null)
				source = target;

			ModeStacker stacker = parseModes(target, ums.getChanges());

			String changes = stacker.getAll();
			if (target.isLocal() && changes != null)
			{
				Message m = new UserModeMessage(source, target, changes);
				((LocalUser) target).writeMessage(m);
			}

			return null;
		}
		finally
		{
			commit.release();
		}
	}

	private ModeStacker parseModes(User user, String modes)
	{
		ModeStacker stack = new ModeStacker();
		boolean add = true;

		for (int i = 0; i < modes.length(); ++i)
		{
			char c = modes.charAt(i);

			if (c == '+')
			{
				add = true;
				continue;
			}

			if (c == '-')
			{
				add = false;
				continue;
			}

			UserMode um = getPlexus().getModeManager().findUserMode(c);
			if (um == null)
				continue;
			
			if (user.hasMode(um) == add)
				continue;

			um.change(user, add);

			if (add)
				stack.set(c);
			else
				stack.unset(c);
		}

		return stack;
	}
}
