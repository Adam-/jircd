/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication;

import io.atomix.copycat.server.Commit;
import io.atomix.copycat.server.Snapshottable;
import io.atomix.copycat.server.StateMachine;
import io.atomix.copycat.server.StateMachineExecutor;
import io.atomix.copycat.server.session.ServerSession;
import io.atomix.copycat.server.session.SessionListener;
import io.atomix.copycat.server.storage.snapshot.SnapshotReader;
import io.atomix.copycat.server.storage.snapshot.SnapshotWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import net.rizon.plexus.Client;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.Server;
import net.rizon.plexus.replication.commands.CaptureUser;
import net.rizon.plexus.replication.commands.ChangeHost;
import net.rizon.plexus.replication.commands.ChangeIdent;
import net.rizon.plexus.replication.commands.ChannelModeSet;
import net.rizon.plexus.replication.commands.IntroduceServer;
import net.rizon.plexus.replication.commands.IntroduceUser;
import net.rizon.plexus.replication.commands.InviteUser;
import net.rizon.plexus.replication.commands.JoinUser;
import net.rizon.plexus.replication.commands.KickUser;
import net.rizon.plexus.replication.commands.KillUser;
import net.rizon.plexus.replication.commands.KnockChannel;
import net.rizon.plexus.replication.commands.MessageChannel;
import net.rizon.plexus.replication.commands.MessageUser;
import net.rizon.plexus.replication.commands.NickChange;
import net.rizon.plexus.replication.commands.OperNotice;
import net.rizon.plexus.replication.commands.PartUser;
import net.rizon.plexus.replication.commands.QuitServer;
import net.rizon.plexus.replication.commands.QuitUser;
import net.rizon.plexus.replication.commands.TopicChange;
import net.rizon.plexus.replication.commands.UserAccept;
import net.rizon.plexus.replication.commands.UserAway;
import net.rizon.plexus.replication.commands.UserModeSet;
import net.rizon.plexus.replication.commands.XLineAdd;
import net.rizon.plexus.replication.commands.XLineRemove;
import net.rizon.plexus.replication.handlers.CaptureUserHandler;
import net.rizon.plexus.replication.handlers.ChangeHostHandler;
import net.rizon.plexus.replication.handlers.ChangeIdentHandler;
import net.rizon.plexus.replication.handlers.ChannelModeSetHandler;
import net.rizon.plexus.replication.handlers.IntroduceServerHandler;
import net.rizon.plexus.replication.handlers.IntroduceUserHandler;
import net.rizon.plexus.replication.handlers.InviteUserHandler;
import net.rizon.plexus.replication.handlers.JoinUserHandler;
import net.rizon.plexus.replication.handlers.KickUserHandler;
import net.rizon.plexus.replication.handlers.KillUserHandler;
import net.rizon.plexus.replication.handlers.KnockChannelHandler;
import net.rizon.plexus.replication.handlers.MessageChannelHandler;
import net.rizon.plexus.replication.handlers.MessageUserHandler;
import net.rizon.plexus.replication.handlers.NickChangeHandler;
import net.rizon.plexus.replication.handlers.OperNoticeHandler;
import net.rizon.plexus.replication.handlers.PartUserHandler;
import net.rizon.plexus.replication.handlers.QuitServerHandler;
import net.rizon.plexus.replication.handlers.QuitUserHandler;
import net.rizon.plexus.replication.handlers.TopicChangeHandler;
import net.rizon.plexus.replication.handlers.UserAcceptHandler;
import net.rizon.plexus.replication.handlers.UserAwayHandler;
import net.rizon.plexus.replication.handlers.UserModeSetHandler;
import net.rizon.plexus.replication.handlers.XLineAddHandler;
import net.rizon.plexus.replication.handlers.XLineRemoveHandler;
import net.rizon.plexus.replication.snapshot.Snapshot;
import net.rizon.plexus.replication.snapshot.SnapshotApplier;
import net.rizon.plexus.replication.snapshot.SnapshotBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlexusMachine extends StateMachine implements SessionListener, Snapshottable
{
	private static final Logger logger = LoggerFactory.getLogger(PlexusMachine.class);

	private final Plexus plexus;
	private final Map<Class<? extends PlexusCommand>, CommitHandler<? extends PlexusCommand>> handlers = new HashMap<>();

	public PlexusMachine(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	@Override
	protected void configure(StateMachineExecutor executor)
	{
		// register plexus copycat command handler
		executor.<PlexusCopycatCommand, Object>register(PlexusCopycatCommand.class, this::handleCopycatCommand);

		// register command handlers
		register(CaptureUser.class, new CaptureUserHandler(this));
		register(ChangeHost.class, new ChangeHostHandler(this));
		register(ChangeIdent.class, new ChangeIdentHandler(this));
		register(ChannelModeSet.class, new ChannelModeSetHandler(this));
		register(IntroduceServer.class, new IntroduceServerHandler(this));
		register(IntroduceUser.class, new IntroduceUserHandler(this));
		register(InviteUser.class, new InviteUserHandler(this));
		register(JoinUser.class, new JoinUserHandler(this));
		register(KickUser.class, new KickUserHandler(this));
		register(KillUser.class, new KillUserHandler(this));
		register(KnockChannel.class, new KnockChannelHandler(this));
		register(MessageChannel.class, new MessageChannelHandler(this));
		register(MessageUser.class, new MessageUserHandler(this));
		register(NickChange.class, new NickChangeHandler(this));
		register(OperNotice.class, new OperNoticeHandler(this));
		register(PartUser.class, new PartUserHandler(this));
		register(QuitServer.class, new QuitServerHandler(this));
		register(QuitUser.class, new QuitUserHandler(this));
		register(TopicChange.class, new TopicChangeHandler(this));
		register(UserAccept.class, new UserAcceptHandler(this));
		register(UserAway.class, new UserAwayHandler(this));
		register(UserModeSet.class, new UserModeSetHandler(this));
		register(XLineAdd.class, new XLineAddHandler(this));
		register(XLineRemove.class, new XLineRemoveHandler(this));
	}

	private <T extends PlexusCommand> void register(Class<T> message, CommitHandler<T> handler)
	{
		logger.debug("Registering handler {} for commit {}", handler, message);

		handlers.put(message, handler);
	}

	private Object handleCopycatCommand(Commit<PlexusCopycatCommand> commit)
	{
		try
		{
			PlexusCopycatCommand command = commit.command();
			PlexusCommand plexusCommand = command.getCommand();

			logger.debug("Applying {}", command);

			CommitHandler<? extends PlexusCommand> handler = handlers.get(plexusCommand.getClass());

			if (handler == null)
			{
				logger.warn("No handler for {}", plexusCommand);
				commit.release();
				return null;
			}

			synchronized (plexus.getIrc())
			{
				// the handler is responsible for releasing the commit
				Object object = handler.apply((Commit) commit);

				// notify waiting handler
				PlexusCopycatCommand pcc = plexus.getServer().removeCommand(command.getUid());
				if (pcc == null)
				{
					// not my command
					return object;
				}

				// Free client of wait
				Client client = pcc.getClient();
				if (client != null)
				{
					// client may have exited, don't notify consumer
					if (client.isExited())
					{
						logger.debug("Command {} has been applied, but client {} exited", pcc, client);
						return object;
					}

					client.setWaitingForCommand(false);
				}

				BiConsumer<Object, ? super Throwable> consumer = pcc.getConsumer();
				if (consumer != null)
				{
					logger.debug("Notifying consumer of replicated command {}", plexusCommand);

					try
					{
						pcc.getConsumer().accept(object, null);
					}
					catch (Throwable ex)
					{
						logger.error("Error in callback", ex);
						throw ex;
					}
				}

				return object;
			}
		}
		catch (Throwable ex)
		{
			logger.warn("Error handling commit", ex);
			throw ex;
		}
	}

	public IRC getIrc()
	{
		return plexus.getIrc();
	}

	@Override
	public void register(ServerSession session)
	{
		logger.info("Registering server session {}", session);
	}

	@Override
	public void unregister(ServerSession session)
	{
		logger.info("Unregistering server session {}", session);
	}

	@Override
	public void expire(ServerSession session)
	{
		logger.info("Expiring server session {}", session);
	}

	private Server findServerFromSession(ServerSession session)
	{
		for (Server s : plexus.getIrc().getServers().values())
		{
			if (s.getSession() != null && s.getSession().equals(session))
			{
				return s;
			}
		}
		return null;
	}

	@Override
	public void close(ServerSession session)
	{
		synchronized (getIrc())
		{
			Server server = findServerFromSession(session);

			if (server == null)
			{
				logger.warn("Closing session with no associated server {}", session);
				return;
			}

			// i dont think all servers necessarily have the sessions associated with servers,
			// like after burst? so broadcast squit event?

			QuitServer qs = new QuitServer();
			qs.setUuid(server.getId());
			qs.setReason("*.net *.split");

			getPlexus().getClient().submit(qs);
		}
	}

	@Override
	public void snapshot(SnapshotWriter writer)
	{
		logger.info("Taking snapshot");
		
		SnapshotBuilder builder = new SnapshotBuilder(getIrc());
		
		synchronized (getIrc())
		{
			builder.build();
		}
		
		Snapshot snapshot = builder.getSnapshot();
		
		writer.writeObject(snapshot);
	}

	@Override
	public void install(SnapshotReader reader)
	{
		logger.info("Installing snapshot");
		
		Snapshot snapshot = reader.readObject();
		
		synchronized (getIrc())
		{
			// exit all users
			for (Client client : plexus.getClientManager().getClients())
			{
				client.error("Snapshot install in progress");
			}

			getIrc().clear();
			
			SnapshotApplier sa = new SnapshotApplier(snapshot, plexus);
			sa.apply();
		}
		
		logger.info("Snapshot installation complete");
	}
}
