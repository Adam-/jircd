/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.serializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import net.rizon.plexus.replication.PlexusCommand;
import net.rizon.plexus.replication.commands.CaptureUser;
import net.rizon.plexus.replication.commands.ChangeHost;
import net.rizon.plexus.replication.commands.ChangeIdent;
import net.rizon.plexus.replication.commands.ChannelModeSet;
import net.rizon.plexus.replication.commands.IntroduceServer;
import net.rizon.plexus.replication.commands.IntroduceUser;
import net.rizon.plexus.replication.commands.InviteUser;
import net.rizon.plexus.replication.commands.JoinUser;
import net.rizon.plexus.replication.commands.KickUser;
import net.rizon.plexus.replication.commands.KillUser;
import net.rizon.plexus.replication.commands.KnockChannel;
import net.rizon.plexus.replication.commands.MessageChannel;
import net.rizon.plexus.replication.commands.MessageUser;
import net.rizon.plexus.replication.commands.NickChange;
import net.rizon.plexus.replication.commands.OperNotice;
import net.rizon.plexus.replication.commands.PartUser;
import net.rizon.plexus.replication.commands.QuitServer;
import net.rizon.plexus.replication.commands.QuitUser;
import net.rizon.plexus.replication.commands.TopicChange;
import net.rizon.plexus.replication.commands.UserAccept;
import net.rizon.plexus.replication.commands.UserAway;
import net.rizon.plexus.replication.commands.UserModeSet;
import net.rizon.plexus.replication.commands.XLineAdd;
import net.rizon.plexus.replication.commands.XLineRemove;

public class GsonFactory
{
	private static final GsonBuilder gsonBuilder = new GsonBuilder()
		.registerTypeAdapterFactory(RuntimeTypeAdapterFactory.of(PlexusCommand.class)
			.registerSubtype(CaptureUser.class)
			.registerSubtype(ChangeHost.class)
			.registerSubtype(ChangeIdent.class)
			.registerSubtype(ChannelModeSet.class)
			.registerSubtype(IntroduceServer.class)
			.registerSubtype(IntroduceUser.class)
			.registerSubtype(InviteUser.class)
			.registerSubtype(JoinUser.class)
			.registerSubtype(KickUser.class)
			.registerSubtype(KillUser.class)
			.registerSubtype(KnockChannel.class)
			.registerSubtype(MessageChannel.class)
			.registerSubtype(MessageUser.class)
			.registerSubtype(NickChange.class)
			.registerSubtype(OperNotice.class)
			.registerSubtype(PartUser.class)
			.registerSubtype(QuitServer.class)
			.registerSubtype(QuitUser.class)
			.registerSubtype(TopicChange.class)
			.registerSubtype(UserAccept.class)
			.registerSubtype(UserAway.class)
			.registerSubtype(UserModeSet.class)
			.registerSubtype(XLineAdd.class)
			.registerSubtype(XLineRemove.class)
		);

	public static Gson create()
	{
		return gsonBuilder.create();
	}
}
