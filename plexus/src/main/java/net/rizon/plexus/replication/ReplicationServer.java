/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication;

import io.atomix.catalyst.transport.Address;
import io.atomix.catalyst.transport.netty.NettyTransport;
import io.atomix.copycat.server.CopycatServer;
import io.atomix.copycat.server.storage.Storage;
import io.atomix.copycat.server.storage.StorageLevel;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.replication.serializer.GsonTypeSerializerFactory;
import net.rizon.plexus.replication.snapshot.Snapshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplicationServer
{
	private static Logger logger = LoggerFactory.getLogger(ReplicationServer.class);

	private final Plexus plexus;
	private CopycatServer server;
	
	private final Map<UUID, PlexusCopycatCommand> pendingCommands = new HashMap<>();

	public ReplicationServer(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public void start()
	{
		Address serverAddress = new Address(plexus.getConf().getCluster().getAddress());

		CopycatServer.Builder builder = CopycatServer.builder(serverAddress)
			.withStateMachine(() -> new PlexusMachine(plexus))
			.withTransport(
				NettyTransport.builder()
				.build()
			)
			.withStorage(
				Storage.builder()
				.withStorageLevel(StorageLevel.MEMORY)
				.build()
			)
			.withElectionTimeout(Duration.ofMillis(plexus.getConf().getCluster().getElectionTimeout()))
			.withHeartbeatInterval(Duration.ofMillis(plexus.getConf().getCluster().getHeartbeatInterval()))
			.withSessionTimeout(Duration.ofMillis(plexus.getConf().getCluster().getSessionTimeout()));

		server = builder.build();

		server.serializer().register(PlexusCopycatCommand.class, new GsonTypeSerializerFactory());
		server.serializer().register(Snapshot.class, new GsonTypeSerializerFactory());

		CompletableFuture<CopycatServer> future;
		List<Address> addresses = plexus.getConf().getCluster().getAddresses().stream()
			.map(a -> new Address(a))
			.collect(Collectors.toList());

		if (plexus.getConf().getCluster().isBootstrap())
		{
			logger.info("Bootstraping new cluster: {}", addresses);

			future = server.bootstrap(addresses);
		}
		else
		{
			logger.info("Joining cluster: {}", addresses);

			future = server.join(addresses);
		}

		future.join();

		logger.info("Finished!");
	}
	
	public void stop()
	{
		server.leave().join();
	}
	
	public synchronized void addCommand(PlexusCopycatCommand pcc)
	{
		pendingCommands.put(pcc.getUid(), pcc);
	}
	
	public synchronized PlexusCopycatCommand removeCommand(UUID uuid)
	{
		return pendingCommands.remove(uuid);
	}
}
