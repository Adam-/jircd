/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.snapshot;

import java.util.List;

public class ChannelSnapshot
{
	private String name;
	private long ts;

	private String modes;
	private String key;
	private int limit;
	private List<ChannelEntrySnapshot> bans, invex, excepts;
	
	private String topic;
	private String topicSetter;
	private long topicTime;

	@Override
	public String toString()
	{
		return "ChannelSnapshot{" + "name=" + name + ", ts=" + ts + ", key=" + key + ", limit=" + limit + ", bans=" + bans + ", invex=" + invex + ", excepts=" + excepts + ", topic=" + topic + ", topicSetter=" + topicSetter + ", topicTime=" + topicTime + '}';
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public long getTs()
	{
		return ts;
	}

	public void setTs(long ts)
	{
		this.ts = ts;
	}

	public String getModes()
	{
		return modes;
	}

	public void setModes(String modes)
	{
		this.modes = modes;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public int getLimit()
	{
		return limit;
	}

	public void setLimit(int limit)
	{
		this.limit = limit;
	}

	public List<ChannelEntrySnapshot> getBans()
	{
		return bans;
	}

	public void setBans(List<ChannelEntrySnapshot> bans)
	{
		this.bans = bans;
	}

	public List<ChannelEntrySnapshot> getInvex()
	{
		return invex;
	}

	public void setInvex(List<ChannelEntrySnapshot> invex)
	{
		this.invex = invex;
	}

	public List<ChannelEntrySnapshot> getExcepts()
	{
		return excepts;
	}

	public void setExcepts(List<ChannelEntrySnapshot> excepts)
	{
		this.excepts = excepts;
	}

	public String getTopic()
	{
		return topic;
	}

	public void setTopic(String topic)
	{
		this.topic = topic;
	}

	public String getTopicSetter()
	{
		return topicSetter;
	}

	public void setTopicSetter(String topicSetter)
	{
		this.topicSetter = topicSetter;
	}

	public long getTopicTime()
	{
		return topicTime;
	}

	public void setTopicTime(long topicTime)
	{
		this.topicTime = topicTime;
	}
}
