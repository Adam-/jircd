/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.snapshot;

import java.util.stream.Collectors;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Server;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Entry;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.users.User;
import net.rizon.plexus.util.Util;

public class SnapshotBuilder
{
	private final IRC irc;
	private final Snapshot snapshot = new Snapshot();

	public SnapshotBuilder(IRC irc)
	{
		this.irc = irc;
	}

	public Snapshot getSnapshot()
	{
		return snapshot;
	}

	public void build()
	{
		for (Server s : irc.getServers().values())
		{
			addServer(s);
		}

		for (User u : irc.getUsers().values())
		{
			addUser(u);
		}

		for (Channel c : irc.getChannels().values())
		{
			addChannel(c);

			for (Membership membership : c.getUsers())
			{
				addMembership(membership);
			}
		}
	}

	private void addServer(Server server)
	{
		ServerSnapshot ss = new ServerSnapshot();

		ss.setId(server.getId());
		ss.setName(server.getName());
		ss.setDescription(server.getDescription());

		snapshot.getServers().add(ss);
	}

	private void addUser(User user)
	{
		UserSnapshot us = new UserSnapshot();

		us.setNickname(user.getName());
		us.setSignon(Util.currentTime());
		us.setModes(user.getModes());
		us.setUsername(user.getUsername());
		us.setHostname(user.getHost());
		us.setIp(user.getIp());
		us.setCloakedHost(user.getChost());
		us.setCloakedIp(user.getCip());
		us.setVhost(user.getVhost());
		us.setId(user.getId());
		us.setRealhost(user.getHost());
		us.setGecos(user.getGecos());
		us.setServerId(user.getServer().getId());

		snapshot.getUsers().add(us);
	}

	private void addChannel(Channel channel)
	{
		ChannelSnapshot cs = new ChannelSnapshot();

		cs.setName(channel.getName());
		cs.setTs(channel.getTs());

		cs.setModes(channel.getModeString(false));
		cs.setKey(channel.getKey());
		cs.setLimit(channel.getLimit());
		cs.setBans(channel.getBans().stream().map(this::convertEntry).collect(Collectors.toList()));
		cs.setExcepts(channel.getExcepts().stream().map(this::convertEntry).collect(Collectors.toList()));
		cs.setInvex(channel.getInvex().stream().map(this::convertEntry).collect(Collectors.toList()));

		cs.setTopic(channel.getTopic());
		cs.setTopicSetter(channel.getTopicSetter());
		cs.setTopicTime(channel.getTopicTime());

		snapshot.getChannels().add(cs);
	}

	private void addMembership(Membership membership)
	{
		MembershipSnapshot ms = new MembershipSnapshot();

		ms.setChannel(membership.getChannel().getName());
		ms.setUser(membership.getUser().getId());

		snapshot.getMemberships().add(ms);
	}

	private ChannelEntrySnapshot convertEntry(Entry entry)
	{
		ChannelEntrySnapshot ces = new ChannelEntrySnapshot();
		ces.setMask(entry.getMask());
		ces.setBy(entry.getWho());
		ces.setCreated(entry.getWhen().toInstant());
		return ces;
	}
}
