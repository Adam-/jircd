/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import java.util.UUID;
import net.rizon.plexus.IRC;
import net.rizon.plexus.IRCStats;
import net.rizon.plexus.Server;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.IntroduceUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntroduceUserHandler extends CommitHandler<IntroduceUser>
{
	private static final Logger logger = LoggerFactory.getLogger(IntroduceUserHandler.class);

	public IntroduceUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public UUID apply(Commit<PlexusCopycatCommand<IntroduceUser>> commit)
	{
		try
		{
			IntroduceUser iu = commit.command().getCommand();

			Server server = getIrc().findServer(iu.getServerId());
			if (server == null)
			{
				logger.error("User {} introduced from unknown server", iu);
				return null;
			}

			User user = getIrc().findUser(iu.getId());
			if (user != null)
			{
				logger.warn("ID collision between {} and {}", user, iu);

				// XXX this will broadcast the kill on each server, maybe can just assume killed
				user.kill(getPlexus().getMe(), "ID collision");

				// assume new user isn't introduced?
				return null;
			}

			user = getIrc().findUser(iu.getNickname());
			if (user != null)
			{
				logger.debug("Nick introduction collision with {}", user);

				// allow the old one to win
				return null;
			}

			if (server == getPlexus().getMe())
			{
				user = new LocalUser(getPlexus());
			}
			else
			{
				user = new User(getPlexus());
			}

			user.setName(iu.getNickname());
			user.setUsername(iu.getUsername());
			user.setIp(iu.getIp());
			user.setHost(iu.getHostname());
			user.setChost(iu.getCloakedHost());
			user.setCip(iu.getCloakedIp());
			user.setVhost(iu.getVhost());
			user.setGecos(iu.getGecos());
			user.setId(iu.getId());
			user.setServer(server);

			getIrc().addUser(user);

			parseModes(user, iu.getModes());

			logger.debug("{} logs online from {}", user, server);

			checkMaxTotal();

			return user.getId();
		}
		finally
		{
			commit.release();
		}
	}

	private void parseModes(User user, String modes)
	{
		ModeStacker stack = new ModeStacker();

		for (int i = 0; i < modes.length(); ++i)
		{
			char c = modes.charAt(i);

			UserMode um = getPlexus().getModeManager().findUserMode(c);
			if (um == null)
			{
				continue;
			}

			um.change(user, true);
			stack.set(c);
		}

		if (stack.empty() == false)
		{
			user.setModes(stack.getAll());
		}
	}

	private void checkMaxTotal()
	{
		IRC irc = getIrc();
		IRCStats stats = getPlexus().getStats();

		int totalUsers = irc.getUsers().size();
		int maxTotal = stats.getMaxTotal();

		if (totalUsers > maxTotal)
		{
			stats.setMaxTotal(totalUsers);
		}
	}
}
