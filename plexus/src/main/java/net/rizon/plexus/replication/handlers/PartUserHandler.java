/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.Message;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.messages.PartMessage;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.PartUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PartUserHandler extends CommitHandler<PartUser>
{
	private static final Logger logger = LoggerFactory.getLogger(PartUserHandler.class);

	public PartUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<PartUser>> commit)
	{
		try
		{
			PartUser pu = commit.command().getCommand();

			User user = getIrc().findUser(pu.getUser());

			if (user == null)
			{
				logger.debug("Part for unknown user {}", pu.getUser());
				return null;
			}

			Channel channel = getIrc().findChannel(pu.getChannel());
			if (channel == null)
			{
				logger.debug("Part for unknown channel {} for user {}", pu.getChannel(), user);
				return null;
			}

			if (user.findChannel(channel) == null)
			{
				logger.debug("Part with no membership for {} in {}", user, channel);
				return null;
			}

			logger.debug("User {} parts {}", user, channel);

			sendPart(user, channel, pu.getReason());

			channel.removeUser(user);
			user.removeChannel(channel);

			if (channel.getUsers().isEmpty())
			{
				logger.debug("Channel {} gets destroyed", channel);

				channel.destroy();
				getIrc().removeChannel(channel);
			}

			return null;
		}
		finally
		{
			commit.release();
		}
	}

	private void sendPart(User user, Channel channel, String reason)
	{
		Message m = new PartMessage(user, channel, reason);
		channel.send(null, m);
	}

}
