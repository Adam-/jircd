/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.commands;

import java.util.UUID;
import net.rizon.plexus.replication.PlexusCommand;
import net.rizon.plexus.xline.XLineType;

public class XLineAdd implements PlexusCommand
{
	private UUID source;
	private XLineType xlineType;
	private String mask;
	private String creator;
	private String reason;
	private String oreason;
	private long created;
	private long expires;

	@Override
	public String toString()
	{
		return "XLineAdd{" + "source=" + source + ", xlineType=" + xlineType + ", mask=" + mask + ", creator=" + creator + ", reason=" + reason + ", oreason=" + oreason + ", created=" + created + ", expires=" + expires + '}';
	}

	public UUID getSource()
	{
		return source;
	}

	public void setSource(UUID source)
	{
		this.source = source;
	}

	public XLineType getXlineType()
	{
		return xlineType;
	}

	public void setXlineType(XLineType xlineType)
	{
		this.xlineType = xlineType;
	}

	public String getMask()
	{
		return mask;
	}

	public void setMask(String mask)
	{
		this.mask = mask;
	}

	public String getCreator()
	{
		return creator;
	}

	public void setCreator(String creator)
	{
		this.creator = creator;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public String getOreason()
	{
		return oreason;
	}

	public void setOreason(String oreason)
	{
		this.oreason = oreason;
	}

	public long getCreated()
	{
		return created;
	}

	public void setCreated(long created)
	{
		this.created = created;
	}

	public long getExpires()
	{
		return expires;
	}

	public void setExpires(long expires)
	{
		this.expires = expires;
	}
}
