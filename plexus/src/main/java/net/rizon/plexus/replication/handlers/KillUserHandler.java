/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.Entity;
import net.rizon.plexus.messages.KillMessage;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.KillUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KillUserHandler extends CommitHandler<KillUser>
{
	private static final Logger logger = LoggerFactory.getLogger(KillUserHandler.class);

	public KillUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<KillUser>> commit)
	{
		KillUser ku = commit.command().getCommand();
		try
		{
			User user = getIrc().findUser(ku.getUser());
			if (user == null)
			{
				logger.debug("Kill for unknown user {}", user);
				return null;
			}
			
			Entity source = getIrc().findEntity(ku.getSource());
			if (source == null)
			{
				logger.debug("Kill from unknown source {}", ku.getSource());
				source = getPlexus().getMe();
			}
			
			logger.debug("User {} killed by {} with reason {}", user, source, ku.getSource());

			getPlexus().getClientManager().sendToOps(null, "Received KILL message for %s. From %s (%s)", user.getName(), source.getName(), ku.getReason());
			
			// kills show up to users as quits
			user.quit(ku.getReason());
			
			if (user.isLocal())
			{
				LocalUser localUser = (LocalUser) user;
				localUser.writeMessage(new KillMessage(source, user.getName(), ku.getReason()));
				localUser.getClient().error("Killed (" + source.getName() + " (" + ku.getReason() + "))");
			}
			
			getIrc().removeUser(user);
			
			return null;
		}
		finally
		{
			commit.release();
		}
	}

}
