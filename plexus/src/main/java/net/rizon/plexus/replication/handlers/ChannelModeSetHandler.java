/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.Entity;
import net.rizon.plexus.IRC;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.messages.ChannelModeMessage;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.StackerLine;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.ChannelModeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChannelModeSetHandler extends CommitHandler<ChannelModeSet>
{
	private static final Logger logger = LoggerFactory.getLogger(ChannelModeSetHandler.class);

	public ChannelModeSetHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<ChannelModeSet>> commit)
	{
		try
		{
			ChannelModeSet cms = commit.command().getCommand();

			Entity source = getIrc().findEntity(cms.getSource());
			Channel channel = getIrc().findChannel(cms.getTarget());

			if (channel == null || channel.getTs() != cms.getTargetTs())
			{
				return null;
			}

			if (source == null)
			{
				source = getPlexus().getMe();
			}

			ModeStacker stacker = parseModes(source, channel, cms.getChanges(), cms.getParameters());
			
			StackerLine line = stacker.getLine(IRC.MAXMODEPARAMS);
			while (line != null)
			{
				channel.send(null, new ChannelModeMessage(source, channel, line.getModes(), line.getParams()));
				
				line = stacker.getLine(IRC.MAXMODEPARAMS);
			}

			return null;
		}
		finally
		{
			commit.release();
		}
	}

	private ModeStacker parseModes(Entity source, Channel channel, String modes, String[] params)
	{
		ModeStacker stack = new ModeStacker();
		boolean add = true;
		int paramIndex = 0;

		for (int i = 0; i < modes.length(); ++i)
		{
			char c = modes.charAt(i);

			if (c == '+')
			{
				add = true;
				continue;
			}

			if (c == '-')
			{
				add = false;
				continue;
			}

			ChannelMode cm = getPlexus().getModeManager().findChannelMode(c);
			if (cm == null)
			{
				continue;
			}

			boolean requiresParameter = cm.isParameter() || cm.isList() || cm.getPrefix() > 0;
			if (requiresParameter && !add && cm.isMinusNoArg())
			{
				requiresParameter = false;
			}

			String param = null;
			if (requiresParameter)
			{
				if (params == null || paramIndex >= params.length)
				{
					logger.warn("More modes with params than params: {} {}", modes, params);
					continue;
				}

				param = params[paramIndex++];
			}
			
			if (cm.hasMode(channel, param) == add)
			{
				continue;
			}

			if (!cm.set(source, channel, null, add, param))
			{
				continue;
			}

			if (add)
			{
				stack.set(c, param);
			}
			else
			{
				stack.unset(c);
			}
		}

		return stack;
	}
}
