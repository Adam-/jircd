/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.Entity;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.XLineAdd;
import net.rizon.plexus.xline.XLine;
import net.rizon.plexus.xline.XLineFactory;
import net.rizon.plexus.xline.XLineManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XLineAddHandler extends CommitHandler<XLineAdd>
{
	private static final Logger logger = LoggerFactory.getLogger(XLineAddHandler.class);

	public XLineAddHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<XLineAdd>> commit)
	{
		try
		{
			XLineAdd xa = commit.command().getCommand();
			XLineManager xlineManager = getPlexus().getXlineManager();
			
			XLine xl = XLineFactory.getXLine(xlineManager, xa.getXlineType());
			if (xl == null)
				return null;

			xl.setMask(xa.getMask());
			xl.setCreator(xa.getCreator());
			xl.setReason(xa.getReason());
			xl.setOreason(xa.getOreason());
			xl.setCreated(xa.getCreated());
			xl.setExpires(xa.getExpires());

			if (xlineManager.findXLine(xl) != null)
				return null;

			Entity source = getIrc().findEntity(xa.getSource());

			if (source != null)
			{
				if (xl.getExpires() != 0)
				{
					getPlexus().getClientManager().sendToOps(null, "%s added temporary %d min. %s for [%s] [%s]", source.getName(), (xl.getExpires() - xl.getCreated()) / 60, xl.getType(), xl.getMask(), xl.getReason());
				}
				else
				{
					getPlexus().getClientManager().sendToOps(null, "%s added %s for [%s] [%s]", source.getName(), xl.getType(), xl.getMask(), xl.getReason());
				}
			}

			xlineManager.addXline(xl);
			xlineManager.apply(xl);

			logger.debug("Added xline {}", xl);

			return null;
		}
		finally
		{
			commit.release();
		}
	}
}
