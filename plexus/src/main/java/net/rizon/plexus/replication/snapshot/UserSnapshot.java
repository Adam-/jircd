/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.snapshot;

import java.util.UUID;

public class UserSnapshot
{
	private UUID id;
	private String nickname;
	private long signon;
	private String modes;
	private String username;
	private String hostname;
	private String ip;
	private String cloakedHost;
	private String cloakedIp;
	private String vhost;
	private String account;
	private String realhost;
	private String gecos;
	private UUID serverId;

	@Override
	public String toString()
	{
		return "UserSnapshot{" + "id=" + id + ", nickname=" + nickname + ", signon=" + signon + ", modes=" + modes + ", username=" + username + ", hostname=" + hostname + ", ip=" + ip + ", cloakedHost=" + cloakedHost + ", cloakedIp=" + cloakedIp + ", vhost=" + vhost + ", account=" + account + ", realhost=" + realhost + ", gecos=" + gecos + ", serverId=" + serverId + '}';
	}

	public UUID getId()
	{
		return id;
	}

	public void setId(UUID id)
	{
		this.id = id;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public long getSignon()
	{
		return signon;
	}

	public void setSignon(long signon)
	{
		this.signon = signon;
	}

	public String getModes()
	{
		return modes;
	}

	public void setModes(String modes)
	{
		this.modes = modes;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getHostname()
	{
		return hostname;
	}

	public void setHostname(String hostname)
	{
		this.hostname = hostname;
	}

	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public String getCloakedHost()
	{
		return cloakedHost;
	}

	public void setCloakedHost(String cloakedHost)
	{
		this.cloakedHost = cloakedHost;
	}

	public String getCloakedIp()
	{
		return cloakedIp;
	}

	public void setCloakedIp(String cloakedIp)
	{
		this.cloakedIp = cloakedIp;
	}

	public String getVhost()
	{
		return vhost;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}

	public String getAccount()
	{
		return account;
	}

	public void setAccount(String account)
	{
		this.account = account;
	}

	public String getRealhost()
	{
		return realhost;
	}

	public void setRealhost(String realhost)
	{
		this.realhost = realhost;
	}

	public String getGecos()
	{
		return gecos;
	}

	public void setGecos(String gecos)
	{
		this.gecos = gecos;
	}

	public UUID getServerId()
	{
		return serverId;
	}

	public void setServerId(UUID serverId)
	{
		this.serverId = serverId;
	}
}
