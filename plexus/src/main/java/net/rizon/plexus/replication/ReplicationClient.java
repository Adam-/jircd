/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication;

import io.atomix.catalyst.transport.Address;
import io.atomix.catalyst.transport.netty.NettyTransport;
import io.atomix.copycat.client.ConnectionStrategies;
import io.atomix.copycat.client.CopycatClient;
import io.atomix.copycat.client.RecoveryStrategies;
import io.atomix.copycat.client.ServerSelectionStrategies;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import net.rizon.plexus.Client;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.replication.commands.IntroduceServer;
import net.rizon.plexus.replication.serializer.GsonTypeSerializerFactory;
import net.rizon.plexus.replication.snapshot.Snapshot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReplicationClient
{
	private static final Logger logger = LoggerFactory.getLogger(ReplicationClient.class);

	private final Plexus plexus;
	private CopycatClient client;

	public ReplicationClient(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	public CopycatClient start()
	{
		CopycatClient.Builder builder = CopycatClient.builder()
			.withTransport(
				NettyTransport.builder()
				.build()
			)
			.withRecoveryStrategy(RecoveryStrategies.RECOVER)
			.withServerSelectionStrategy(ServerSelectionStrategies.LEADER)
			.withConnectionStrategy(ConnectionStrategies.FIBONACCI_BACKOFF);

		client = builder.build();

		client.serializer().register(PlexusCopycatCommand.class, new GsonTypeSerializerFactory());
		client.serializer().register(Snapshot.class, new GsonTypeSerializerFactory());

		client.onStateChange(this::onStateChange);

		CompletableFuture<CopycatClient> future = client.connect(new Address(plexus.getConf().getCluster().getAddress()));

		logger.info("Connecting to cluster...");

		future.join();

		logger.info("Connected!");

		return client;
	}
	
	public void stop()
	{
		client.close();
	}

	private void onStateChange(CopycatClient.State state)
	{
		logger.info("Client state change to {}", state);

		if (state == CopycatClient.State.CLOSED)
		{
			onClose();
			return;
		}
		
		if (state != CopycatClient.State.CONNECTED)
		{
			return;
		}
		
		// introduce myself
		IntroduceServer is = new IntroduceServer();
		is.setUuid(plexus.getMe().getId());
		is.setName(plexus.getMe().getName());
		is.setDescription(plexus.getMe().getDescription());
		
		submit(is);
		
		logger.debug("Introduced self to cluster");
	}
	
	public <T> void submit(PlexusCommand operation)
	{
		submit(null, operation, null);
	}

	public <T> void submit(Client client, PlexusCommand operation)
	{
		submit(client, operation, null);
	}

	public <T> void submit(Client localClient, PlexusCommand operation, BiConsumer<T, ? super Throwable> consumer)
	{
		if (client.state() != CopycatClient.State.CONNECTED)
		{
			if (consumer != null)
			{
				consumer.accept(null, new NotConnectedException());
			}
			return;
		}

		if (localClient != null)
		{
			if (localClient.isWaitingForCommand())
			{
				logger.warn("Client {} is already waiting, but submitted operation {}", localClient, operation);
			}

			localClient.setWaitingForCommand(true);
		}
		
		PlexusCopycatCommand pcc = new PlexusCopycatCommand();
		pcc.setUid(UUID.randomUUID());
		pcc.setCommand(operation);
		pcc.setConsumer(consumer);
		pcc.setClient(localClient);
		
		logger.debug("Submitting command to cluster: {}", pcc);
		
		plexus.getServer().addCommand(pcc);
		
		CompletableFuture<T> future = client.submit(pcc);

		future.whenComplete((res, ex) -> notifyComplete(pcc, res, ex));
	}

	private <T> void notifyComplete(PlexusCopycatCommand pcc, T result, Throwable ex)
	{
		Client localClient = pcc.getClient();
		BiConsumer<T, ? super Throwable> consumer = pcc.getConsumer();
		
		if (localClient != null && localClient.isExited())
		{
			plexus.getServer().removeCommand(pcc.getUid());

			logger.debug("Command {} has completed result: {} ex: {}, but client {} has exited", pcc, result, ex, localClient);
			return;
		}

		logger.debug("Command {} has completed result: {} ex: {}", pcc, result, ex);

		// if there was an exception submitting the command to the cluster, fail the command
		if (ex != null)
		{
			synchronized (plexus.getIrc())
			{
				// command has failed
				if (localClient != null)
				{
					localClient.setWaitingForCommand(false);
				}

				// command won't get ever applied, remove it
				plexus.getServer().removeCommand(pcc.getUid());

				if (consumer != null)
				{
					try
					{
						consumer.accept(null, ex);
					}
					catch (Throwable ex2)
					{
						logger.error("Error in callback when handling client error", ex2);
						logger.error("Client error was:", ex);
					}
				}
			}

			return;
		}

		if (client.state() != CopycatClient.State.CONNECTED)
		{
			/*
			 * Operations submitted to or completed by clients in this state should be considered unsafe.
			 * An operation submitted to a CONNECTED client that transitions to the SUSPENDED state prior to the
			 * operation's completion may be committed multiple times in the event that the underlying session
			 * is ultimately expired, thus breaking linearizability. Additionally, state machines may see the
			 * session expire while the client is in this state.
			 */

			synchronized (plexus.getIrc())
			{
				// command has failed
				if (localClient != null)
				{
					localClient.setWaitingForCommand(false);
				}

				// command won't get ever applied, remove it
				plexus.getServer().removeCommand(pcc.getUid());

				if (consumer != null)
				{
					try
					{
						consumer.accept(null, new NotConnectedException());
					}
					catch (Throwable ex2)
					{
						logger.error("Error in callback when handling not connected exception", ex2);
						throw ex2;
					}
				}
			}
		}

		// command has succeeded, we wait for it to return to our state machine
	}

	private void onClose()
	{
		logger.info("Connection to the cluster has been lost");

		// exit all users
		for (Client client : plexus.getClientManager().getClients())
		{
			client.error("Connection to the cluster has been lost");
		}

		plexus.getIrc().clear();
	}
}
