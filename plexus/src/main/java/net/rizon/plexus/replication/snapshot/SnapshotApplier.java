/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.snapshot;

import java.util.Date;
import java.util.stream.Collectors;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.Server;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Entry;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SnapshotApplier
{
	private static final Logger logger = LoggerFactory.getLogger(SnapshotApplier.class);

	private final Snapshot snapshot;
	private final Plexus plexus;

	public SnapshotApplier(Snapshot snapshot, Plexus plexus)
	{
		this.snapshot = snapshot;
		this.plexus = plexus;
	}

	public void apply()
	{
		logger.info("Applying snapshot with {} servers, {} users, and {} channels",
			snapshot.getServers().size(), snapshot.getUsers().size(),
			snapshot.getChannels().size());

		for (ServerSnapshot ss : snapshot.getServers())
		{
			applyServerSnapshot(ss);
		}

		for (UserSnapshot us : snapshot.getUsers())
		{
			applyUserSnapshot(us);
		}

		for (ChannelSnapshot cs : snapshot.getChannels())
		{
			applyChannelSnapshot(cs);
		}

		for (MembershipSnapshot ms : snapshot.getMemberships())
		{
			applyMembershipSnapshot(ms);
		}
	}

	private void applyServerSnapshot(ServerSnapshot serverSnapshot)
	{
		Server server = plexus.getIrc().findServer(serverSnapshot.getId());
		if (server != null)
		{
			if (server == plexus.getMe())
			{
				return;
			}

			logger.warn("Snapshot reintroduces known server {}", server);
			return;
		}

		server = new Server(plexus);
		server.setId(serverSnapshot.getId());
		server.setName(serverSnapshot.getName());
		server.setDescription(serverSnapshot.getDescription());
		// XXX not sure about session?

		logger.debug("Server introduced in snapshot: {}", server);

		plexus.getIrc().addServer(server);
	}

	private void applyUserSnapshot(UserSnapshot us)
	{
		Server server = plexus.getIrc().findServer(us.getServerId());
		if (server == null)
		{
			logger.warn("User {} introduced from unknown server", us);
			return;
		}

		User user = new User(plexus);

		user.setName(us.getNickname());
		user.setUsername(us.getUsername());
		user.setIp(us.getIp());
		user.setHost(us.getHostname());
		user.setCip(us.getCloakedIp());
		user.setChost(us.getCloakedHost());
		user.setVhost(us.getVhost());
		user.setGecos(us.getGecos());
		user.setId(us.getId());
		user.setServer(server);
		user.setModes(us.getModes());

		logger.debug("User introduced in snapshot: {}", user);

		plexus.getIrc().addUser(user);
	}

	private void applyChannelSnapshot(ChannelSnapshot cs)
	{
		Channel channel = new Channel(plexus);

		channel.setName(cs.getName());
		channel.setTs(cs.getTs());

		channel.setModes(cs.getModes());
		channel.setKey(cs.getKey());
		channel.setLimit(cs.getLimit());
		channel.setBans(cs.getBans().stream().map(this::convertEntry).collect(Collectors.toList()));
		channel.setExcepts(cs.getExcepts().stream().map(this::convertEntry).collect(Collectors.toList()));
		channel.setInvex(cs.getInvex().stream().map(this::convertEntry).collect(Collectors.toList()));

		channel.setTopic(cs.getTopic());
		channel.setTopicSetter(cs.getTopicSetter());
		channel.setTopicTime(cs.getTopicTime());

		logger.debug("Channel introduced in snapshot: {}", channel);

		plexus.getIrc().addChannel(channel);
	}

	private Entry convertEntry(ChannelEntrySnapshot ces)
	{
		Entry entry = new Entry(ces.getMask(), ces.getBy(), new Date(ces.getCreated().toEpochMilli()));
		return entry;
	}

	private void applyMembershipSnapshot(MembershipSnapshot ms)
	{
		User user = plexus.getIrc().findUser(ms.getUser());
		Channel channel = plexus.getIrc().findChannel(ms.getChannel());

		if (user == null)
		{
			logger.debug("Membership in snapshot with nonexistant user: {}", ms);
			return;
		}

		if (channel == null)
		{
			logger.debug("Membership in snapshot with nonexistant channel: {}", ms);
			return;
		}

		Membership membership = new Membership(user, channel, null); // XXX modes

		channel.addUser(user, membership);
		user.addChannel(channel, membership);
	}
}
