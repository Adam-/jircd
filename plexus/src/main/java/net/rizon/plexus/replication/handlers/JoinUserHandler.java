/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.IRC;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.messages.ChannelModeMessage;
import net.rizon.plexus.messages.JoinMessage;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.StackerLine;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.JoinUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JoinUserHandler extends CommitHandler<JoinUser>
{
	private static final Logger logger = LoggerFactory.getLogger(JoinUserHandler.class);

	public JoinUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<JoinUser>> commit)
	{
		JoinUser ju = commit.command().getCommand();
		PlexusMachine machine = getMachine();

		try
		{
			User user = machine.getIrc().findUser(ju.getUser());

			if (user == null)
			{
				logger.debug("Join for unknown user {}", ju.getUser());
				return null;
			}

			Channel channel = machine.getIrc().findChannel(ju.getChannel());
			if (channel == null)
			{
				channel = new Channel(getPlexus());
				
				channel.setName(ju.getChannel());
				channel.setTs(ju.getTs().getEpochSecond());
				machine.getIrc().addChannel(channel);
			}
			else
			{
				if (user.findChannel(channel) != null)
				{
					logger.debug("Join for user {} already in channel {}", user, channel);
					return null;
				}
			}

			Membership membership = new Membership(user, channel, null);
			channel.addUser(user, membership);
			user.addChannel(channel, membership);

			logger.debug("User {} joins {}", user, channel);

			sendJoin(user, channel);
			sendModes(user, channel, ju.getModes());

			return null;
		}
		finally
		{
			commit.release();
		}
	}

	private void sendJoin(User user, Channel channel)
	{
		JoinMessage message = new JoinMessage(user, channel);
		channel.send(null, message);
	}

	private void sendModes(User user, Channel channel, String modes)
	{
		if (modes == null)
			return;

		ModeStacker stacker = new ModeStacker();

		for (int i = 0; i < modes.length(); ++i)
		{
			ChannelMode cm = getPlexus().getModeManager().findChannelMode(modes.charAt(i));
			if (cm == null)
			{
				continue;
			}

			cm.set(null, channel, null, true, cm.getPrefix() > 0 ? user.getName() : null);
			stacker.set(cm.getCharacter(), cm.getPrefix() > 0 ? user.getName() : null);
		}

		StackerLine line = stacker.getLine(IRC.MAXMODEPARAMS);
		while (line != null)
		{
			// Joining user gets this through their /NAMES
			channel.send(user.isLocal() ? (LocalUser) user : null, new ChannelModeMessage(getPlexus().getMe(), channel, line.getModes(), line.getParams()));
				
			line = stacker.getLine(IRC.MAXMODEPARAMS);
		}
	}
}
