/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.messages.NickMessage;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.NickChange;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NickChangeHandler extends CommitHandler<NickChange>
{
	private static final Logger logger = LoggerFactory.getLogger(NickChangeHandler.class);

	public NickChangeHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<NickChange>> commit)
	{
		try
		{
			NickChange nc = commit.command().getCommand();

			User source = getIrc().findUser(nc.getSource());

			if (source == null || source.getTs() != nc.getSourceTs().getEpochSecond())
				return null;

			User target = getIrc().findUser(nc.getNewNick());
			if (target != null)
			{
				if (source == target)
				{
					if (source.getName().equals(nc.getNewNick()))
						return null;
				}
				else
				{
					logger.debug("Nick change collision from {} to {}", source, target);

					// XXX may be able to implicitly kill, or simply return an error...
					source.kill(getPlexus().getMe(), "Nick change collision");

					return null;
				}
			}

			if (source.isLocal())
			{
				ModeManager modeManager = getPlexus().getModeManager();

				modeManager.getRegistered().onNickChange((LocalUser) source, nc.getNewNick());

				getPlexus().getClientManager().sendToOps(modeManager.getNickChange(),
					"Nick change: From %s to %s [%s@%s]",
					source.getName(), nc.getNewNick(), source.getUsername(), source.getHost());
			}

			NickMessage message = new NickMessage(source, nc.getNewNick());
			if (source.isLocal())
				((LocalUser) source).writeMessage(message);
			source.sendCommon(source, message);

			getPlexus().getWhowas().add(source);

			logger.debug("{} changes nick to {}", source, nc.getNewNick());

			if (source != target)
			{
				// no point reinserting into the hash if the nick didnt change
				getIrc().removeUser(source);
			}

			source.changeNick(nc.getNewNick(), nc.getTs().getEpochSecond());

			if (source != target)
			{
				getIrc().addUser(source);
			}

			return null;
		}
		finally
		{
			commit.release();
		}
	}

}
