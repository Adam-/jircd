/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.CaptureUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaptureUserHandler extends CommitHandler<CaptureUser>
{
	private static final Logger logger = LoggerFactory.getLogger(CaptureUserHandler.class);

	public CaptureUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<CaptureUser>> commit)
	{
		try
		{
			CaptureUser cu = commit.command().getCommand();

			User source = getIrc().findUser(cu.getSource());
			User dest = getIrc().findUser(cu.getUser());

			if (source == null || dest == null)
				return null;

			if (dest.isLocal())
			{
				LocalUser destLocal = (LocalUser) dest;

				if (cu.isAdd() && !destLocal.isCaptured())
				{
					destLocal.setCaptured(true);
					getPlexus().getClientManager().sendToOps(null, "Captured %s (%s@%s)", dest.getName(), dest.getUsername(), dest.getHost());
				}
				else if (!cu.isAdd() && destLocal.isCaptured())
				{
					destLocal.setCaptured(false);
					getPlexus().getClientManager().sendToOps(null, "Uncaptured %s (%s@%s)", dest.getName(), dest.getUsername(), dest.getHost());
				}
			}

			return null;
		}
		finally
		{
			commit.release();
		}
	}

}
