/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.replication.handlers;

import io.atomix.copycat.server.Commit;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Invite;
import net.rizon.plexus.messages.InviteMessage;
import net.rizon.plexus.replication.CommitHandler;
import net.rizon.plexus.replication.PlexusCopycatCommand;
import net.rizon.plexus.replication.PlexusMachine;
import net.rizon.plexus.replication.commands.InviteUser;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InviteUserHandler extends CommitHandler<InviteUser>
{
	private static final Logger logger = LoggerFactory.getLogger(InviteUserHandler.class);

	public InviteUserHandler(PlexusMachine machine)
	{
		super(machine);
	}

	@Override
	public Object apply(Commit<PlexusCopycatCommand<InviteUser>> commit)
	{
		try
		{
			InviteUser iu = commit.command().getCommand();

			User source = getIrc().findUser(iu.getSource());
			User dest = getIrc().findUser(iu.getDest());

			if (source == null || dest == null)
				return null;

			Channel channel = getIrc().findChannel(iu.getChannel());
			if (channel == null || channel.getTs() != iu.getChannelTs())
				return null;

			logger.debug("{} invites {} to {}", source, dest, channel);

			// User already on channel?
			if (channel.findUser(dest) != null)
				return null;

			Invite invite = new Invite(source, dest, channel, iu.getTs());

			source.addInvite(invite);
			dest.addInvite(invite);
			channel.addInvite(invite);

			if (dest.isLocal())
			{
				LocalUser localDest = (LocalUser) dest;

				localDest.writeMessage(new InviteMessage(source, dest, channel));
			}

			return null;

		}
		finally
		{
			commit.release();
		}
	}
}
