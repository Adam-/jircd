/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.channels;

import java.util.Date;

public class Entry
{
	private String nick;
	private String user;
	private String host;
	private final String who;
	private final Date when;
	
	public Entry(String mask, String who, Date when)
	{
		int i = mask.indexOf('!');
		if (i != -1)
		{
			if (i > 0)
				nick = mask.substring(0, i);
			
			int j = mask.indexOf('@', i + 1);
			if (j != -1)
			{
				user = mask.substring(i + 1, j);
				host = mask.substring(j + 1);
			}
			else
			{
				user = mask.substring(i + 1);
			}
		}
		else
		{
			int j = mask.indexOf('@');
			if (j != -1)
			{
				user = mask.substring(0, j);
				host = mask.substring(j + 1);
			}
			else if (mask.indexOf('.') != -1 || mask.indexOf(':') != -1)
				host = mask;
			else
				nick = mask;
		}
		
		if (nick == null || nick.equals(""))
			nick = "*";
		if (user == null || user.equals(""))
			user = "*";
		if (host == null || host.equals(""))
			host = "*";
		
		this.who = who;
		this.when = when;
	}
	
	public String getMask()
	{
		return nick + "!" + user + "@" + host;
	}

	public String getNick()
	{
		return nick;
	}

	public String getUser()
	{
		return user;
	}

	public String getHost()
	{
		return host;
	}

	public String getWho()
	{
		return who;
	}

	public Date getWhen()
	{
		return when;
	}
}