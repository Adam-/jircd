/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.channels;

import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.modes.type.Status;
import net.rizon.plexus.users.User;

public class Membership
{
	private final User user;
	private final Channel channel;
	private String modes;

	public Membership(User user, Channel channel, String modes)
	{
		this.user = user;
		this.channel = channel;
		this.modes = modes;
	}

	public String getPrefixes()
	{
		StringBuilder sb = new StringBuilder();
		ModeManager modeManager = user.getPlexus().getModeManager();
		if (modes != null)
		{
			for (int i = 0; i < modes.length(); ++i)
			{
				ChannelMode cm = modeManager.findChannelMode(modes.charAt(i));
				if (cm != null && cm.getPrefix() > 0)
				{
					sb.append(cm.getPrefix());
				}
			}
		}
		return sb.toString();
	}

	public void setMode(Status cm)
	{
		if (hasMode(cm))
		{
			return;
		}

		if (modes == null)
		{
			modes = "" + cm.getCharacter();
		}
		else
		{
			modes += cm.getCharacter();
		}
	}

	public void unsetMode(Status cm)
	{
		modes = modes.replace("" + cm.getCharacter(), "");

		if (modes.isEmpty())
		{
			modes = null;
		}
	}

	public boolean hasMode(Status cm)
	{
		return modes != null && modes.indexOf(cm.getCharacter()) != -1;
	}

	public User getUser()
	{
		return user;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public String getModes()
	{
		return modes;
	}
}
