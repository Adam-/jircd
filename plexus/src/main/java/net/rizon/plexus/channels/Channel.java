/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.channels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Message;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;
import net.rizon.plexus.util.Util;

public class Channel
{
	private final Plexus plexus;

	private String name;
	private long ts;
	
	private final Map<User, Membership> users = new HashMap<>();
	
	private String modes = "";
	private String key;
	private int limit;
	private List<Entry> bans = new ArrayList<>();
	private List<Entry> invex = new ArrayList<>();
	private List<Entry> excepts = new ArrayList<>();
	
	private String topic;
	private String topicSetter;
	private long topicTime;
	
	private long lastKnock;

	private List<Invite> invites = new ArrayList<>();

	public Channel(Plexus plexus)
	{
		this.plexus = plexus;
	}
	
	public void destroy()
	{
		assert users.isEmpty();

		// Remove invites to the channel
		clearInvites();
	}
	
	public void addUser(User u, Membership mem)
	{
		users.put(u, mem);
	}
	
	public void removeUser(User u)
	{
		users.remove(u);
	}
	
	public Membership findUser(User u)
	{
		return users.get(u);
	}
	
	public Collection<Membership> getUsers()
	{
		return users.values();
	}
	
	public List<LocalUser> getLocalUsers()
	{
		return users.keySet().stream().filter(u -> u.isLocal()).map(u -> (LocalUser) u).collect(Collectors.toList());
	}

	public List<Entry> getBans()
	{
		return bans;
	}

	public void setBans(List<Entry> bans)
	{
		this.bans = bans;
	}

	public List<Entry> getInvex()
	{
		return invex;
	}

	public void setInvex(List<Entry> invex)
	{
		this.invex = invex;
	}

	public List<Entry> getExcepts()
	{
		return excepts;
	}

	public void setExcepts(List<Entry> excepts)
	{
		this.excepts = excepts;
	}

	public String getModes()
	{
		return modes;
	}

	public void setModes(String modes)
	{
		this.modes = modes;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public void setKey(String key)
	{
		this.key = key;
	}
	
	public int getLimit()
	{
		return limit;
	}
	
	public void setLimit(int limit)
	{
		this.limit = limit;
	}
	
	public boolean isBanned(LocalUser user)
	{
		if (this.bans == null)
			return false;
		for (Entry e : this.bans)
			if (Util.match(user.getName() + "!" + user.getUsername() + "@" + user.getVhost(), e.getNick() + "!" + e.getUser() + "@" + e.getHost())) // XXX cidr, ?
				return true;
		return false;
	}
	
	public boolean isExempt(LocalUser user)
	{
		if (this.excepts == null)
			return false;
		for (Entry e : this.excepts)
			if (Util.match(user.getName() + "!" + user.getUsername() + "@" + user.getVhost(), e.getNick() + "!" + e.getUser() + "@" + e.getHost())) // XXX cidr, ?
				return true;
		return false;
	}
	
	public boolean isInvex(LocalUser user)
	{
		if (this.invex == null)
			return false;
		for (Entry e : this.invex)
			if (Util.match(user.getName() + "!" + user.getUsername() + "@" + user.getVhost(), e.getNick() + "!" + e.getUser() + "@" + e.getHost())) // XXX cidr, ?
				return true;
		return false;
	}

	public void send(LocalUser except, Message message)
	{
		for (LocalUser user : this.getLocalUsers())
			if (user != except)
				user.writeMessage(message);
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public long getTs()
	{
		return ts;
	}

	public void setTs(long ts)
	{
		this.ts = ts;
	}

	public String getTopic()
	{
		return topic;
	}

	public void setTopic(String topic)
	{
		this.topic = topic;
	}

	public String getTopicSetter()
	{
		return topicSetter;
	}

	public void setTopicSetter(String topicSetter)
	{
		this.topicSetter = topicSetter;
	}

	public long getTopicTime()
	{
		return topicTime;
	}

	public void setTopicTime(long topicTime)
	{
		this.topicTime = topicTime;
	}

	public long getLastKnock()
	{
		return lastKnock;
	}

	public void setLastKnock(long lastKnock)
	{
		this.lastKnock = lastKnock;
	}

	public void addInvite(Invite i)
	{
		if (invites.size() >= IRC.INVITE_HISTORY)
			invites.remove(0);
		
		invites.add(i);
	}

	public void removeInvite(Invite i)
	{
		invites.remove(i);
	}

	private void clearInvites()
	{
		List<Invite> is = invites;
		invites = new ArrayList<>();

		for (Invite i : is)
			i.remove();
	}

	public void setMode(char mode)
	{
		if (hasMode(mode))
			return;

		char[] newModes = (modes + mode).toCharArray();
		Arrays.sort(newModes);
		modes = new String(newModes);
	}

	public boolean hasMode(ChannelMode mode)
	{
		return hasMode(mode.getCharacter());
	}
	
	public boolean hasMode(char mode)
	{
		return modes.contains("" + mode);
	}

	public void unsetMode(char mode)
	{
		modes = modes.replace("" + mode, "");
	}

	public String getModeString(boolean full)
	{
		StringBuilder sb = new StringBuilder();
		sb.append('+').append(modes);

		if (key != null)
		{
			sb.append(' ').append(key);
		}

		if (limit != 0)
		{
			sb.append(' ').append(limit);
		}

		return sb.toString();
	}
}