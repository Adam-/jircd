/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.channels;

import java.time.Instant;
import net.rizon.plexus.users.User;

public class Invite
{
	private User from;
	private final User to;
	private final Channel channel;
	private final Instant when;

	public Invite(User from, User to, Channel channel, Instant when)
	{
		this.from = from;
		this.to = to;
		this.channel = channel;
		this.when = when;
	}

	@Override
	public String toString()
	{
		return "Invite{" + "from=" + from + ", to=" + to + ", channel=" + channel + ", when=" + when + '}';
	}

	public User getFrom()
	{
		return from;
	}

	public void setFrom(User from)
	{
		this.from = from;
	}

	public User getTo()
	{
		return to;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public Instant getWhen()
	{
		return when;
	}

	public void remove()
	{
		if (from != null)
			from.removeInvite(this);

		if (to != null)
			to.removeInvite(this);

		if (channel != null)
			channel.removeInvite(this);
	}
}
