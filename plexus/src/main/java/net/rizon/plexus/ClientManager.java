/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.users.LocalUser;

public class ClientManager
{
	private long currentSerial;

	private final Set<Client> clients = new HashSet<>();
	private final List<LocalUser> localOpers = new ArrayList<>();
	private final List<LocalUser> localUsers = new ArrayList<>();
	
	public long getNextSerial()
	{
		return currentSerial++;
	}
	
	public void insertClient(Client client)
	{
		clients.add(client);
	}
	
	public void removeClient(Client client)
	{
		clients.remove(client);
	}
	
	public Collection<Client> getClients()
	{
		return clients;
	}
	
	public void addLocalOper(LocalUser user)
	{
		if (!localOpers.contains(user))
			localOpers.add(user);
	}
	
	public void removeLocalOper(LocalUser user)
	{
		localOpers.remove(user);
	}
	
	public Collection<LocalUser> getLocalOpers()
	{
		return localOpers;
	}
	
	public void addLocalUser(LocalUser user)
	{
		localUsers.add(user);
	}
	
	public void removeLocalUser(LocalUser user)
	{
		localUsers.remove(user);
	}
	
	public Collection<LocalUser> getLocalUsers()
	{
		return localUsers;
	}
	
	public void sendToOps(UserMode mode, String format, Object... args)
	{
		for (LocalUser u : localOpers)
			if (mode == null || u.hasMode(mode))
				u.writeNotice(format, args);
	}
	
	public void sendToOps(UserMode mode, Message message)
	{
		for (LocalUser u : localOpers)
			if (mode == null || u.hasMode(mode))
				u.writeMessage(message);
	}
}
