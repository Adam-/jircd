/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import java.lang.reflect.Field;
import java.util.Arrays;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.io.IRCMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Message
{
	private static final Logger logger = LoggerFactory.getLogger(Message.class);

	private IRCMessage localBuf, operBuf;

	private final Entity source;
	private String command;
	private Numeric numeric;

	protected Message(Entity source, String command)
	{
		this.source = source;
		this.command = command;
	}

	protected Message(Entity source, Numeric numeric)
	{
		this.source = source;
		this.numeric = numeric;
	}

	public IRCMessage pack(Messageable to)
	{
		// check if finished buffer is cached
		if (to.isOper())
		{
			if (operBuf != null)
			{
				return operBuf;
			}
		}
		else if (localBuf != null)
		{
			return localBuf;
		}

		IRCMessage message = new IRCMessage();
		if (source != null)
		{
			message.setSource(source.getSource());
		}

		if (command != null)
		{
			message.setCommand(command);
		}
		else if (numeric != null)
		{
			message.setCommand(String.format("%03d", numeric.getNumeric()));
			message.push(to.getMessageName()); // first parameter of numeric is always the users name
		}
		else
		{
			assert false : "message with no command or numeric";
			return null;
		}

		Class<?> c = this.getClass();

		// XXX cache this
		Field[] fields = Arrays.stream(c.getDeclaredFields())
			.filter(f -> f.getAnnotation(Parameter.class) != null)
			.sorted((f1, f2) -> Integer.compare(f1.getAnnotation(Parameter.class).index(), f2.getAnnotation(Parameter.class).index()))
			.toArray(i -> new Field[i]);

		for (int i = 0; i < fields.length; ++i)
		{
			Field f = fields[i];
			Class<?> type = f.getType();

			f.setAccessible(true);

			try
			{
				Object object = f.get(this);

				if (Entity.class.isAssignableFrom(type))
				{
					String id = ((Entity) object).getName();
					message.push(id);
				}
				else if (type == Client.class)
				{
					String id = ((Client) object).getMessageName();
					message.push(id);
				}
				else if (type == Channel.class)
				{
					String name = ((Channel) object).getName();
					message.push(name);
				}
				else if (type == String[].class)
				{
					StringBuilder sb = new StringBuilder();
					for (String s : (String[]) object)
					{
						sb.append(s).append(" ");
					}
					message.push(sb.toString());
				}
				else if (object != null)
				{
					message.push(object.toString());
				}
			}
			catch (IllegalArgumentException | IllegalAccessException ex)
			{
				logger.warn(null, ex);
			}
		}

		// cache result
		if (to.isOper())
		{
			operBuf = message;
		}
		else
		{
			localBuf = message;
		}

		return message;
	}
}
