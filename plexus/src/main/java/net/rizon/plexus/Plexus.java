/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.WriteBufferWaterMark;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import net.rizon.plexus.command.CommandManager;
import net.rizon.plexus.commands.list.ListManager;
import net.rizon.plexus.commands.whowas.WhowasManager;
import net.rizon.plexus.config.Config;
import net.rizon.plexus.config.ConfigException;
import net.rizon.plexus.config.ConfigLoader;
import net.rizon.plexus.config.Listen;
import net.rizon.plexus.dns.DnsClient;
import net.rizon.plexus.ident.IdentClient;
import net.rizon.plexus.io.Initializer;
import net.rizon.plexus.io.Listener;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.replication.ReplicationClient;
import net.rizon.plexus.replication.ReplicationServer;
import net.rizon.plexus.xline.XLineManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Plexus
{
	public static Logger logger = LoggerFactory.getLogger(Plexus.class);

	private final Date start = new Date();

	private final EventLoopGroup bossGroup = new NioEventLoopGroup();
	private final EventLoopGroup workerGroup = new NioEventLoopGroup(1);

	private Config conf;
	private Server me;

	private ReplicationClient client;
	private ReplicationServer server;

	private IRC irc = new IRC(this);

	private final CommandManager commandManager = new CommandManager(this);
	private final IdentClient identClient = new IdentClient(this);
	private final Supported supported = new Supported(this);
	private final ModeManager modeManager = new ModeManager(this);
	private final ClientManager clientManager = new ClientManager();
	private final DnsClient dnsClient = new DnsClient(workerGroup.next());
	private final WhowasManager whowas = new WhowasManager();
	private final ListManager listManager = new ListManager();
	private final XLineManager xlineManager = new XLineManager(this);
	private final IRCStats stats = new IRCStats();

	private final List<Listener> listeners = new ArrayList<>();

	private MOTD motd;

	public static void main(String[] args)
	{
		Thread.setDefaultUncaughtExceptionHandler((thread, ex) -> 
			{
				logger.error("Uncaught exception in thread " + thread, ex);
		});

		Plexus plexus = new Plexus();
		plexus.run(args);
		plexus.awaitUninterruptibly();
		plexus.stop();
	}

	public void run(String[] args)
	{
		ServerBootstrap server = new ServerBootstrap()
			.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.childHandler(new Initializer(this, false))
			.option(ChannelOption.SO_BACKLOG, IRC.SO_BACKLOG)
			.option(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(IRC.DEFAULT_SENDQ, IRC.DEFAULT_SENDQ))
			.childOption(ChannelOption.SO_KEEPALIVE, true);

		ServerBootstrap serverSSL = new ServerBootstrap()
			.group(bossGroup, workerGroup)
			.channel(NioServerSocketChannel.class)
			.childHandler(new Initializer(this, true))
			.option(ChannelOption.SO_BACKLOG, IRC.SO_BACKLOG)
			.option(ChannelOption.WRITE_BUFFER_WATER_MARK, new WriteBufferWaterMark(IRC.DEFAULT_SENDQ, IRC.DEFAULT_SENDQ))
			.childOption(ChannelOption.SO_KEEPALIVE, true);

		try
		{
			setConf(ConfigLoader.load());
		}
		catch (ConfigException ex)
		{
			logger.error("Unable to load config", ex);
			return;
		}

		supported.setup();

		me = new Server(this);
		me.setId(UUID.randomUUID());
		me.setName(conf.getServerinfo().getName());
		me.setDescription(conf.getServerinfo().getDescription());
		irc.addServer(me);

		workerGroup.scheduleAtFixedRate(new Pinger(this), 30, 30, TimeUnit.SECONDS);

		this.server = new ReplicationServer(this);
		this.server.start();

		client = new ReplicationClient(this);
		client.start();

		for (Listen l : conf.getListen())
		{
			ServerBootstrap b = l.isSsl() ? serverSSL : server;
			for (int port : l.getPort())
			{
				Channel channel = b.bind(port).channel();
				Listener listener = new Listener(channel, l);

				listeners.add(listener);
			}
		}
	}

	public void awaitUninterruptibly()
	{
		bossGroup.terminationFuture().awaitUninterruptibly();
		workerGroup.terminationFuture().awaitUninterruptibly();
	}

	public void stop()
	{
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
		
		client.stop();
		server.stop();
	}

	public Date getStart()
	{
		return start;
	}

	public CommandManager getCommandManager()
	{
		return commandManager;
	}

	public IdentClient getIdentClient()
	{
		return identClient;
	}

	public Config getConf()
	{
		return conf;
	}

	public void setConf(Config conf)
	{
		this.conf = conf;

		try
		{
			if (conf.getGeneral().getMotd() != null)
			{
				motd = new MOTD(new File(conf.getGeneral().getMotd()));
			}
		}
		catch (IOException ex)
		{
			logger.warn("Unable to load motd", ex);
		}
	}

	public Supported getSupported()
	{
		return supported;
	}

	public IRC getIrc()
	{
		return irc;
	}

	public void setIrc(IRC irc)
	{
		this.irc = irc;
	}

	public ModeManager getModeManager()
	{
		return modeManager;
	}

	public ClientManager getClientManager()
	{
		return clientManager;
	}

	public EventLoopGroup getBossGroup()
	{
		return bossGroup;
	}

	public EventLoopGroup getWorkerGroup()
	{
		return workerGroup;
	}

	public DnsClient getDnsClient()
	{
		return dnsClient;
	}

	public ReplicationClient getClient()
	{
		return client;
	}

	public ReplicationServer getServer()
	{
		return server;
	}

	public Server getMe()
	{
		return me;
	}

	public WhowasManager getWhowas()
	{
		return whowas;
	}

	public ListManager getListManager()
	{
		return listManager;
	}

	public XLineManager getXlineManager()
	{
		return xlineManager;
	}

	public List<Listener> getListeners()
	{
		return listeners;
	}

	public MOTD getMotd()
	{
		return motd;
	}

	public IRCStats getStats()
	{
		return stats;
	}
}
