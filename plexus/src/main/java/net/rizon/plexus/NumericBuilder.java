/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus;

import java.util.ArrayList;
import java.util.List;
import net.rizon.plexus.users.LocalUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumericBuilder
{
	private static final Logger logger = LoggerFactory.getLogger(NumericBuilder.class);

	private final Numeric numeric;
	private final LocalUser to;
	private final Object[] args;
	private final List<String> split = new ArrayList<>();

	private Message message;
	private BuildableNumeric buildableNumeric;

	public NumericBuilder(Numeric num, LocalUser to, Object... args)
	{
		this.numeric = num;
		this.to = to;
		this.args = args;

		buildMessage();
	}

	private void buildMessage()
	{
		message = numeric.buildMessage(to.getPlexus().getMe(), args);
		buildableNumeric = (BuildableNumeric) message;
	}

	public void add(String s)
	{
		split.add(s);
	}

	public void send()
	{
		// :server.name 005 nick args... :are supported by this server
		final int startLen = 1 // :
			+ to.getPlexus().getMe().getName().length() // server name
			+ 1 // space
			+ 3 // numeric, assumed 3
			+ 1 // space
			+ to.getName().length() // nick
			+ 1; // space

		// Final len of the numeric minus the variable part
		final int numericLen = startLen
			+ buildableNumeric.getArgs() // number of args (spaces)
			+ buildableNumeric.getLength() // length of fixed args
			+ 1; // for the :

		StringBuilder sb = new StringBuilder();
		int startArgs = 1 + buildableNumeric.getArgs(); // + 1 for nickname
		int args = startArgs;
		for (String s : split)
		{
			int toAdd = (sb.length() > 0 ? 1 : 0) + s.length();

			if ((buildableNumeric.wrapMaxArgs() && args >= IRC.MAXARGS) || startLen + numericLen + sb.length() + toAdd > IRC.MAXBUF - 2) // - 2 for \r\n
			{
				buildableNumeric.setReplacement(sb.toString());

				to.writeMessage(message);

				buildMessage();
				sb = new StringBuilder();
				args = startArgs;
			}

			if (sb.length() > 0)
			{
				sb.append(' ');
			}
			sb.append(s);
			++args;
		}

		if (sb.length() > 0)
		{
			buildableNumeric.setReplacement(sb.toString());

			to.writeMessage(message);
		}
	}
}
