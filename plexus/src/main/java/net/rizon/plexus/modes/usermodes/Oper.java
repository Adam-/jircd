/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.usermodes;

import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.modes.ModeStacker;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.replication.commands.UserModeSet;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Oper extends UserMode
{
	public Oper(ModeManager manager)
	{
		super(manager, 'o');
	}
	
	@Override
	public void change(User u, boolean add) 
	{
		if (u.isLocal())
		{
			LocalUser lu = (LocalUser) u;
			if (add)
				getPlexus().getClientManager().addLocalOper(lu);
			else
				getPlexus().getClientManager().removeLocalOper(lu);
		}
		
		super.change(u, add);
		
		if (!add && u.isLocal())
		{
			LocalUser lu = (LocalUser) u;
			
			// unassociate oline with the user
			lu.setOper(null);
			
			// Remove all oper only usermodes
			ModeStacker stack = new ModeStacker();
			for (int i = 0; i < u.getModes().length(); ++i)
			{
				UserMode um = getManager().findUserMode(u.getModes().charAt(i));

				if (um == null || !um.isUnsettable())
					continue;
				
				if (!u.hasMode(um))
					continue;
				
				if (getPlexus().getConf().getGeneral().getUser_umodes().indexOf(um.getCharacter()) != -1)
					continue;
				
				stack.unset(um.getCharacter());
			}

			if (stack.empty() == false)
			{
				UserModeSet ums = new UserModeSet();
				ums.setSource(u.getId());
				ums.setTarget(u.getId());
				ums.setChanges(stack.getAll());

				getPlexus().getClient().submit(ums);
			}
		}
	}
}