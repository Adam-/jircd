/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes;


import net.rizon.plexus.Entity;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.users.LocalUser;

public abstract class ChannelMode extends Mode
{
	private char prefix;
	private boolean parameter;
	private boolean minusNoArg;
	private boolean list;
	private short position;

	public ChannelMode(ModeManager manager, char character)
	{
		super(manager, character);
		
		manager.insertChannelMode(this);
	}
	
	public boolean canSet(LocalUser u, Channel c, Membership mem, boolean add, String param, boolean shownError)
	{
		if (u.isOper() || getPlexus().getConf().getGeneral().getUser_chmodes().indexOf(this.getCharacter()) != -1)
			return true;
		
		if (!shownError)
			u.writeNumeric(Numeric.ERR_NOPRIVILEGES);
		return false;
	}
	
	public boolean hasMode(Channel c, String param)
	{
		return c.hasMode(this);
	}
	
	public boolean set(Entity source, Channel c, Membership mem, boolean add, String param)
	{
		if (add)
			c.setMode(this.getCharacter());
		else
			c.unsetMode(this.getCharacter());;

		return true;
	}
	
	public String paramValid(LocalUser u, Channel c, Membership mem, boolean add, String param)
	{
		return param;
	}

	public char getPrefix()
	{
		return prefix;
	}

	final public void setPrefix(char prefix)
	{
		this.prefix = prefix;
	}

	public boolean isParameter()
	{
		return parameter;
	}

	final public void setParameter(boolean parameter)
	{
		this.parameter = parameter;
	}

	public boolean isMinusNoArg()
	{
		return minusNoArg;
	}

	final public void setMinusNoArg(boolean minusNoArg)
	{
		this.minusNoArg = minusNoArg;
	}

	public boolean isList()
	{
		return list;
	}

	final public void setList(boolean list)
	{
		this.list = list;
	}

	public short getPosition()
	{
		return position;
	}

	final public void setPosition(short position)
	{
		this.position = position;
	}
}