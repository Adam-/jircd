/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.type;

import net.rizon.plexus.Entity;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Status extends ChannelMode implements Comparable<Status>
{
	public Status(ModeManager manager, char c, char prefix, int position)
	{
		super(manager, c);
		
		this.setPrefix(prefix);
		this.setPosition((short) position);
	}
	
	@Override
	public boolean hasMode(Channel c, String param)
	{
		User target = getPlexus().getIrc().findUser(param);
		if (target == null)
			return false;
		
		Membership mem = c.findUser(target);
		if (mem == null)
			return false;
		
		return mem.getModes().indexOf(this.getCharacter()) != -1;
	}
	
	@Override
	public boolean set(Entity source, Channel c, Membership mem, boolean add, String param)
	{
		User targ = getPlexus().getIrc().findUser(param);
		
		if (targ == null)
			return false;
		
		mem = c.findUser(targ);
		if (mem == null)
			return false;
		
		if (add)
		{
			if (mem.hasMode(this))
				return false;
			
			mem.setMode(this);
		}
		else
		{
			if (!mem.hasMode(this))
				return false;
			
			mem.unsetMode(this);
		}
		
		return true;
	}
	
	@Override
	public String paramValid(LocalUser u, Channel c, Membership mem, boolean add, String param)
	{
		if (param == null)
			return null;
		
		User targ = getPlexus().getIrc().findUser(param);
		if (targ == null)
		{
			u.writeNumeric(Numeric.ERR_NOSUCHNICK, param);
			return null;
		}
		
		if (c.findUser(targ) == null)
		{
			u.writeNumeric(Numeric.ERR_NOTONCHANNEL, targ.getName());
			return null;
		}
		
		return targ.getName();
	}

	@Override
	public int compareTo(Status o)
	{
		if (this.getPosition() < o.getPosition())
			return 1;
		else if (this.getPosition() > o.getPosition())
			return -1;
		return 0;
	}
	
}