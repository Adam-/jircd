/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.plexus.modes;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import net.rizon.plexus.IRC;

public class ModeStacker
{
	private final Deque<StackInfo> set = new ArrayDeque<>();
	private final Deque<StackInfo> unset = new ArrayDeque<>();

	private static boolean exists(Deque<StackInfo> list, StackInfo info, boolean remove)
	{
		for (Iterator<StackInfo> it = list.iterator(); it.hasNext();)
		{
			StackInfo si = it.next();

			if (info.equals(si))
			{
				if (remove)
				{
					it.remove();
				}
				return true;
			}
		}

		return false;
	}

	private void set(boolean add, char c, String param)
	{
		StackInfo si = new StackInfo(c, param);

		if (add)
		{
			if (exists(set, si, false))
			{
				return;
			}
			if (exists(unset, si, true))
			{
				return;
			}

			set.add(si);
		}
		else
		{
			if (exists(unset, si, false))
			{
				return;
			}
			if (exists(set, si, true))
			{
				return;
			}

			unset.add(si);
		}
	}

	public void set(char c)
	{
		set(true, c, null);
	}

	public void set(char c, String param)
	{
		set(true, c, param);
	}

	public void unset(char c)
	{
		set(false, c, null);
	}

	public void unset(char c, String param)
	{
		set(false, c, param);
	}

	public StackerLine getLine(int max)
	{
		StringBuilder modes = new StringBuilder();
		List<String> params = new ArrayList<>();
		
		boolean adding = true;
		int numParams = 0;

		for (;;)
		{
			StackInfo si;

			if (max > 0 && numParams++ >= max)
			{
				break;
			}

			if (!set.isEmpty())
			{
				if (modes.length() == 0)
				{
					modes.append('+');
				}

				si = set.removeFirst();
			}
			else if (!unset.isEmpty())
			{
				if (adding)
				{
					adding = false;
					modes.append('-');
				}

				si = unset.removeFirst();
			}
			else
			{
				break;
			}

			modes.append(si.getModeCharacter());
			if (si.getParam() != null)
			{
				params.add(si.getParam());
			}
		}

		if (modes.length() == 0)
		{
			return null;
		}
		
		return new StackerLine(modes.toString(), params.toArray(new String[params.size()]));
	}
	
	public String getLineString(int max)
	{
		StackerLine stacker = getLine(max);
		return stacker != null ? stacker.getModeString() : null;
	}

	public String getLine()
	{
		return this.getLineString(IRC.MAXMODEPARAMS);
	}

	public String getAll()
	{
		return this.getLineString(0);
	}

	public boolean empty()
	{
		return set.isEmpty() && unset.isEmpty();
	}
}
