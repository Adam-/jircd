/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.usermodes;

import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.messages.UserModeMessage;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public class Registered extends UserMode
{
	public Registered(ModeManager manager)
	{
		super(manager, 'r');
	}
	
	public void whois(LocalUser source, User target)
	{
		if (target.hasMode(getManager().getRegistered()))
		{
			if (!getPlexus().getConf().getGeneral().isAccount_whois() && (source == target || source.isOper()) && target.getAccount() != null)
				source.writeNumeric(Numeric.RPL_WHOISREGNICK, target.getName(), target.getAccount());
			else
				source.writeNumeric(Numeric.RPL_WHOISREGNICK, target.getName(), "this nick");
		}
	}
	
	public void onNickChange(LocalUser user, String newNick)
	{
		if (!IRC.equalsIgnoreCase(user.getName(), newNick))
		{
			if (user.hasMode(getManager().getRegistered()))
			{
				change(user, false);
				if (user.isLocal())
					user.writeMessage(new UserModeMessage(user, user, "-r"));
			}
		}
	}
}