/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.users.LocalUser;
import net.rizon.plexus.users.User;

public abstract class UserMode extends Mode
{
	/* Restricted modes are not settable via O:lines and can only be set by either a
	 * SVSMODE from services or from a server introducing a client with the mode.
	 */
	private boolean restricted;
	private boolean unsettable = true;

	public UserMode(ModeManager manager, char character)
	{
		super(manager, character);
		
		manager.insertUserMode(this);
	}
	
	protected boolean canSet(LocalUser u, boolean add)
	{
		// If the mode can't ever be unset it most certainly can't be set, so deny either way
		if (!this.unsettable)
			return false;
		
		// Otherwise you are free to unset anything that you already have
		if (!add)
			return true;
		
		// Restricted modes can never be set by a user
		if (this.restricted)
			return false;
		
		// Allow if their oline lets them
		if (u.getOper() != null && u.getOper().getUmodes().indexOf(this.getCharacter()) != -1)
			return true;
		
		// Otherwise only allow if its an explicitly given usermode
		if (getPlexus().getConf().getGeneral().getUser_umodes().indexOf(this.getCharacter()) != -1)
			return true;
		
		return false;
	}
	
	public boolean canSet(LocalUser u, boolean add, boolean shownError)
	{
		boolean b = this.canSet(u, add);
		if (!b && !shownError)
			u.writeNumeric(Numeric.ERR_NOPRIVILEGES);
		return b;
	}
	
	public void change(User u, boolean add) 
	{
		if (add)
			u.setMode(this.getCharacter());
		else
			u.unsetMode(this.getCharacter());
	}

	public boolean isRestricted()
	{
		return restricted;
	}

	final public void setRestricted(boolean restricted)
	{
		this.restricted = restricted;
	}

	public boolean isUnsettable()
	{
		return unsettable;
	}

	final public void setUnsettable(boolean unsettable)
	{
		this.unsettable = unsettable;
	}
}