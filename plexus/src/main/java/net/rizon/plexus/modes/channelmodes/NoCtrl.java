/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.channelmodes;

import net.rizon.plexus.Numeric;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;

public class NoCtrl extends ChannelMode
{
	public NoCtrl(ModeManager manager)
	{
		super(manager, 'c');
	}
	
	protected static boolean hasCTRLs(String msg)
	{
		for (int i = 0; i < msg.length(); ++i)
		{
			char c = msg.charAt(i);
			
			/* not a control code */
			if (c > 31)
				continue;
			
			/* ctcp */
			if (c == 1)
				continue;
			
			/* escape */
			if (c == 27 && i + 1 < msg.length())
			{
				c = msg.charAt(i + 1);
				if (c == '$' || c == '(')
				{
					++i;
					continue;
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	public boolean checkMessage(LocalUser user, Channel c, boolean privmsg, String msg)
	{
		if (!user.isOper() && c.hasMode(getManager().getNoCtrl()) && hasCTRLs(msg))
		{
			if (privmsg)
			{
				String message = String.format("You cannot use control codes on %s. Not sent: %s", c.getName(), msg);
				user.writeNumeric(Numeric.ERR_NOCTRLSONCHAN, c.getName(), message);
			}
			return false;
		}
		return true;
	}
}