/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import net.rizon.plexus.Plexus;
import net.rizon.plexus.modes.channelmodes.BWSaver;
import net.rizon.plexus.modes.channelmodes.Ban;
import net.rizon.plexus.modes.channelmodes.Except;
import net.rizon.plexus.modes.channelmodes.HalfOp;
import net.rizon.plexus.modes.channelmodes.Invex;
import net.rizon.plexus.modes.channelmodes.InviteOnly;
import net.rizon.plexus.modes.channelmodes.Key;
import net.rizon.plexus.modes.channelmodes.Limit;
import net.rizon.plexus.modes.channelmodes.ModReg;
import net.rizon.plexus.modes.channelmodes.Moderated;
import net.rizon.plexus.modes.channelmodes.NoCTCP;
import net.rizon.plexus.modes.channelmodes.NoCtrl;
import net.rizon.plexus.modes.channelmodes.NoExternal;
import net.rizon.plexus.modes.channelmodes.NoNotice;
import net.rizon.plexus.modes.channelmodes.Op;
import net.rizon.plexus.modes.channelmodes.OperOnly;
import net.rizon.plexus.modes.channelmodes.Owner;
import net.rizon.plexus.modes.channelmodes.Persist;
import net.rizon.plexus.modes.channelmodes.Private;
import net.rizon.plexus.modes.channelmodes.Protect;
import net.rizon.plexus.modes.channelmodes.RegOnly;
import net.rizon.plexus.modes.channelmodes.SSLOnly;
import net.rizon.plexus.modes.channelmodes.Secret;
import net.rizon.plexus.modes.channelmodes.TopicLimit;
import net.rizon.plexus.modes.channelmodes.Voice;
import net.rizon.plexus.modes.type.Status;
import net.rizon.plexus.modes.usermodes.Admin;
import net.rizon.plexus.modes.usermodes.Bots;
import net.rizon.plexus.modes.usermodes.CConn;
import net.rizon.plexus.modes.usermodes.CallerID;
import net.rizon.plexus.modes.usermodes.Cloak;
import net.rizon.plexus.modes.usermodes.Deaf;
import net.rizon.plexus.modes.usermodes.Debug;
import net.rizon.plexus.modes.usermodes.External;
import net.rizon.plexus.modes.usermodes.FarConnect;
import net.rizon.plexus.modes.usermodes.Full;
import net.rizon.plexus.modes.usermodes.HideChannels;
import net.rizon.plexus.modes.usermodes.Invisible;
import net.rizon.plexus.modes.usermodes.Locops;
import net.rizon.plexus.modes.usermodes.NetAdmin;
import net.rizon.plexus.modes.usermodes.NickChange;
import net.rizon.plexus.modes.usermodes.Oper;
import net.rizon.plexus.modes.usermodes.Operwall;
import net.rizon.plexus.modes.usermodes.Registered;
import net.rizon.plexus.modes.usermodes.Reject;
import net.rizon.plexus.modes.usermodes.Routing;
import net.rizon.plexus.modes.usermodes.SKill;
import net.rizon.plexus.modes.usermodes.SSL;
import net.rizon.plexus.modes.usermodes.ServerNotice;
import net.rizon.plexus.modes.usermodes.Service;
import net.rizon.plexus.modes.usermodes.SoftCallerID;
import net.rizon.plexus.modes.usermodes.Spy;
import net.rizon.plexus.modes.usermodes.Unauth;
import net.rizon.plexus.modes.usermodes.Wallop;
import net.rizon.plexus.modes.usermodes.WebIRC;

public class ModeManager
{
	private final UserMode[] userModes = new UserMode['z' - 'A' + 1];
	private final ChannelMode[] channelModes = new ChannelMode['z' - 'A' + 1];
	
	private final Plexus plexus;

	private final Admin admin = new Admin(this);
	private final Bots bots = new Bots(this);
	private final CConn cconn = new CConn(this);
	private final Cloak cloak = new Cloak(this);
	private final Deaf deaf = new Deaf(this);
	private final Debug debug = new Debug(this);
	private final CallerID callerId = new CallerID(this);
	private final External external = new External(this);
	private final FarConnect farConnect = new FarConnect(this);
	private final Full full = new Full(this);
	private final HideChannels hideChannels = new HideChannels(this);
	private final Invisible invisible = new Invisible(this);
	private final Locops locops = new Locops(this);
	private final NetAdmin netadmin = new NetAdmin(this);
	private final NickChange nickChange = new NickChange(this);
	private final net.rizon.plexus.modes.usermodes.NoCTCP um_noCtcp = new net.rizon.plexus.modes.usermodes.NoCTCP(this);
	private final Oper oper = new Oper(this);
	private final Operwall operwall = new Operwall(this);
	private final SoftCallerID softCallerId = new SoftCallerID(this);
	private final Spy spy = new Spy(this);
	private final net.rizon.plexus.modes.usermodes.RegOnly um_regOnly = new net.rizon.plexus.modes.usermodes.RegOnly(this);
	private final Registered registered = new Registered(this);
	private final Reject reject = new Reject(this);
	private final Routing routing = new Routing(this);
	private final ServerNotice serverNotice = new ServerNotice(this);
	private final SKill skill = new SKill(this);
	private final SSL ssl = new SSL(this);
	private final Service service = new Service(this);
	private final Unauth unauth = new Unauth(this);
	private final Wallop wallop = new Wallop(this);
	private final WebIRC webirc = new WebIRC(this);
	
	private final Ban ban = new Ban(this);
	private final BWSaver bwSaver = new BWSaver(this);
	private final Except except = new Except(this);
	private final HalfOp halfop = new HalfOp(this);
	private final Invex invex = new Invex(this);
	private final InviteOnly inviteOnly = new InviteOnly(this);
	private final Key key = new Key(this);
	private final Limit limit = new Limit(this);
	private final ModReg modReg = new ModReg(this);
	private final Moderated moderated = new Moderated(this);
	private final NoCTCP cm_noCtcp = new NoCTCP(this);
	private final NoCtrl noCtrl = new NoCtrl(this);
	private final NoExternal noExternal = new NoExternal(this);
	private final NoNotice noNotice = new NoNotice(this);
	private final Op op = new Op(this);
	private final OperOnly operOnnly = new OperOnly(this);
	private final Owner owner = new Owner(this);
	private final Persist persist = new Persist(this);
	private final Private priv = new Private(this);
	private final Protect protect = new Protect(this);
	private final RegOnly cm_regOnly = new RegOnly(this);
	private final Secret secret = new Secret(this);
	private final SSLOnly sslOnly = new SSLOnly(this);
	private final TopicLimit topicLimit = new TopicLimit(this);
	private final Voice voice = new Voice(this);

	public ModeManager(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public Plexus getPlexus()
	{
		return plexus;
	}

	public UserMode[] getUserModes()
	{
		return userModes;
	}

	public ChannelMode[] getChannelModes()
	{
		return channelModes;
	}

	public ChannelMode[] getStatusModes()
	{
		List<Status> status = new LinkedList<>();
		for (ChannelMode cm : channelModes)
		{
			if (cm instanceof Status)
			{
				status.add((Status) cm);
			}
		}
		Collections.sort(status);
		Status[] s = new Status[status.size()];
		status.toArray(s);
		return s;
	}

	public void insertChannelMode(ChannelMode cm)
	{
		char c = cm.getCharacter();

		if (c < 'A' || c > 'z')
		{
			throw new IllegalArgumentException();
		}

		channelModes[c - 'A'] = cm;
	}

	public ChannelMode findChannelMode(char c)
	{
		if (c < 'A' || c > 'z')
		{
			return null;
		}

		return channelModes[c - 'A'];
	}

	public void insertUserMode(UserMode um)
	{
		char c = um.getCharacter();
		
		if (c < 'A' || c > 'z')
			throw new IllegalArgumentException();
		
		userModes[c - 'A'] = um;
	}

	public UserMode findUserMode(char c)
	{
		if (c < 'A' || c > 'z')
		{
			return null;
		}

		return userModes[c - 'A'];
	}

	public Admin getAdmin()
	{
		return admin;
	}

	public Bots getBots()
	{
		return bots;
	}

	public CConn getCconn()
	{
		return cconn;
	}

	public Cloak getCloak()
	{
		return cloak;
	}

	public Deaf getDeaf()
	{
		return deaf;
	}

	public Debug getDebug()
	{
		return debug;
	}

	public CallerID getCallerId()
	{
		return callerId;
	}

	public External getExternal()
	{
		return external;
	}

	public FarConnect getFarConnect()
	{
		return farConnect;
	}

	public Full getFull()
	{
		return full;
	}

	public HideChannels getHideChannels()
	{
		return hideChannels;
	}

	public Invisible getInvisible()
	{
		return invisible;
	}

	public Locops getLocops()
	{
		return locops;
	}

	public NetAdmin getNetadmin()
	{
		return netadmin;
	}

	public NickChange getNickChange()
	{
		return nickChange;
	}

	public net.rizon.plexus.modes.usermodes.NoCTCP getUm_noCtcp()
	{
		return um_noCtcp;
	}

	public Oper getOper()
	{
		return oper;
	}

	public Operwall getOperwall()
	{
		return operwall;
	}

	public SoftCallerID getSoftCallerId()
	{
		return softCallerId;
	}

	public Spy getSpy()
	{
		return spy;
	}

	public net.rizon.plexus.modes.usermodes.RegOnly getUm_regOnly()
	{
		return um_regOnly;
	}

	public Registered getRegistered()
	{
		return registered;
	}

	public Reject getReject()
	{
		return reject;
	}

	public Routing getRouting()
	{
		return routing;
	}

	public ServerNotice getServerNotice()
	{
		return serverNotice;
	}

	public SKill getSkill()
	{
		return skill;
	}

	public SSL getSsl()
	{
		return ssl;
	}

	public Service getService()
	{
		return service;
	}

	public Unauth getUnauth()
	{
		return unauth;
	}

	public Wallop getWallop()
	{
		return wallop;
	}

	public WebIRC getWebirc()
	{
		return webirc;
	}

	public Ban getBan()
	{
		return ban;
	}

	public BWSaver getBwSaver()
	{
		return bwSaver;
	}

	public Except getExcept()
	{
		return except;
	}

	public HalfOp getHalfop()
	{
		return halfop;
	}

	public Invex getInvex()
	{
		return invex;
	}

	public InviteOnly getInviteOnly()
	{
		return inviteOnly;
	}

	public Key getKey()
	{
		return key;
	}

	public Limit getLimit()
	{
		return limit;
	}

	public ModReg getModReg()
	{
		return modReg;
	}

	public Moderated getModerated()
	{
		return moderated;
	}

	public NoCTCP getCm_noCtcp()
	{
		return cm_noCtcp;
	}

	public NoCtrl getNoCtrl()
	{
		return noCtrl;
	}

	public NoExternal getNoExternal()
	{
		return noExternal;
	}

	public NoNotice getNoNotice()
	{
		return noNotice;
	}

	public Op getOp()
	{
		return op;
	}

	public OperOnly getOperOnnly()
	{
		return operOnnly;
	}

	public Owner getOwner()
	{
		return owner;
	}

	public Persist getPersist()
	{
		return persist;
	}

	public Private getPriv()
	{
		return priv;
	}

	public Protect getProtect()
	{
		return protect;
	}

	public RegOnly getCm_regOnly()
	{
		return cm_regOnly;
	}

	public Secret getSecret()
	{
		return secret;
	}

	public SSLOnly getSslOnly()
	{
		return sslOnly;
	}

	public TopicLimit getTopicLimit()
	{
		return topicLimit;
	}

	public Voice getVoice()
	{
		return voice;
	}
}
