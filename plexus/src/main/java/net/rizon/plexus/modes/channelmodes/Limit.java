/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.channelmodes;

import net.rizon.plexus.Entity;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;

public class Limit extends ChannelMode
{
	public Limit(ModeManager manager)
	{
		super(manager, 'l');
		
		this.setParameter(true);
		this.setMinusNoArg(true);
	}
	
	@Override
	public boolean set(Entity source, Channel c, Membership mem, boolean add, String param)
	{
		try
		{
			if (add)
				c.setLimit(Integer.parseInt(param));
			else
				c.setLimit(0);
		}
		catch (NumberFormatException ex)
		{
			return false;
		}
		return super.set(source, c, mem, add, param);
	}
	
	@Override
	public <T> String getParam(T t)
	{
		if (t instanceof Channel)
		{
			Channel c = (Channel) t;
			return "" + c.getLimit();
		}
		return null;
	}
	
	@Override
	public String paramValid(LocalUser u, Channel c, Membership mem, boolean add, String param)
	{
		assert add;
		
		int i;
		try
		{
			i = Integer.parseInt(param);
		}
		catch (NumberFormatException ex)
		{
			return null;
		}
		if (i <= 0)
			return null;
		
		return param;
	}
}