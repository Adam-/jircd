/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.type;

import java.util.Date;
import java.util.Iterator;
import net.rizon.plexus.Entity;
import net.rizon.plexus.IRC;
import net.rizon.plexus.Numeric;
import net.rizon.plexus.channels.Channel;
import net.rizon.plexus.channels.Entry;
import net.rizon.plexus.channels.Membership;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.users.LocalUser;

public abstract class List extends ChannelMode
{
	public List(ModeManager manager, char c)
	{
		super(manager, c);
		
		this.setList(true);
	}

	@Override
	public boolean canSet(LocalUser u, Channel c, Membership mem, boolean add, String param, boolean shownError)
	{
		boolean b = super.canSet(u, c, mem, add, param, shownError);
		if (!b)
			return false;
		
		int total = c.getBans().size() + c.getExcepts().size() + c.getInvex().size();
		if (total >= getPlexus().getConf().getChannel().getMax_bans())
		{
			if (!shownError)
				u.writeNumeric(Numeric.ERR_BANLISTFULL, c.getName(), param);
			return false;
		}
		
		return true;
	}
	
	@Override
	public abstract boolean hasMode(Channel c, String param);
	
	protected boolean hasMode(Channel c, java.util.List<Entry> list, String param)
	{
		for (Entry e : list)
			if (IRC.equalsIgnoreCase(e.getMask(), param))
				return true;
		return false;
	}
	
	@Override
	public String paramValid(LocalUser u, Channel c, Membership mem, boolean add, String param)
	{
		if (param == null)
		{
			this.list(u, c);
			return null;
		}
		
		Entry e = new Entry(param, null, null);
		return e.getNick() + "!" + e.getUser() + "@" + e.getHost();
	}
	
	@Override
	public abstract boolean set(Entity source, Channel c, Membership mem, boolean add, String param);
	
	public abstract void list(LocalUser u, Channel c);
	
	protected void list(LocalUser u, Channel c, Numeric list, Numeric end, java.util.List<Entry> entries)
	{
		for (Entry e : entries)
			u.writeNumeric(list, c.getName(), e.getNick(), e.getUser(), e.getHost(), e.getWho(), e.getWhen().getTime() / 1000L);
		u.writeNumeric(end, c.getName());
	}
	
	protected boolean set(Entity source, Channel c, Membership mem, boolean add, String param, java.util.List<Entry> list)
	{
		for (Iterator<Entry> it = list.iterator(); it.hasNext();)
		{
			Entry e = it.next();
			
			if (e.getMask().equalsIgnoreCase(param))
			{
				if (!add)
				{
					it.remove();
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		
		if (!add)
		{
			return false;
		}
		else
		{
			list.add(new Entry(param, source.getName(), new Date()));
			return true;
		}
	}
}