/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.rizon.plexus.modes.usermodes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import net.rizon.plexus.IRC;
import net.rizon.plexus.config.Config;
import net.rizon.plexus.modes.ModeManager;
import net.rizon.plexus.modes.UserMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Cloak extends UserMode
{
	private static final Logger logger = LoggerFactory.getLogger(Cloak.class);
	
	public Cloak(ModeManager manager)
	{
		super(manager, 'x');
	}
	
	private static MessageDigest md5;
	
	static
	{
		try
		{
			md5 = MessageDigest.getInstance("MD5");
		}
		catch (NoSuchAlgorithmException ex)
		{
			logger.error("No MD5 digest", ex);
		}
	}
	
	/* 128 bit in -> 32 bit result */
	private static int downsample(byte[] i)
	{
		byte r1, r2, r3, r4;
		
		r1 = (byte) (i[0] ^ i[1] ^ i[2] ^ i[3]);
		r2 = (byte) (i[4] ^ i[5] ^ i[6] ^ i[7]);
		r3 = (byte) (i[8] ^ i[9] ^ i[10] ^ i[11]);
		r4 = (byte) (i[12] ^ i[13] ^ i[14] ^ i[15]);
		
		return (r1 << 24) + (r2 << 16) + (r3 << 8) + r4;
	}
	
	private String cloakIPv6(String host)
	{
		Config conf = getPlexus().getConf();
		
		/* Plexus uses sscanf %x:%x:%x etc, so the first :: causes it to choke */
		int i = host.indexOf("::");
		if (i != -1)
			host = host.substring(0, i);
		
		String[] s = host.split(":");
		int a, b, c, d, e, f, g, h;
		int alpha, beta, gamma;
		
		try
		{
			a = s.length > 0 ? Integer.parseInt(s[0]) : 0;
			b = s.length > 1 ? Integer.parseInt(s[1]) : 0;
			c = s.length > 2 ? Integer.parseInt(s[2]) : 0;
			d = s.length > 3 ? Integer.parseInt(s[3]) : 0;
			e = s.length > 4 ? Integer.parseInt(s[4]) : 0;
			f = s.length > 5 ? Integer.parseInt(s[5]) : 0;
			g = s.length > 6 ? Integer.parseInt(s[6]) : 0;
			h = s.length > 7 ? Integer.parseInt(s[7]) : 0;
			
			if (a < 0 || a > 255)
				return null;
			if (b < 0 || b > 255)
				return null;
			if (c < 0 || c > 255)
				return null;
			if (d < 0 || d > 255)
				return null;
			if (e < 0 || e > 255)
				return null;
			if (f < 0 || f > 255)
				return null;
			if (g < 0 || g > 255)
				return null;
			if (h < 0 || h > 255)
				return null;
		}
		catch (NumberFormatException ex)
		{
			return null;
		}
		
		/* alpha */
		String buf = conf.getGeneral().getCloak_key2() + ":" + host + ":" + conf.getGeneral().getCloak_key3();
		byte[] res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key1();
		res = md5.digest(buf.getBytes());
		alpha = downsample(res);
		
		/* beta */
		buf = String.format("%s:%x:%x:%x:%x:%x:%x:%x:%s", conf.getGeneral().getCloak_key3(), a, b, c, d, e, f, g, conf.getGeneral().getCloak_key1());
		res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key2();
		res = md5.digest(buf.getBytes());
		beta = downsample(res);
		
		/* gamma */
		buf = String.format("%s:%x:%x:%x:%x:%s", conf.getGeneral().getCloak_key1(), a, b, c, d, conf.getGeneral().getCloak_key2());
		res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key3();
		res = md5.digest(buf.getBytes());
		gamma = downsample(res);
		
		return String.format("%X:%X:%X:IP", alpha, beta, gamma);
	}
	
	private String cloakIPv4(String host)
	{
		Config conf = getPlexus().getConf();
		
		int a, b, c, d;
		int alpha, beta, gamma;
		
		String[] h = host.split("\\.");
		if (h.length != 4)
			return null;
		
		try
		{
			a = Integer.parseInt(h[0]);
			b = Integer.parseInt(h[1]);
			c = Integer.parseInt(h[2]);
			d = Integer.parseInt(h[3]);
			
			if (a < 0 || a > 255)
				return null;
			if (b < 0 || b > 255)
				return null;
			if (c < 0 || c > 255)
				return null;
			if (d < 0 || d > 255)
				return null;
		}
		catch (NumberFormatException ex)
		{
			return null;
		}
		
		/* alpha */
		String buf = conf.getGeneral().getCloak_key2() + ":" + host + ":" + conf.getGeneral().getCloak_key3();
		byte[] res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key1();
		res = md5.digest(buf.getBytes());
		alpha = downsample(res);
		
		/* beta */
		buf = conf.getGeneral().getCloak_key3() + ":" + a + "." + b + "." + c + ":" + conf.getGeneral().getCloak_key1();
		res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key2();
		res = md5.digest(buf.getBytes());
		beta = downsample(res);
		
		/* gamma */
		buf = conf.getGeneral().getCloak_key1() + ":" + a + "." + b + "." + conf.getGeneral().getCloak_key2();
		res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key3();
		res = md5.digest(buf.getBytes());
		gamma = downsample(res);
		
		return String.format("%X.%X.%X.IP", alpha, beta, gamma);
	}
	
	private String cloakNormal(String host)
	{
		Config conf = getPlexus().getConf();
		
		int alpha;
		
		String buf = conf.getGeneral().getCloak_key1() + ":" + host + ":" + conf.getGeneral().getCloak_key2();
		byte[] res = md5.digest(buf.getBytes());
		buf = new String(res) + conf.getGeneral().getCloak_key3();
		res = md5.digest(buf.getBytes());
		alpha = downsample(res);
		
		int i;
		for (i = 0; i < host.length(); ++i)
		{
			char c = host.charAt(i);
			if (c == '.' && i + 1 < host.length() && IRC.isLetter(host.charAt(i + 1)))
				break;
		}
		
		if (i < host.length())
		{
			++i;
			buf = String.format("%s-%X.", conf.getServerinfo().getNetwork_name(), alpha);
			buf += host.substring(i);
			if (buf.length() > IRC.HOSTLEN)
				buf = buf.substring(0, IRC.HOSTLEN); 
		}
		else
			buf = String.format("%s-%X", conf.getServerinfo().getNetwork_name(), alpha);
		
		return buf;
	}
	
	public String cloak(String host)
	{
		if (!isEnabled())
			return null;
		
		host = IRC.toLowerCase(host);
		
		if (host.indexOf(':') != -1)
			return cloakIPv6(host);
		
		int i;
		for (i = 0; i < host.length(); ++i)
		{
			char c = host.charAt(i);
			if (!IRC.isDigit(c) && c != '.')
				break;
		}
		if (i == host.length())
			return cloakIPv4(host);
		
		return cloakNormal(host);
	}
	
	public boolean isEnabled()
	{
		Config conf = getPlexus().getConf();
		return conf.getGeneral().getCloak_key1() != null && conf.getGeneral().getCloak_key2() != null && conf.getGeneral().getCloak_key3() != null;
	}
}