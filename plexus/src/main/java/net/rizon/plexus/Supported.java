/*
 * Copyright (c) 2016, Adam <Adam@anope.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.plexus;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import net.rizon.plexus.modes.ChannelMode;
import net.rizon.plexus.modes.UserMode;
import net.rizon.plexus.users.LocalUser;

public class Supported
{
	private final Plexus plexus;
	private final Map<String, String> settings = new TreeMap<>();
	private String usermodes, channelmodes, parammodes;

	public Supported(Plexus plexus)
	{
		this.plexus = plexus;
	}

	public void setup()
	{
		addSupport("CASEMAPPING", "rfc1459");
		addSupport("NICKLEN", IRC.NICKLEN);
		addSupport("ELIST", "CMNTU"); // not sure what this is? related to list
		addSupport("SAFELIST"); // will throttle /list and not flood users off of the network

		addSupport("NETWORK", plexus.getConf().getServerinfo().getNetwork_name());
		addSupport("DEAF", Character.toString(plexus.getModeManager().getDeaf().getCharacter()));
		addSupport("EXCEPTS", Character.toString(plexus.getModeManager().getExcept().getCharacter()));
		addSupport("INVEX", Character.toString(plexus.getModeManager().getInvex().getCharacter()));
		addSupport("KNOCK");

		buildPrefix();
		buildChannelModes();
		buildUserModes();
	}

	private void buildPrefix()
	{
		// PREFIX=(chars)prefix
		StringBuilder prefixChars = new StringBuilder(),
			prefixPrefix = new StringBuilder();

		for (ChannelMode cm : plexus.getModeManager().getStatusModes())
		{
			prefixChars.append(cm.getCharacter());
			prefixPrefix.append(cm.getPrefix());
		}

		addSupport("PREFIX", "(" + prefixChars + ")" + prefixPrefix);
	}

	private void buildChannelModes()
	{
		// CHANMODES=list,param with arg,param with no minus arg,other

		StringBuilder listModes = new StringBuilder(),
			paramModes = new StringBuilder(),
			paramMinusNoArg = new StringBuilder(),
			otherModes = new StringBuilder(),
			statusModes = new StringBuilder();

		for (ChannelMode cm : plexus.getModeManager().getChannelModes())
		{
			if (cm == null)
			{
				continue;
			}

			if (cm.isList())
			{
				listModes.append(cm.getCharacter());
			}
			else if (cm.isMinusNoArg())
			{
				paramMinusNoArg.append(cm.getCharacter());
			}
			else if (cm.isParameter())
			{
				paramModes.append(cm.getCharacter());
			}
			else if (cm.getPrefix() > 0)
			{
				statusModes.append(cm.getCharacter());
			}
			else
			{
				otherModes.append(cm.getCharacter());
			}
		}

		parammodes = listModes.toString() + paramModes + paramMinusNoArg;
		addSupport("CHANMODES", listModes + "," + paramModes + "," + paramMinusNoArg + "," + otherModes);

		StringBuilder sb = new StringBuilder();
		sb.append(listModes);
		sb.append(paramModes);
		sb.append(paramMinusNoArg);
		sb.append(statusModes);

		// Sort param modes
		parammodes = sort(sb.toString());

		// Sort channel modes
		sb = new StringBuilder();
		sb.append(otherModes);
		sb.append(parammodes);

		channelmodes = sort(sb.toString());
	}

	private void buildUserModes()
	{
		StringBuilder userModes = new StringBuilder();

		for (UserMode um : plexus.getModeManager().getUserModes())
		{
			if (um != null)
			{
				userModes.append(um.getCharacter());
			}
		}

		usermodes = userModes.toString();
	}

	public <T> void addSupport(String key, T value)
	{
		settings.put(key, value != null ? value.toString() : null);
	}

	public void addSupport(String key)
	{
		addSupport(key, null);
	}

	public void removeSupport(String key)
	{
		settings.remove(key);
	}

	public void showNewClient(LocalUser user)
	{
		user.writeNumeric(Numeric.RPL_MYINFO, plexus.getConf().getServerinfo().getName(), "jircd-dev", usermodes, channelmodes, parammodes);
		show(user);
	}

	public void show(LocalUser user)
	{
		NumericBuilder nb = new NumericBuilder(Numeric.RPL_ISUPPORT, user);

		for (Map.Entry<String, String> e : settings.entrySet())
		{
			if (e.getValue() != null)
			{
				nb.add(e.getKey() + "=" + e.getValue());
			}
			else
			{
				nb.add(e.getKey());
			}
		}

		nb.send();
	}

	private static String sort(String str)
	{
		char[] c = str.toCharArray();
		Arrays.sort(c);
		return new String(c);
	}
}
